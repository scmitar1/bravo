Ext.define('BRAVO.override.window.Toast', {
    override: 'Ext.window.Toast',
    initComponent: function () {
        var me = this;
        this.minWidth = 300;
        if(!Ext.isEmpty(this.html)){
            this.html = '<center>' + this.html + '</center>';
        }
        
        me.callParent(arguments);
    }

});