Ext.define('BRAVO.grid.NavigationModel',{
    override: 'Ext.grid.NavigationModel',
    onContainerMouseDown: function(view, mousedownEvent) {
        var me = this,
            context = new Ext.grid.CellContext(view),
            lastFocused, position;

        me.callSuper([view, mousedownEvent]);

        lastFocused = view.lastFocused;
        position = (view.actionableMode && view.actionPosition) || lastFocused;

        if (!position || lastFocused === 'scrollbar') {
            return;
        }

        context.setPosition(position.record, position.column);
        mousedownEvent.position = context;
        me.attachClosestCell(mousedownEvent);

        // If we are not already on that position, set position there.
        if (!me.position.isEqual(context)) {
            me.setPosition(context, null, mousedownEvent);
        }
    }
});