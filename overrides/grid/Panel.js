Ext.define('BRAVO.grid.Panel', {
    override: 'Ext.grid.Panel',
    cls: 'title-header',
    initComponent : function(){
        var me = this;

        Ext.apply(me,{
           _columns : me.columns
        });
        me.callParent(arguments);

    }

});