Ext.define('BRAVO.override.grid.feature.Summary', {
    override: 'Ext.grid.feature.Summary',
    getSummary: function (store, type, field, group) {
        var isGrouped = !!group,
            item = isGrouped ? group : store,
            aggregators = Ext.util.Collection.prototype._aggregators;

        if (type) {
            if (Ext.isFunction(type)) {
                if (isGrouped) {
                    return item.aggregate(field, type);
                } else {
                    return item.aggregate(type, null, false, [field]);
                }
            }
            if (aggregators[type]) {
                return item[type](field);
            } else {
                return '';
            }
        }
    }
});