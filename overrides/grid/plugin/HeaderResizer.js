Ext.define('BRAVO.override.grid.plugin.HeaderResizer', {
    override: 'Ext.grid.plugin.HeaderResizer',
    onStart: function(e) {
        var me = this,
            dragHd = me.dragHd,
            width = dragHd.el.getWidth(),
            headerCt = dragHd.up('[isRootHeader][rendered]'),
            x, y, markerOwner, lhsMarker, rhsMarker, markerHeight;
        me.headerCt.dragging = true;
        me.origWidth = width;
        // setup marker proxies
        if (!me.dynamic) {
            markerOwner = me.markerOwner;
            // https://sencha.jira.com/browse/EXTJSIV-11299
            // In Neptune (and other themes with wide frame borders), resize handles are embedded in borders,
            // *outside* of the outer element's content area, therefore the outer element is set to overflow:visible.
            // During column resize, we should not see the resize markers outside the grid, so set to overflow:hidden.
            if (markerOwner.frame && markerOwner.resizable) {
                me.gridOverflowSetting = markerOwner.el.dom.style.overflow;
                markerOwner.el.dom.style.overflow = 'hidden';
            }
            x = me.getLeftMarkerX(markerOwner);
            lhsMarker = markerOwner.getLhsMarker();
            rhsMarker = markerOwner.getRhsMarker();
            markerHeight = me.ownerGrid.body.getHeight() + headerCt.getHeight();
            y = headerCt.getOffsetsTo(markerOwner)[1] - markerOwner.el.getBorderWidth('t');
            // Ensure the markers have the correct cursor in case the cursor is *exactly* over
            // this single pixel line, not just within the active resize zone
            lhsMarker.dom.style.cursor = me.eResizeCursor;
            rhsMarker.dom.style.cursor = me.eResizeCursor;
            lhsMarker.setLocalY(y);
            rhsMarker.setLocalY(y);
            lhsMarker.setHeight(markerHeight);
            rhsMarker.setHeight(markerHeight);
            me.setMarkerX(lhsMarker, x);
            me.setMarkerX(rhsMarker, x + width);
        }
    }
});