Ext.define('BRAVO.override.grid.column.Column', {
    override: 'Ext.grid.column.Column',
    style: 'text-align:center',
    localeProperties: ['text', 'header'],
    align: 'start',
    editable: false,
    initComponent: function () {
        var me = this;


        Ext.apply(me,{
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        });

        if (me.editor) {
            Ext.apply(me, {
                editor: Ext.widget(me.editor.xtype, me.editor),
                getEditor: function () {
                    return me.editor;
                }
            });
        }

        me.callParent(arguments);
    },

    getView: function () {
        var rootHeaderCt;
        // Only traverse to get our view once.
        if (!this.view) {
            rootHeaderCt = this.getRootHeaderCt();
            if (rootHeaderCt) {
                this.view = rootHeaderCt.view;
            }
        }
        if (!this.view) {
            this.view = this.up('tablepanel').view;
        }
        return this.view;
    },

    getIndex: function () {
        return this.isGroupColumn ? false : this.up('[isRootHeader][rendered]').getHeaderIndex(this)
    }
});