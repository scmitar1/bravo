/**
 * Created by Hynkuk on 2018-02-23.
 */
Ext.define('BRAVO.overrides.grid.column.Widget', {
    override: 'Ext.grid.column.Widget',
    initComponent: function() {
        var me = this,
            widget, bind, clazz;

        me.callParent(arguments);

        widget = me.widget;
        //<debug>
        if (!widget || widget.isComponent) {
            Ext.raise('column.Widget requires a widget configuration.');
        }
        //</debug>
        me.widget = widget = Ext.apply({}, widget);

        clazz = Ext.ClassManager.getByAlias('widget.' + widget.xtype);
        if (clazz.prototype.xtypesMap['field']) {
            bind = widget.bind = widget.bind || {};
            if (Ext.isObject(bind)) {
                Ext.apply(bind, {
                    value: Ext.String.format('{record.{0}}', me.dataIndex)
                });
            } else if (Ext.isString(bind)) {
                var _bind = {};
                _bind[clazz.prototype['defaultBindProperty']] = bind;
                _bind['value'] = Ext.String.format('{record.{0}}', me.dataIndex);
                widget.bind = _bind;
            }
        }
        // Apply the default UI for the xtype which is going to feature in this column.
        if (!widget.ui) {
            widget.ui = me.getDefaultWidgetUI()[widget.xtype] || 'default';
        }
        me.isFixedSize = Ext.isNumber(widget.width);
    }
});