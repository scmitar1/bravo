Ext.define('BRAVO.override.data.field.Boolean', {
    override: 'Ext.data.field.Boolean',
    trueRe: /^\s*(?:true|yes|on|1|Y)\s*$/i,
    defaultsValue: false
});