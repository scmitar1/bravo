Ext.define('BRAVO.override.data.Store', {
    override: 'Ext.data.Store',
    constructor: function (config) {
        var me = this,
            data;


        if(config==undefined){
            config={};
        }

        var modelType =  {name: 'isChecked', type: 'boolean', defaultValue: false};
        if(Ext.isArray(config.fields)){
            config.fields.push(modelType);
        }else {
            config.fields = [modelType];
        }



        if (config) {
            if (config.buffered) {
                //<debug>
                if (this.self !== Ext.data.Store) {
                    Ext.raise('buffered config not supported on derived Store classes. '+
                        'Please derive from Ext.data.BufferedStore.');
                }
                //</debug>

                return new Ext.data.BufferedStore(config);
            }

            //<debug>
            if (config.remoteGroup) {
                Ext.log.warn('Ext.data.Store: remoteGroup has been removed. Use remoteSort instead.');
            }
            //</debug>
        }

        /**
         * @event beforeprefetch
         * Fires before a prefetch occurs. Return `false` to cancel.
         * @param {Ext.data.Store} this
         * @param {Ext.data.operation.Operation} operation The associated operation.
         */
        /**
         * @event groupchange
         * Fired whenever the grouping in the grid changes.
         * @param {Ext.data.Store} store The store.
         * @param {Ext.util.Grouper} grouper The grouper object.
         */
        /**
         * @event prefetch
         * Fires whenever records have been prefetched.
         * @param {Ext.data.Store} this
         * @param {Ext.data.Model[]} records An array of records.
         * @param {Boolean} successful `true` if the operation was successful.
         * @param {Ext.data.operation.Operation} operation The associated operation.
         */
        /**
         * @event filterchange
         * Fired whenever the filter set changes.
         * @param {Ext.data.Store} store The store.
         * @param {Ext.util.Filter[]} filters The array of Filter objects.
         */

        me.callParent([config]);

        // See applyData for the details.
        data = me.inlineData;
        if (data) {
            delete me.inlineData;
            me.loadInlineData(data);
        }





    },
    copy: function () {
        var me = this;

        var records = [];
        me.each(function(r){
            records.push(r.copy());
        });
        var store = Ext.create('Ext.data.Store',{
           data : records
        });
        return store;
    },
    loadRawData : function(data,append){
        var me      = this,
            session = me.getSession(),
            result  = me.getProxy().getReader().read(data, session ? {
                recordCreator: session.recordCreator
            } : undefined),
            records = result.getRecords(),
            success = result.getSuccess();

        if (success) {
            me.totalCount = result.getTotal();
            me.loadRecords(records, append ? me.addRecordsOptions : undefined);
        }

        if(me.getProxy() && me.getProxy().type=='memory'){
            me.fireEvent('load',me);
        }

        return success;
    },

    sumNll: function (field, grouped) {
        var data = this.getData();
        return (grouped && this.isGrouped()) ? data.sumNllByGroup(field) : data.sumNll(field)
    }

});