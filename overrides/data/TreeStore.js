Ext.define('BRAVO.override.data.TreeStore', {
    override: 'Ext.data.TreeStore',
    loadListData : function(arrayList, id, parentId, rootId){
        var me = this;
        var childNode = me.convertListToTree(arrayList, id, parentId, rootId);



        me.setRoot({
            expanded: true,
            children: childNode
        });

    },
    convertListToTree: function (arrayList, id, parentId, rootId) {

        var rootNodes = [];
        var traverse = function (nodes, item, index) {

            if (nodes instanceof Array) {

                return nodes.some(function (node) {

                    if (node[id] === item[parentId]) {

                        node.children = node.children || [];
                        return node.children.push(arrayList.splice(index, 1)[0]);
                    }

                    return traverse(node.children, item, index);
                });
            }
        };




        var beforeId = null;
        while (arrayList.length > 0) {
            arrayList.some(function (item, index) {

                if(beforeId==item[id]){
                    return arrayList.splice(index, 1)[0];
                }

                if (item[parentId] == rootId ||item[parentId] == '') {
                    return rootNodes.push(arrayList.splice(index, 1)[0]);
                }


                beforeId=item[id];

                return traverse(rootNodes, item, index);
            });
        }

        return rootNodes;
    }

});