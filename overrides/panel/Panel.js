Ext.define('BRAVO.override.panel.Panel', {
    override: 'Ext.panel.Panel',
    localeProperties: 'title',
    initComponent: function () {
        var me = this;
        me.on('afterrender',function(comp){
            comp.fireEvent('InitMode', comp);

        });
        me.callParent(arguments);
    }

});