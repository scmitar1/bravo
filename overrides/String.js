/**
 * Created by Hynkuk on 2017-11-17.
 */
Ext.define('BRAVO.overrides.String', {
}, function () {
   Ext.String.rightPad = function (string, size, character) {
       var result = String(string);
       character = character || " ";
       while (result.length < size) {
           result = result + character ;
       }
       return result;
   };
});