Ext.define('BRAVO.override.view.Table', {
    override: 'Ext.view.Table',
    /**
     *
     * @param {Boolean} enabled
     * @param {Ext.grid.CellContext} position The cell to activate.
     * @param {HTMLElement/Ext.dom.Element} [position.target] The element within the referenced cell to focus.
     * @return {Boolean} Returns `false` if the mode did not change.
     * @private
     */
    privates: {
        setActionableMode: function(enabled, position) {
            var me = this,
                navModel = me.getNavigationModel(),
                activeEl,
                actionables = me.grid.actionables,
                len = actionables.length,
                i, record, column,
                isActionable = false,
                lockingPartner, cell;
            // No mode change.
            // ownerGrid's call will NOT fire mode change event upon false return.
            if (me.actionableMode === enabled) {
                // If we're not actinoable already, or (we are actionable already at that position) return false.
                // Test using mandatory passed position because we may not have an actionPosition if we are
                // the lockingPartner of an actionable view that contained the action position.
                //
                // If we being told to go into actionable mode but at another position, we must continue.
                // This is just actionable navigation.
                if (!enabled || position.isEqual(me.actionPosition)) {
                    return false;
                }
            }
            // If this View or its lockingPartner contains the current focus position, then make the tab bumpers tabbable
            // and move them to surround the focused row.
            if (enabled) {
                if (position && (position.view === me || (position.view === (lockingPartner = me.lockingPartner) && lockingPartner.actionableMode))) {
                    isActionable = me.activateCell(position);
                }
                // Did not enter actionable mode.
                // ownerGrid's call will NOT fire mode change event upon false return.
                return isActionable;
            } else {
                // Capture before exiting from actionable mode moves focus
                activeEl = Ext.fly(Ext.Element.getActiveElement());
                // Blur the focused descendant, but do not trigger focusLeave.
                // This is so that when the focus is restored to the cell which contained
                // the active content, it will not be a FocusEnter from the universe.
                if (me.el.contains(activeEl) && !Ext.fly(activeEl).is(me.getCellSelector())) {
                    // Row to return focus to.
                    record = (me.actionPosition && me.actionPosition.record) || me.getRecord(activeEl);
                    column = me.getHeaderByCell(activeEl.findParent(me.getCellSelector()));
                    cell = position && position.getCell(true);
                    // Do not allow focus to fly out of the view when the actionables are deactivated
                    // (and blurred/hidden). Restore focus to the cell in which actionable mode is active.
                    // Note that the original position may no longer be valid, e.g. when the record
                    // was removed.
                    if (!position || !cell) {
                        position = new Ext.grid.CellContext(me).setPosition(record || 0, column || 0);
                        cell = position.getCell(true);
                    }
                    // Ext.grid.NavigationModel#onFocusMove will NOT react and navigate because the actionableMode
                    // flag is still set at this point.
                    cell && Ext.fly(cell).focus();
                    // Let's update the activeEl after focus here
                    activeEl = Ext.fly(Ext.Element.getActiveElement());
                    // If that focus triggered handlers (eg CellEditor after edit handlers) which
                    // programatically moved focus somewhere, and the target cell has been unfocused, defer to that,
                    // null out position, so that we do not navigate to that cell below.
                    // See EXTJS-20395
                    if (!(me.el.contains(activeEl) && activeEl.is(me.getCellSelector()))) {
                        position = null;
                    }
                }
                // We are exiting actionable mode.
                // Tell all registered Actionables about this fact if they need to know.
                for (i = 0; i < len; i++) {
                    if (actionables[i].deactivate) {
                        actionables[i].deactivate();
                    }
                }
                // If we had begun action (we may be a dormant lockingPartner), make any tabbables untabbable
                if (me.actionRow) {
                    me.actionRow.saveTabbableState({
                        skipSelf: true,
                        includeSaved: false
                    });
                }
                if (me.destroyed) {
                    return false;
                }
                // These flags MUST be set before focus restoration to the owning cell.
                // so that when Ext.grid.NavigationModel#setPosition attempts to exit actionable mode, we don't recurse.
                me.actionableMode = me.ownerGrid.actionableMode = false;
                me.actionPosition = navModel.actionPosition = me.actionRow = null;
                // Push focus out to where it was requested to go.
                if (position) {
                    navModel.setPosition(position);
                }
            }
        },

        resumeActionableMode: function(position) {
            var me = this,
                actionables = me.grid.actionables,
                len = actionables.length,
                i,
                activated;

            // Disable tabbability of elements within this view.
            me.toggleChildrenTabbability(false);

            for (i = 0; i < len; i++) {
                activated = activated || actionables[i].resume(position);
            }
            // If non of the Actionable responded, attempt to find a naturally focusable child element.
            if (!activated && position.record) {
                me.activateCell(position);
            }
        }
    },

    onFocusEnter: function(e) {
        // We need to react in a correct way to focus entering the TableView.
        // Much of this is based upon http://www.w3.org/TR/wai-aria-practices-1.1/#h-grid
        // specifically: "Once focus has been moved inside the grid, subsequent tab presses that re-enter the grid shall return focus to the cell that last held focus."
        //
        // If an interior element is being focused, then if it is a cell, we enter navigable mode at that cell.
        // If an interior element *wthin* a cell is being focused, we enter actionable mode at that cell and focus that element.
        // If just the view itself is being focused we focus the lastFocused CellContext. This is the last cell position which
        // the user navigated to in any mode, actinoable or navigable. It is maintained during navigation in navigable mode.
        // It is set upon focus leave if focus left during actionable mode - set to actionPosition.
        // actionPosition is cleared when actionable mode is exited.
        //
        // The important context is lastFocused.
        var me = this,
            fromComponent = e.fromComponent,
            navigationModel = me.getNavigationModel(),
            focusPosition, cell, focusTarget;
        // If a mousedown listener has synchronously focused an internal element
        // from outside and proceeded to process focus consequences, then the impending focusenter
        // MUST NOT process focus consequences.
        // See Ext.grid.NagivationModel#onCellMouseDown
        if (me.containsFocus) {
            return Ext.Component.prototype.onFocusEnter.call(me, e);
        }
        // FocusEnter while in actionable mode.
        if (me.actionableMode) {
            // If we own the actionPosition it must be due to a setActionPosition call
            // setting the actionPosition and then focusing the actionable element.
            // We need to disable view outer el focusing while focus is inside.
            if (me.actionPosition) {
                me.el.dom.setAttribute('tabIndex', '-1');
                me.cellFocused = true;
                return;
            }
            // Must have swapped sides of a lockable.
            // We don't know what we're focusing into yet.
            // So exit actionable mode.
            // We could be focusing a cell, in which case navigable mode is correct.
            // If we are focusing an interior element that is not a cell, we will enter actionable mode.
            me.ownerGrid.setActionableMode(false);
        }
        // The underlying DOM event
        e = e.event;
        // We can only focus if there are rows in the row cache to focus *and* records
        // in the store to back them. Buffered Stores can produce a state where
        // the view is not cleared on the leading end of a reload operation, but the
        // store can be empty.
        if (!me.cellFocused && me.all.getCount() && me.dataSource.getCount()) {
            focusTarget = e.getTarget();
            // The View's el has been focused.
            // We now have to decide which cell to focus
            if (focusTarget === me.el.dom) {
                // This lastFocused value is set on mousedown on the scrollbar in IE/Edge.
                // Those browsers focus the element on mousedown on its scrollbar
                // which is not what we want, so throw focus back in this
                // situation.
                // See Ext.view.navigationModel for this being set.
                if (me.lastFocused === 'scrollbar') {
                    e.relatedTarget && e.relatedTarget.focus();
                    return;
                }
                focusPosition = me.getDefaultFocusPosition(fromComponent);
                // Not a descendant which we allow to carry focus. Focus the view el.
                if (!focusPosition) {
                    e.stopEvent();
                    me.el.focus();
                    return;
                }
                // We are entering navigable mode, so we have a focusPosition but no focusTarget
                focusTarget = null;
            }
            // Hit the invisible focus guard. This mean SHIT+TAB back into the grid.
            // Focus last cell.
            else if (focusTarget === me.tabGuardEl) {
                focusPosition = new Ext.grid.CellContext(me).setPosition(me.all.endIndex, me.getVisibleColumnManager().getColumns().length - 1);
                focusTarget = null;
            }
            // Now there are just two valid choices.
            // Focused a cell, or an interior element within a cell.
            else if (cell = e.getTarget(me.getCellSelector())) {
                // Programmatic focus of a cell...
                if (focusTarget === cell) {
                    // We are entering navigable mode, so we have a focusPosition but no focusTarget
                    focusPosition = new Ext.grid.CellContext(me).setPosition(me.getRecord(focusTarget), me.getHeaderByCell(cell));
                    focusTarget = null;
                }
                // If what is being focused an interior element, but is not a cell, we plan to enter
                // actionable mode. This will happen when an ActionColumn invokes a modal window
                // and that window is dismissed leading to automatic focus of the previously focused element.
                // This also happens when SHIFT+TAB moves back towards the view. It navigated to the last tabbable element.
                // Testing whether the focusTarget isFocusable is a fix for IE. It can sometimes fire a focus event with the .x-scroll-scroller as the target
                else if (focusTarget && Ext.fly(focusTarget).isFocusable() && me.el.contains(focusTarget)) {
                    // We are entering actionable mode, so we have a focusPosition and a focusTarget
                    focusPosition = new Ext.grid.CellContext(me).setPosition(me.getRecord(focusTarget), me.getHeaderByCell(cell));
                }
            }
        }
        // We must exit from the above code block with focusPosition set to a CellContext
        // which is going to be either the navigable or actionable position. If focusPosition
        // is null, we are not focusing the view.
        //
        // IF we are entering actionable mode, then focusTarget will be set to an internal
        // focusable element within the cell referenced by focusPosition.
        // We calculated a cell to focus on. Either from the target element, or the last focused position
        if (focusPosition) {
            // Disable tabbability of elements within this view.
            me.toggleChildrenTabbability(false);
            // If we fall through to here with a focusTarget, it means that it's an internal focusable element
            // and we request to enter actionable mode at the focusPosition
            if (focusTarget) {
                // Tell actionable mode which element we want to focus.
                // By default it focuses the first focusable in the cell.
                focusPosition.target = focusTarget;
                // If we successfully entered actionable mode at the requested position, prevent entering navigable mode by nulling
                // the focusPosition, and focus the intended target (setActionableMode will have focused the *first* tabbable in the cell)
                // If we were unsuccessful, then we must proceed with focusPosition set in order to enter navigable mode here.
                if (me.ownerGrid.setActionableMode(true, focusPosition)) {
                    focusPosition = null;
                }
            }
            // Test again here.
            // If we successfully entered actionable mode, this will be null.
            // If the attempt failed, it should fall back to navigable mode.
            if (focusPosition) {
                navigationModel.setPosition(focusPosition, null, e, null, true);
            }
            // We now contain focus if that was successful
            me.cellFocused = me.el.contains(Ext.Element.getActiveElement());
            if (me.cellFocused) {
                me.el.dom.setAttribute('tabIndex', '-1');
            }
        }
        // Skip the AbstractView's implementation.
        // It initializes its NavModel differently.
        Ext.Component.prototype.onFocusEnter.call(me, e);
    }
});