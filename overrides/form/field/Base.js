Ext.define('BRAVO.override.form.field.Base', {
    override: 'Ext.form.field.Base',
    localeProperties: ['fieldLabel'],
    labelAlign: 'top',
    labelSeparator: '',
    enableKeyEvents: true,
    initComponent: function () {
        var me = this;
        me.callParent(arguments);
        me.oriFieldLabel = me.fieldLabel;
        me.addListener('afterrender', function (component) {
            if (component.allowBlank === false) {
                component.setAllowBlank(component.allowBlank);
            }
        });
    },

    setAllowBlank: function (allowBlank, afterTextLabel) {
        var me = this;
        me.allowBlank = !!allowBlank;
        
        if (allowBlank) {
            me.setFieldLabel(me.oriFieldLabel);
        } else {

            if (!Ext.isEmpty(me.fieldLabel)) {
                me.setFieldLabel(me.oriFieldLabel + '<font color="red"> *</font>');
            }
        }
    },
    setOriFieldLabel: function (label) {
        var me = this;
        me.oriFieldLabel = label;
        me.setAllowBlank(me.allowBlank);
    }
});