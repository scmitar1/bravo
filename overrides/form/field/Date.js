Ext.define('BRAVO.override.form.field.Date', {
    override: 'Ext.form.field.Date',
    submitFormat : 'Ymd',
    format : 'Ymd',
    altFormats : 'Ymd|Y-m-d|Y.m.d|Y/m/d|m/d/Y|n/j/Y|n/j/y|m/j/y|n/d/y|m/j/Y|n/d/Y|m-d-y|m-d-Y|m/d|m-d|md|mdy|mdY|d|Y-m-d|n-j|n/j|time'


});