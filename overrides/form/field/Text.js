Ext.define('BRAVO.override.form.field.Text', {
    override: 'Ext.form.field.Text',
    readOnlyInputCls : 'bravo-field-readonly',
    initComponent: function () {
        var me = this;
        me.callParent(arguments);
        me.addListener('afterrender', function (component) {
            if (component.readOnly === true) {
                component.setReadOnly(component.readOnly);
            }
        });
    },
    setReadOnly: function (readOnly) {

        var me = this;
        me.callParent(arguments);
        if (me.rendered) {
            if (readOnly) {
                me.inputEl.addCls(me.readOnlyInputCls);

            } else {
                me.inputEl.removeCls(me.readOnlyInputCls);
            }
        }
    }

});