Ext.define('BRAVO.override.form.field.vtype.Character',{
   override : 'Ext.form.field.VTypes',


    /**
     *  코드용 - 숫자,영어 대문자, - 만 입력가능
     *  vtype : engOnly
     * @param value
     * @param field
     * @returns {boolean}
     */
    codeOnly: function (value, field) {
        return /^[0-9-|A-Z]+$/.test(value);
    },
    codeOnlyText :'영문대문자와 숫자만 입력가능합니다.' ,

    
    /**
     *  영어 대소문자/ 숫자 가능
     *  vtype : engNumOnly
     * @param value
     * @param field
     * @returns {boolean}
     */
    engNumOnly: function (value, field) {
        return /^[0-9|A-Z|a-z]+$/.test(value);
    },
    engNumOnlyText :'영문과 숫자만 입력가능합니다.' ,

    /**
     *  영문만 입력가능
     *  vtype : engOnly
     * @param value
     * @param field
     * @returns {boolean}
     */
    engOnly: function (value, field) {
        return /^[a-z|A-Z]+$/.test(value);
    },
    engOnlyText :'영문과 공백만 입력가능합니다.' ,

    /**
     *  한글만 입력가능
     *  vtype : korOnly
     * @param value
     * @param field
     * @returns {boolean}
     */
    korOnly: function (value, field) {
        return /^[a-z|A-Z]+$/.test(value);
    },
    korOnlyText :'한글만 입력가능합니다.',

    /**
     *  숫자만 입력가능 - 넘버필드는 맨 앞에 0을 사용하지 못함.
     *  vtype : numOnly
     * @param value
     * @param field
     * @returns {boolean}
     */
    numOnly: function (value, field) {
        return /^[0-9]+$/.test(value);
    },
    numOnlyText :'숫자만 입력가능합니다.',
    
    /**
     *  숫자/대쉬만 입력가능
     *  vtype : numDashOnly
     * @param value
     * @param field
     * @returns {boolean}
     */
    numDashOnly: function (value, field) {
        return /^[0-9-]+$/.test(value);
    },
    numDashOnlyText :'숫자/하이픈만 입력가능합니다.',
    
    
    /**
     *  한글,영문,숫자,공백만 입력가능
     *  vtype : engKorNumberSpaceOnly
     * @param value
     * @param field
     * @returns {boolean}
     */
    engKorNumberSpaceOnly: function (value, field) {
        return /^[ㄱ-ㅎ|가-힣|a-z|A-Z|0-9\ \-]+$/.test(value);
    },
    engKorNumberSpaceOnlyText : '한글/영문/숫자/공백/대쉬만 입력가능합니다.',
        
    /**
     *  한글,영문,숫자,공백,콤마,대쉬 만 입력가능
     *  vtype : engKorNumberSpaceCommaOnly
     * @param value
     * @param field
     * @returns {boolean}
     */
    engKorNumberSpaceCommaOnly: function (value, field) {
        return /^[ㄱ-ㅎ|가-힣|a-z|A-Z|0-9\ \,\-]+$/.test(value);
    },
    engKorNumberSpaceCommaOnlyText : '한글/영문/숫자/공백/콤마/대쉬만 입력가능합니다.'
});