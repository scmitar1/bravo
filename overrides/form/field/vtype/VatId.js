Ext.define('BRAVO.override.form.field.vtype.VatId',{
   override : 'Ext.form.field.VTypes',



    /**
     * 사업자등록번호
     *
     * vtype : IpAddress
     *
     * @param value
     * @param field
     * @returns {boolean}
     */
    vatId: function (value, field) {

        if(!Ext.isNumber(value.substr(0,3))){
            return false;
        }

    },
    vatIdText :'1.0.0.0 ~ 254.255.255.255 범위내의 IP만 입력가능합니다.',
    vatIdMask :/[\d\.]/i,


});