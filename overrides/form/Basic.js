Ext.define('BRAVO.override.form.Basic', {
    override: 'Ext.form.Basic',
    trackResetOnLoad: true,
    setReadOnlyFields: function (readOnly, fields) {
        var me = this;

        if (!Ext.isArray(fields)) {
            fields = [];
            var keyMap = me.getValues();

            Ext.iterate(keyMap, function (key, value) {
                fields.push(key);
            });
        }

        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            me.findField(field).setReadOnly(readOnly);
        }

    },
    getValues: function(asString, dirtyOnly, includeEmptyText, useDataValues, isSubmitting) {
        var values  = {},
            fields  = this.getFields().items,
            fLen    = fields.length,
            isArray = Ext.isArray,
            field, data, val, bucket, name, f;

        for (f = 0; f < fLen; f++) {
            field = fields[f];
            if (!dirtyOnly || field.isDirty()) {
                data = field[useDataValues ? 'getModelData' : 'getSubmitData'](includeEmptyText, isSubmitting);

                if (Ext.isObject(data)) {
                    for (name in data) {
                        if (data.hasOwnProperty(name)) {
                            val = data[name];

                            if (includeEmptyText && val === '') {
                                val = field.emptyText || '';
                            }

                            if (!field.isRadio) {
                                // skipping checkbox null values since they have no contextual value
                                if(field.isCheckbox && val===null) {
                                    continue;
                                }
                                if (values.hasOwnProperty(name)) {
                                    bucket = values[name];

                                    if (!isArray(bucket)) {
                                        bucket = values[name] = [bucket];
                                    }

                                    if (isArray(val)) {
                                        values[name] = bucket.concat(val);
                                    } else {
                                        bucket.push(val);
                                    }
                                } else {
                                    values[name] = val;
                                }
                            } else {
                                values[name] = values[name] || val;
                            }
                        }
                    }
                }
            }
        }

        if (asString) {
            values = Ext.Object.toQueryString(values);
        }

        Ext.iterate(values,function(key,value){
           if(value==""){
               values[key]=null;
           }

        });

        return values;


    },
    resetClearFields: function () {
        var me = this;
        var valueObject = me.getValues();
        var fields = me.getFields().items;
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            var fieldName = field.getName();
            valueObject[fieldName] = field.value;
        }
        me.setValues(valueObject);
        me.reset();

    },
    getOriginalValues: function () {
        var me = this;
        var fields = me.getFields().items;
        var valueObject = {};

        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            valueObject[field.name] = field.originalValue;
        }
        return valueObject;
    }
});