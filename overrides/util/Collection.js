Ext.define('BRAVO.override.util.Collection', {
        override: 'Ext.util.Collection',
        _aggregators: {
            average: function (items, begin, end, property, root) {
                var n = end - begin;
                return n &&
                    this._aggregators.sum.call(this, items, begin, end, property, root) / n;
            },

            bounds: function (items, begin, end, property, root) {
                for (var value, max, min, i = begin; i < end; ++i) {
                    value = items[i];
                    value = (root ? value[root] : value)[property];

                    // First pass max and min are undefined and since nothing is less than
                    // or greater than undefined we always evaluate these "if" statements as
                    // true to pick up the first value as both max and min.
                    if (!(value < max)) { // jshint ignore:line
                        max = value;
                    }
                    if (!(value > min)) { // jshint ignore:line
                        min = value;
                    }
                }

                return [min, max];
            },

            count: function (items) {
                return items.length;
            },

            extremes: function (items, begin, end, property, root) {
                var most = null,
                    least = null,
                    i, item, max, min, value;

                for (i = begin; i < end; ++i) {
                    item = items[i];
                    value = (root ? item[root] : item)[property];

                    // Same trick as "bounds"
                    if (!(value < max)) { // jshint ignore:line
                        max = value;
                        most = item;
                    }
                    if (!(value > min)) { // jshint ignore:line
                        min = value;
                        least = item;
                    }
                }

                return [least, most];
            },

            max: function (items, begin, end, property, root) {
                var b = this._aggregators.bounds.call(this, items, begin, end, property, root);
                return b[1];
            },

            maxItem: function (items, begin, end, property, root) {
                var b = this._aggregators.extremes.call(this, items, begin, end, property, root);
                return b[1];
            },

            min: function (items, begin, end, property, root) {
                var b = this._aggregators.bounds.call(this, items, begin, end, property, root);
                return b[0];
            },

            minItem: function (items, begin, end, property, root) {
                var b = this._aggregators.extremes.call(this, items, begin, end, property, root);
                return b[0];
            },

            sum: function (items, begin, end, property, root) {
                for (var value, sum = 0, i = begin; i < end; ++i) {
                    value = items[i];
                    value = (root ? value[root] : value)[property];
                    sum += value;
                }

                return sum;
            },

            sumNll: function (items, begin, end, property, root) {
                for (var value, sum = 0, i = begin; i < end; ++i) {
                    value = items[i];
                    value = (root ? value[root] : value)[property];
                    value = Ext.isNumeric(value) ? Number(value) : 0;
                    sum += value;
                }

                return sum;
            }
        }
    },
    function () {
        var prototype = this.prototype;

        // Minor compat method
        prototype.removeAtKey = prototype.removeByKey;

        /**
         * This method is an alias for `decodeItems` but is called when items are being
         * removed. If a `decoder` is provided it may be necessary to also override this
         * method to achieve symmetry between adding and removing items. This is the case
         * for `Ext.util.FilterCollection' and `Ext.util.SorterCollection' for example.
         *
         * @method decodeRemoveItems
         * @protected
         * @since 5.0.0
         */
        prototype.decodeRemoveItems = prototype.decodeItems;

        Ext.Object.each(prototype._aggregators, function (name) {
            prototype[name] = function (property, begin, end) {
                return this.aggregate(property, name, begin, end);
            };

            prototype[name + 'ByGroup'] = function (property) {
                return this.aggregateByGroup(property, name);
            };
        });
    }
);