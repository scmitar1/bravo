Ext.define('BRAVO.window.SearchGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.bravosearchgridcontroller',
    
    init: function() {
        var me = this;    
        var genParams = this.getView().gridParams;
        var grid = this.getView().down('grid');
        
        this.getView().title = genParams.title;
        grid.setColumns(genParams.columns);
        this.onSelectRec = genParams.recordSelect;
        
        var newStore = Ext.create('Ext.data.Store', {
            proxy: {
                type: 'ajax',
                url: genParams.storeProps.url,
                reader: {
                    type: 'json',
                    rootProperty: 'users'
                },
                extraParams: genParams.storeProps.extraParams || {}
             },
             autoLoad: true,
             listeners: {
                 'load': function(store, rec){
                     me.originalRec = rec;
                 }
             }
        });
        grid.setStore(newStore);
    },
    
    searchText: function(btn){
        var text = this.getView().down('#searchField').getValue();
        if(text === ""){
            this.getView().down('grid').getStore().loadRecords(this.originalRec, false);
        } else {
            text = text.toLowerCase();
            var filtered = [];
            var keys = Ext.pluck(this.getView().down('grid').getColumns(), 'dataIndex');
            this.originalRec.filter(function(rec){
                for(var i=0; i<keys.length; i++){
                    if((rec.data[keys[i]]+"").toLowerCase().indexOf(text) > -1){
                        filtered.push(rec);
                        break;
                    }
                }
            });
            this.getView().down('grid').getStore().loadRecords(filtered, false);
        }
    }
    
});
