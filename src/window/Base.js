Ext.define('BRAVO.window.Base', {
    extend: 'Ext.window.Window',
    modal: true,
    closable: true,
    minimizable: true,
    maximizable: true,
    hidden: true,
    closeAction: 'destroy',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [],
    
    setupPopup: function (options) {
        //this.show();
        var me = this;
        var target = me.getController() || me;
        target.callbackScope = options.callBackScope ||options.callbackScope;
        var callbackFunc =  options.callBackFunc ||options.callbackFunc;
        if(Ext.isFunction(callbackFunc)){

            target.callBackFunc =function(){
                me.hide();
                Ext.callback(callbackFunc,target.callbackScope,arguments);
            };

            target.callbackFunc =function(){
                me.hide();
                Ext.callback(callbackFunc,target.callbackScope,arguments);
            };
        }

        Ext.apply(target,options);
        me.show();

        if(options.callbackScope && me.autoSize!==false){
            var masterView = options.callbackScope.getView()|| options.callbackScope;
            me.setSize(masterView.getSize());
            me.setPosition(masterView.getPosition());
        }

        if(options.mode){
            Ext.callback(target.initSetting, target, [options.mode,options.params]);
        }else{
            Ext.callback(target.initSetting, target, [options.params]);
        }
    },


    setCenterPosition : function(){
        var me = this;

        var windowWidth = me.getWidth()/2;
        var windowHeight = me.getHeight()/2;

        var browserWidth = window.innerWidth/2;
        var browserHeight = window.innerHeight/2;

        var width = browserWidth-windowWidth;
        var height = browserHeight-windowHeight;

        me.setPosition(width,height);
    }


});