Ext.define('BRAVO.window.ZipCodeWindow', {
    extend: 'BRAVO.window.Base',
    alias: 'widget.zipcode_popup',
    closeAction: 'hide',
    layout: {
        type: 'vbox',
        align : 'stretch'
    },
    hidden: true,
    width: 800,
    title : '우편번호 찾기',
    height: 500,
    initComponent : function(){
        var me = this;


        Ext.apply(me,{

            items: [{
                xtype : 'bravo_form',
                layout : 'column',
                defaults : {
                    columnWidth : 0.3,
                    labelWidth : 60,
                    margin : '5 5 5 0'

                },

                items : [{
                    xtype : 'textfield',
                    labelAlign : 'left',
                    columnWidth : 0.5,
                    width : 300,
                    emptyText: '번지입력 필수) (구)역삼동 635-2 (신)테헤란로 7길 22',
                    fieldLabel : '검색어',
                    name : 'keyword',
                    listeners: { 
                        scope: me,
                        specialKey: function(f, e) {
                            if (e.getKey() == e.ENTER) {
                                me.onBtnSearch();
                            }
                        }
                    }
                },{
                    xtype : 'container',
                    columnWidth : 0.2,
                    layout : {
                        type : 'hbox',
                        align : 'middle',
                        pack : 'end'
                    },
                    items : [{
                        xtype : 'bravo_button_search',
                        columnWidth : undefined,
                        width : 100,
                        handler : me.onBtnSearch,
                        scope : me
                    }]
                }]
            },{
                xtype: 'bravo_grid_base',
                usePagingToolbar : false,
                flex : 1,
                store : Ext.create('Ext.data.Store',{
                    pageSize : 100,
                    proxy : {
                        type : 'ajax',
                        url : '/system/getZipCode',
                        pageParam : 'currentPage',
                        reader: {
                            type: 'json',
                            totalProperty: 'results.common.totalCount',
                            rootProperty:  'results.juso'
                        }
                    },listeners : {
                        beforeload : function(store,operation){
                            //operation._params.currentPage=store.currentPage;
                            operation._params={
                                countPerPage : store.pageSize

                            };
                        }
                    }

                }),
                dockedItems : [{
                    xtype: 'pagingtoolbar',
                    displayInfo: true,
                    displayMsg: '총 데이터건수 {2}',
                    emptyMsg: "데이터가 존재하지 않습니다.",
                    dock: 'bottom'
                }],
                columns : [
                    {text : '우편번호'    , dataIndex : 'zipNo' , align : 'center'},
                    {text : '지번주소'    , dataIndex : 'jibunAddr' , flex :1},
                    {text : '도로명주소'    , dataIndex : 'roadAddr' , flex : 1}
                ],
                listeners: {
                    scope: me,
                    rowdblclick: function(grid , record) {
                        me.callbackFunc(record);
                    }
                }
            },{
                xtype : 'panel',
                layout : {
                    type : 'hbox',
                    align: 'middle',
                    pack: 'center'
                },
                defaults : {
                    margin : 5
                },
                items :[{
                    xtype : 'button',
                    text : '적용',
                    width : 100,
                    handler : function(button){

                        var rec = me.down('grid').getSelection()[0];

                        if(!rec){
                            BRAVO.Msg.alert('확인','대상을 선택하세요');
                            return;
                        }
                        me.callbackFunc(rec)
                    },scope : me
                },{
                    xtype : 'button',
                    width : 100,
                    text : '닫기',
                    handler : function(button){
                        me.hide();
                    }
                }]
            }]
        });

        me.callParent(arguments);

    },
    onBtnSearch : function(button){

        var me = this;
        var store = this.down('grid').getStore();

        store.getProxy().setExtraParams(me.down('form').getForm().getValues());
        store.currentPage=1;
        this.down('grid').getStore().load();
    },

    initSetting : function(){
        var me = this;
        me.down('form').getForm().reset();
        me.down('grid').getStore().removeAll();
        me.setCenterPosition();
    }
});