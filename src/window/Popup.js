Ext.define('BRAVO.window.Popup', {
    extend: 'Ext.panel.Panel',
    layout: { type: 'vbox', align: 'stretch' },
    closable: true,
    items: [],
    
    setupPopup: function (options) {
        var me = this;
        var target = me.getController() || me;
        target.callbackScope = options.callBackScope ||options.callbackScope;
        var callbackFunc =  options.callBackFunc ||options.callbackFunc;
        if(Ext.isFunction(callbackFunc)){
            target.callBackFunc =function(){
                me.hide();
                Ext.callback(callbackFunc,target.callbackScope,arguments);
            };

            target.callbackFunc =function(){
                me.hide();
                Ext.callback(callbackFunc,target.callbackScope,arguments);
            };
        }

        Ext.apply(target,options);
        
        if(options.mode){
            Ext.callback(target.initSetting, target, [options.mode,options.params]);
        }else{
            Ext.callback(target.initSetting, target, [options.params]);
        }
    }
});