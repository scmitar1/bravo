Ext.define('BRAVO.window.SearchGrid', {
    extend: 'Ext.window.Window',
    alias: 'widget.bravo_search_grid',
    controller: 'bravosearchgridcontroller',
    layout: 'fit',
    width: 700,
    height: 500,
    modal: true,
    title: '', // 1
    closeAction: 'hide',
    items: [{
        xtype: 'grid',
        listeners: {
            select: 'onSelectRec'
        },
        dockedItems: [{
            docked: 'top',
            margin: '5 5 5 5',
            xtype: 'container',
            layout: {
                type: 'hbox', pack: 'center'
            },
            items: [{
                xtype: 'textfield',
                itemId: 'searchField',
                fieldLabel: '검색어',
                labelAlign: 'right',
                labelWidth: 40
            }, {
                xtype: 'button',
                text: '검색',
                margin: '0 0 0 2',
                width: 80,
                iconCls: 'fa fa-search',
                handler: 'searchText'
            }]
        }]
    }]

});