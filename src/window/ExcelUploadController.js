Ext.define('BRAVO.window.ExcelUploadController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.bravowindowexcel',

    initSetting : function(params){


        if(!Ext.isArray(params.columns)){
            alert('params.columns 가 지정되지 않았습니다');
            return;
        }

        if(Ext.isEmpty(this.params.uploadConfig)){
            alert('params.uploadConfig 가 지정되지 않았습니다');
            return;
        }

        if(Ext.isEmpty(this.params.uploadConfig.url)){
            alert('params.uploadConfig.url 가 지정되지 않았습니다');
            return;
        }

        if(Ext.isEmpty(this.params.uploadConfig.method)){
            alert('params.uploadConfig.method 가 지정되지 않았습니다');
            return;
        }

        if(!Ext.isFunction(this.params.uploadConfig.paramFunc)){
            alert('params.uploadConfig.paramFunc 가 지정되지 않았습니다');
            return;
        }

        var field = this.getView().down('bravo_exceluploadfield');
        field.columnData=Ext.clone(params.columns);

        var grid = this.getView().down('grid');
        grid.getStore().removeAll();

        params.columns.unshift({xtype : 'rownumberer', text : '#', align : 'center', width : 38});
        grid.reconfigure(grid.getStore(),params.columns);




    },
    onBtnDownload : function(button){
        this.getView().down('grid').downloadDocument({
            type : 'excel',    //export alias
            title : 'Excel export',

            onlyExpandedNodes : false,
            showSummary : true,

            fileName : 'export.xlsx'

        });

    },
    onExcelLoad : function(field,excelData){
        var grid = this.getView().down('grid');
        grid.getStore().loadData(excelData);
    },

    onBtnSave : function(button){
        var grid = this.getView().down('grid');
        if(!grid.isValidData()){
            return false;
        }

        var gridDatas = grid.getSubmitDataList();

        var  params = this.params.uploadConfig.paramFunc(gridDatas);

        BRAVO.Ajax.request({
            url :this.params.uploadConfig.url,
            method : this.params.uploadConfig.method,
            params : params,
            successMsg : {
                title : '업로드 성공',
                message : '정상처리되었습니다.'
            },
            success : this.successSave,
            scope : this
        });
    },

    successSave : function(resObj){
      this.callbackFunc();
    },
    onBtnClose : function(button){
        this.getView().hide();
    }

});
