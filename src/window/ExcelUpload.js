Ext.define('BRAVO.window.ExcelUpload', {
    alias: 'widget.bravo_window_excelupload',
    extend: 'BRAVO.window.Base',
    controller: 'bravowindowexcel',
    layout: 'fit',
    width: 700,
    height: 500,
    items : [{
        xtype : 'container',
        layout : 'fit',
        items: [{
            xtype: 'bravo_grid_base',
            layout : 'fit',
            columns: [],
            usePagingToolbar: false,
            excelMode: true,
            store: Ext.create('Ext.data.Store')
        }]

    }],
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [{xtype: 'tbfill'},
            {
                xtype: 'bravo_button_download',
                handler: 'onBtnDownload'


            }, {
                xtype: 'bravo_exceluploadfield',
                listeners: {
                    excelload: 'onExcelLoad'
                }
            }
        ]
    }, {
        xtype: 'toolbar_bottom_button',
        bottomButtons: ['save', 'close'],
        dock: 'bottom'

    }]
});