Ext.define('BRAVO.window.DynamicGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.dynamic_grid_popup',
    initSetting: function (params) {
        var me = this;
        if(!Ext.isArray(params.columns)){
            alert('params.columns가 미지정되었습니다.');
            return;
        }
        if(!Ext.isArray(params.datas)){
            alert('params.data가 미지정되었습니다.');
            return;
        }
        var view = me.callbackScope.getView();

        me.getView().setSize(view.getSize());
        me.getView().setPosition(view.getPosition());


        var grid = me.getView().down('grid');
        grid.reconfigure(params.columns);
        grid.getStore().loadData(params.datas);
    }

});
