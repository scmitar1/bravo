Ext.define('BRAVO.window.ComboWindow', {
    extend: 'BRAVO.window.Base',
    alias : 'widget.combo_window',
    layout: 'fit',
    height : 500,
    width : 400,

    initComponent : function(){
        var me = this;

        Ext.apply(me,{
            dockedItems : [{
                xtype : 'toolbar',
                items : [{
                    xtype : 'bravo_button_save',
                    handler : me.onBtnSave,
                    scope : me
                },{
                    xtype : 'bravo_button_close',
                    handler : me.onBtnClose,
                    scope : me
                },{
                    xtype : 'textfield',
                    name : 'SEARCH',
                    scope : me,
                    listeners: {
                        change: me.onFilter        
                    }
                }]
            }],
            items: [{
                xtype : 'bravo_grid_base_multi',
                itemId : 'combowindowgrid',
                usePagingToolbar: false,
                columns : []
                
            }]

        });
        me.callParent(arguments);
    },



    initSetting : function(params){
        var grid = this.down('grid');
        grid.reconfigure(params.columns);
        grid.getStore().loadData(params.data);
        
        var store = grid.getStore();

        var filters = [
               new Ext.util.Filter({
                property: 'INACTIVE', 
                value: 'N'
               })
              ];
        
        store.clearFilter();
        store.filter(filters);
        
        var datas = params.checked;

        for(var i=0;i<datas.length;i++){
            var findIdx = grid.getStore().find(params.valueField,datas[i]);

            if(findIdx!=-1){
                grid.getStore().getAt(findIdx).set('isChecked',true);
            }
        }


    },
    onBtnSave : function(button){
        var datas  = this.down('grid').getCheckedData();
        var result = [];
        for(var i=0;i<datas.length;i++){
            result.push(datas[i].copy().data);
        }
        this.callbackFunc(result);
        this.hide();
    },
    onBtnClose : function(button){
        this.hide();
    },
    onFilter : function(me){

        var grid = Ext.ComponentQuery.query('grid[itemId="combowindowgrid"]')[0]  
        var store = grid.getStore();

        var filters = [
               new Ext.util.Filter({
                property: grid.columnManager.columns[2].dataIndex, 
                value: me.getValue(),
                 anyMatch: true,
                 caseSensitive: false
               })
              ];
        var filters1 = [
              new Ext.util.Filter({
               property: 'INACTIVE', 
               value: 'N'
              })
             ];
        
        store.clearFilter();
        store.filter(filters1);
        store.filter(filters);
        
    }
});