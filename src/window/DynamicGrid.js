Ext.define('BRAVO.window.DynamicGrid', {
    extend: 'BRAVO.window.Base',
    alias: 'widget.dynamic_grid_popup',
    controller: 'dynamic_grid_popup',
    closeAction: 'hide',
    layout: 'fit',

    hidden: true,
    width: 400,
    height: 500,
    items: [{
        xtype: 'bravo_grid_base',
        features: [{
            ftype: 'groupingsummary',
            groupHeaderTpl: '{columnName}: {name}'
        }],
        layout: 'fit'
    }]
});