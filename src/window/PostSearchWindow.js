Ext.define('BRAVO.window.PostSearchWindow', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.post_search_window',
    minWidth: 800, frame: false, layout: { type: 'vbox' }, 
    selectCallback : {}, // callback
    initComponent: function(){
        var me = this;
        Ext.apply( this, {
            items: [ me.getSearchForm(), me.getGrid() ]
        });
        this.callParent(arguments);
    },

    updatedItem: function(){
        this.down('#PostnewdbFormId').getStore().reload();
    },
    
    getGrid: function(){
        var me = this;
        return {
            xtype: 'grid', width: '100%', border: true, height: 400,
            store: Ext.create( 'Magic.store.PostZipCodes'),
            columns: [
                { xtype: 'rownumberer', width: 40, text: '#', filterable: false, sortable: false},
                { text: "우편번호", flex: 1, dataIndex: 'ZIPCODE', filterable: true},
                { text: "시/도", flex: 1.5, dataIndex: 'SIDO', filterable: true},
                { text: "구/군", flex: 1.2, dataIndex: 'GUGUN', filterable: true},
                { text: "읍/면", flex: 1.2, dataIndex: 'EUPMYEON', filterable: true},
                { text: "동/리", flex: 1.2, dataIndex: 'DONGRI', filterable: true},
                { text: "건물1", flex: 0.2, dataIndex: 'GUNMUL1NO', filterable: true},
                { text: "건물2", flex: 0.2, dataIndex: 'GUNMUL2NO', filterable: true},
                { text: "건물명", flex: 3, dataIndex: 'GUNMULNM', filterable: true},
                { text: "도로명", flex: 3, dataIndex: 'DORONM', filterable: true}
            ],
            listeners: {
                scope: this,
                itemclick: this.selectCallback
            }
        };
    },
    
    getSearchForm: function(){
        var me = this;
        return {
            xtype: 'form', width: '100%', bodyPadding: 5, height: 35, layout: 'hbox', border: false, 
            fieldDefaults: { labelAlign: 'center', labelSeparator : ' :' },
            items:[ {
                    xtype: 'radiogroup', width: 250, labelWidth: 70, margin: '0 50 0 0', fieldLabel: '검색항목', labelSeparator : ' :', labelAlign: 'right',
                    items: [ {boxLabel: '동/리', name: 'KEY_FIELD', inputValue: 'DONGRI', checked: true, width: '60px'},
                       {boxLabel: '도로명', name: 'KEY_FIELD', inputValue: 'DORONM', width: '60px'},
                       {boxLabel: '건물명', name: 'KEY_FIELD', inputValue: 'GUNMULNM', width: '60px'}]
                }, { 
                    xtype:'textfield', fieldLabel: '검색어', name:"KEY_FIELD_VALUE", labelWidth: 50, margin: '0 0 0 10', width: '180',
                    listeners: {
                            specialkey: function(field, e){
                                var me = this.up('postnewdbsearchwindow');
                                if (e.getKey() == e.ENTER) {
                                   var keyword =  field.up('form')
                                                                .getForm()
                                                                .findField( 'KEY_FIELD_VALUE')
                                                                .getValue();
                                   if( keyword === "" ){
                                       Ext.Msg.alert( 'Warning', '검색어를 입력해 주세요.');
                                   }else{
                                       me.down( 'postnewdblist').getStore().load({
                                               params:{
                                                   KEY_FIELD_VALUE: keyword,
                                                   KEY_FIELD : me.down( 'form')
                                                                       .down( 'radiogroup')
                                                                       .getChecked()[0]
                                                                       .inputValue
                                               }
                                           }
                                       );
                                   }
                                }
                            }
                        }
                }, {
                    xtype:'button', text:"찾기", width:70, margin: '0 0 0 30', iconCls: 'search', 
                    handler: function(){
                           var me = this.up('postnewdbsearchwindow');
                           var keyword = me.down('form').getForm().findField( 'KEY_FIELD_VALUE').getValue();
                               
                           if( keyword === "" ){
                               Ext.Msg.alert( 'Warning', '검색어를 입력해 주세요.');
                           }else{
                               me.down( 'postnewdblist').getStore().load({
                                       params:{
                                           KEY_FIELD_VALUE: keyword,
                                           KEY_FIELD : me.down( 'form')
                                                               .down( 'radiogroup')
                                                               .getChecked()[0]
                                                               .inputValue
                                       }
                                   }
                               );
                           }
                       }
                   }
               ]
        };
    }
});
