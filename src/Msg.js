Ext.define('BRAVO.Msg', {
    extend: 'Ext.Base',
    
    statics: {
        alert: function (title, msg, callbackFunc) {
            Ext.Msg.alert(title, msg, function (btn) {
                if(Ext.isFunction(callbackFunc)){
                    callbackFunc();
                }
            });
        },
        
        confirm: function (title, msg, callbackFunc) {
            var message= Ext.Msg.confirm(title, msg, function (btn) {
                if (btn === 'yes') {
                    if(Ext.isFunction(callbackFunc)){
                        callbackFunc();
                    }
                }
            });
            Ext.WindowMgr.bringToFront(message);
        },

        save: function (callbackFunc, scope) {
            Ext.Msg.confirm('확인', '저장하시겠습니까?', function (btn) {
                if (btn === 'yes') {
                    if(Ext.isFunction(callbackFunc)){
                        callbackFunc();
                    }
                }
            });
        },
        
        delete: function (callbackFunc, scope) {
            Ext.Msg.confirm('확인', '삭제하시겠습니까?', function (btn) {
                if (btn === 'yes') {
                    if(Ext.isFunction(callbackFunc)){
                        callbackFunc();
                    }
                }
            });
        },
        
        /**
         * 처리 성공시 메시지 뿌려준 후 자동으로 사라짐
         */
        promptOk: function( options) {
            Ext.toast({ html: 'Success.' , title: 'Information', align:'t'});
        },
        
        // 두글자 모두 대문자임
        OK: function() {
            Ext.toast({ html: 'Success.' , title: 'Information', align:'t'});
        },
        
        /**
         * 처리 오류시 뿌려주는 에러 메시지창
         */
        showErrorMessage: function(title, msg ) {
            Ext.MessageBox.show({
                title: title,
                msg: msg,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        }
        
    }

});