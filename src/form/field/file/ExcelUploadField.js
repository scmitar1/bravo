Ext.define('BRAVO.form.field.file.ExcelUploadField', {
    alias: 'widget.bravo_exceluploadfield',
    extend: 'Ext.form.field.File',
    text: '엑셀업로드',
    output: null,
    snapshot: null,
    columnData: null,
    getGridColumns: Ext.emptyFn,

    initComponent: function () {
        var me = this;
        me.callParent();

        me.on('afterrender', function (comp) {

            me.fireEvent('initGrid', comp);

            me.fileInputEl.dom.onclick = function (event) {
                me.fireEvent('click', me);
            };
            me.fileInputEl.dom.onchange = function (event) {


                if (me.columnData == null) {
                    alert('업로드 대상그리드 미지정 initGrid를 이용하여 대상그리드를 설정하시길 바랍니다.');
                    return;

                }
                var file = event.target.files[0];
                var reader = new FileReader();
                reader.onload = function (event) {
                    var data = event.target.result;
                    var wb = XLSX.read(data, {type: 'binary'});

                    var result = me.toJson(wb);
                    me.snapshot = result;
                    var gridDatas = me.convertUploadData(result);
                    me.fireEvent('excelload', me, gridDatas);
                };
                reader.readAsBinaryString(file);
            };
        });
    },
    toJson: function (workbook) {
        var me = this;
        var result = {};
        workbook.SheetNames.forEach(function (sheetName) {

            var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName],
                {
                    header: [
                    	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
                        '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
                        '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '60',
                        '61', '62', '63', '64', '65', '66', '67', '68', '69', '70', '71', '72', '73', '74', '75', '76', '77', '78', '79', '80',
                        '81', '82', '83', '84', '85', '86', '87', '88', '89', '90'
                    ]
               });
            
            if (roa.length > 0) {
                result = roa;
            }
            
        });
        me.output = result;
        return result;
    },
    convertUploadData: function (datas) {

        var me = this;
        var columns = me.columnData;
        var dataIndex = [];
        var text = {};

        for (var i = 0; i < columns.length; i++) {
            dataIndex.push(columns[i].dataIndex);
            text[i] = columns[i].text;
        }

        var startIndex = -1;
        for (var i = 0; i < this.output.length; i++) {

            if (Ext.encode(me.output[i]) == Ext.encode(text)) {
                startIndex = i + 1;
                break;
            }
        }

        var result = [];
        for (i = startIndex; i < me.output.length; i++) {
            var data = me.output[i];
            var tempObj = {};

            Ext.iterate(data, function (key, value) {

                tempObj[dataIndex[key]] = value;

            });
            result.push(tempObj);

        }
        return result;
    }
});