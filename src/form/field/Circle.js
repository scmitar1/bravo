Ext.define('BRAVO.form.field.Circle', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.circlefield',
    height : '100%',
    layout : 'fit',
    style : 'cursor: pointer',
    value : 'N',
    initComponent: function () {
        var me = this;

        var height = me.height;
        var lineheight= height;
        if(Ext.isString(me.fieldLabel) &&me.fieldLabel.search('<br>')!=-1){
            lineheight = 17;
        }
        var borderWidth = '0px';
        if(me.value=='Y'){
            borderWidth = '1px';
        }




        Ext.apply(me,{
            items : [{
                xtype : 'component',
                renderTpl :'<span class="circle" style="border-width:'+borderWidth+';font-size: 11px;height:'+height+'px;line-height:'+lineheight+'px;">'+me.fieldLabel+'</span>',
                listeners : {
                    render : function(c){
                        c.getEl().on({
                            click : function(){

                                var comp = me.down('textfield');

                                if(comp.readOnly!==true){
                                    var value = comp.getValue();

                                    comp.setValue(value=='Y' ? 'N' :'Y');
                                }

                            }
                        });

                    }
                }
            },{
                xtype : 'textfield',
                name : me.name,
                hidden : true,
                listeners : {
                    change : function(field){
                        me.fireEvent('change',field);

                        var circle = this.down('component').getEl().dom.childNodes[0].style;
                        if(field.getValue()=='Y'){

                            circle.borderWidth='1px'
                        }else{
                            circle.borderWidth='0px'
                        }
                    },
                    scope : me
                }
            }]
        });
        delete me.name;
        delete me.fieldLabel;
        me.callParent(arguments);
    }
});