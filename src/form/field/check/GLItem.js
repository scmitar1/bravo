Ext.define('BRAVO.form.field.check.GLItem', {
    extend: 'BRAVO.form.field.check.Base',
    alias: 'widget.bravo_check_glitem',
    checkItems: [
        {
            xtype: 'checkboxfield',
            boxLabel: '부서',
            name: 'EXT_DEPTCODE',
            inputValue: true,
            checked: true
        },{
            xtype: 'checkboxfield',
            boxLabel: '보조코드',
            name: 'EXT_SUBCODE',
            inputValue: true,
            checked: true
        },{
            xtype: 'checkboxfield',
            boxLabel: '거래처',
            name: 'EXT_BIZCODE',
            inputValue: true
        },{
            xtype: 'checkboxfield',
            boxLabel: '프로젝트',
            name: 'EXT_PROJECTCODE',
            inputValue: true
        },{
            xtype: 'checkboxfield',
            boxLabel: '재고코드',
            name: 'EXT_INVCODE',
            inputValue: true
        }
    ]
});
