Ext.define('BRAVO.form.field.radio.ACCTCurrency', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_acctcurrency',
    name: 'CURRENCY',
    title: '통화종류',
    radioItems: [
        {
            xtype: 'radiofield',
            boxLabel: '기능통화',
            inputValue: 'FCTYPE',
            checked: true
        },
        {
            xtype: 'radiofield',
            boxLabel: '보고통화',
            inputValue: 'RCTYPE'
        }
    ]
});
