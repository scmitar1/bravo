Ext.define('BRAVO.form.field.radio.Language', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_language',
    name: 'RPT_LANG',
    title: '조회결과 언어',
    hidden: false,
    radioItems: [
        {
            xtype: 'radiofield',
            boxLabel: '한글',
            inputValue: 'KOREAN',
            checked: true
        },
        {
            xtype: 'radiofield',
            boxLabel: 'English',
            inputValue: 'ENGLISH'
        }
    ]
});