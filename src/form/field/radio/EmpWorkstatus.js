Ext.define('BRAVO.form.field.radio.EmpWorkstatus', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_emp_workstatus',
    name: 'JOB_STATUS',
    reference: 'empWorkStatusType',
    title: '근무상태',

    radioItems: [
        {
            xtype: 'radiofield',
            boxLabel: '전체',
            inputValue: 'ALL',
            checked : true
        },{
            xtype: 'radiofield',
            boxLabel: '재직',
            inputValue: 'Y'
        },{
            xtype: 'radiofield',
            boxLabel: '퇴직',
            inputValue: 'N'
        }
    ],
    listeners: {
        change: {
            element: 'el', //bind to the underlying el property on the panel
            fn: 'onChangeEmpWorkStatusType'
        }
    }

});