Ext.define('BRAVO.form.field.radio.Personal', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_personal',
    name: 'PERSONAL_INFO_MASK',
    title : '개인정보 보호',
    radioItems: [
        {
            xtype: 'radiofield',
            boxLabel: 'Yes',
            inputValue: 'Y'
        },
        {
            xtype: 'radiofield',
            boxLabel: 'No',
            inputValue: 'N',
            checked: true
        }
    ]
});