Ext.define('BRAVO.form.field.radio.Inactive', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_inactive',
    title: '사용중지',
    maxLength:1,

    radioItems: [{
        xtype: 'radiofield',
        boxLabel: 'No',
        name: 'INACTIVE',
        inputValue: 'N',
        checked: true
    }, {
        xtype: 'radiofield',
        boxLabel: 'Yes',
        name: 'INACTIVE',
        inputValue: 'Y'
    }]
});