Ext.define('BRAVO.form.field.radio.OutFile', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_outfile',
    radioItems: [
        {
            xtype: 'radiofield',
            name: 'DSP',
            boxLabel: 'HTML',
            inputValue: 'HTML',
            checked: true
        },
/*        {
            xtype: 'radiofield',
            name: 'DSP',
            boxLabel: 'Excel',
            inputValue: 'EXL'
        },
*/        {
            xtype: 'radiofield',
            name: 'DSP',
            boxLabel: 'PDF',
            inputValue: 'PDF'
        }
    ]
});