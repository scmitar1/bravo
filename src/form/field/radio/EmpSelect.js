Ext.define('BRAVO.form.field.radio.EmpSelect', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_emp_select',
    reference: 'empSelectType',
    name: 'EMP_SEL',
    fieldLabel: '사원선택',

    radioItems: [
        {
            xtype: 'radiofield',
            boxLabel: '사원 전체',
            inputValue: 'ALL'
        },
        {
            xtype: 'radiofield',
            boxLabel: '선택된 인원',
            inputValue: 'SELECT',
            checked: true
        }
    ],
    listeners: {
        change: {
            element: 'el', //bind to the underlying el property on the panel
            fn: 'onChangeEmpSelectType'
        }
    }
});