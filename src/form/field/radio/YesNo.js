Ext.define('BRAVO.form.field.radio.YesNo', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_yesno',

    radioItems: [{
        xtype: 'radiofield',
        boxLabel: 'Yes',
        inputValue: 'Y'
    }, {
        xtype: 'radiofield',
        boxLabel: 'No',
        inputValue: 'N',
        checked: true
    }]
});