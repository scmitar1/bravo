Ext.define('BRAVO.form.field.radio.OutFileHtmlExcel', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_outfile_htmlexcel',
    name: 'DSP',
    title : '출력물',
    radioItems: [
        {
            xtype: 'radiofield',
            boxLabel: 'HTML',
            inputValue: 'HTML',
            checked: true
        },
        {
            xtype: 'radiofield',
            boxLabel: 'Excel',
            inputValue: 'EXL'
        }
    ]
});