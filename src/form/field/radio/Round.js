Ext.define('BRAVO.form.field.radio.Round', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_round',
    title: '끝자리',

    radioItems: [
        {
            xtype: 'radiofield',
            name: 'FRACTION',
            boxLabel: 'Round Off',
            inputValue: 'OFF',
            checked: true
        },{
            xtype: 'radiofield',
            name: 'FRACTION',
            boxLabel: 'Round Down',
            inputValue: 'DOWN'
        },{
            xtype: 'radiofield',
            name: 'FRACTION',
            boxLabel: 'Round Up',
            inputValue: 'UP'
        }
    ]
});