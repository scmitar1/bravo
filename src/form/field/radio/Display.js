Ext.define('BRAVO.form.field.radio.Display', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_display',
    radioItems: [
        {
            xtype: 'radiofield',
            boxLabel: 'HTML',
            name: 'DSP',
            inputValue: 'HTML',
            checked: true
        },
        {
            xtype: 'radiofield',
            boxLabel: 'EXCEL',
            name: 'DSP',
            inputValue: 'EXL2K'
/*
        },
        {
            xtype: 'radiofield',
            boxLabel: 'PDF',
            name: 'DSP',
            inputValue: 'PDF'
*/
        }
    ]
});
