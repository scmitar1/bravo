Ext.define('BRAVO.form.field.radio.GAAP', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_gaap',
    name: 'GAAP',
    fieldLabel: '원장',
    title: '회계 원장',
    radioItems: [
        {
            xtype: 'radiofield',
            boxLabel: '세무원장',
            inputValue: '1',
            checked: true
        },
        {
            xtype: 'radiofield',
            boxLabel: '관리원장',
            inputValue: '2'
        }/*,
        {
            xtype: 'radiofield',
            boxLabel: 'Global원장',
            inputValue: '3'
        }*/
    ]
});
