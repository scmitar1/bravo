Ext.define('BRAVO.form.field.radio.Workstatus', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_workstatus',
    radioItems: [
        {
            xtype: 'radiofield',
            name: 'WORK_STATUS',
            boxLabel: '전체',
            inputValue: 'ALL'
        },
        {
            xtype: 'radiofield',
            name: 'WORK_STATUS',
            boxLabel: '재직',
            inputValue: 'INC'
        },
        {
            xtype: 'radiofield',
            name: 'WORK_STATUS',
            boxLabel: '퇴직',
            inputValue: 'RET'
        }
    ]
});
