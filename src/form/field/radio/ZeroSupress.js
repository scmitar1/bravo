Ext.define('BRAVO.form.field.radio.ZeroSupress', {
    extend: 'BRAVO.form.field.radio.Base',
    alias: 'widget.bravo_radio_zero_supress',
    radioItems: [
        {
            xtype: 'radiofield',
            boxLabel: '출력 안함',
            name: 'ZEROSUPP',
            inputValue: 'YES',
            checked: true
        },
        {
            xtype: 'radiofield',
            boxLabel: '출력함',
            name: 'ZEROSUPP',
            inputValue: 'NO'
        }
    ]
});