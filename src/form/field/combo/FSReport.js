Ext.define('BRAVO.form.field.combo.FSReport', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_fsreport',
    valueField: 'FSREPORT_CODE',
    displayField: 'FSREPORT_NAM',
    FSREPORTCODE_TYPE: null,

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.acct.FSReports', {
                autoLoad: true,
                proxy: {
                    extraParams: {
                        FSREPORTCODE_TYPE: me.FSREPORTCODE_TYPE
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});