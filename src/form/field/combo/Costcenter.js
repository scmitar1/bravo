Ext.define('BRAVO.form.field.combo.Costcenter', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_costcenter',
    valueField: 'CCENTER_CODE',
    displayField: 'CCENTER_NAM',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.acct.Costcenters', {
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: '/acct/code/costcenter/list',
                    extraParams: {
                        ACTIVE_ONLY: 'YES'
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});