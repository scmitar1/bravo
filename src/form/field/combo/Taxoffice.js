Ext.define('BRAVO.form.field.combo.Taxoffice', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_taxoffice',
    valueField: 'TAXOFFICE_CD',
    displayField: 'TAXOFFICE_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.TaxOffices', {
                autoLoad: true,
                proxy: {
                    extraParams: {
                        INACTIVE: 'N'
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});