Ext.define('BRAVO.form.field.combo.statics.MenuAccess', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_menuaccess',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"A", "desc":"All Client"},
                    {"abbr":"S", "desc":"Special Client"}
                ]
            })
        });
        me.callParent(arguments);
    }

});