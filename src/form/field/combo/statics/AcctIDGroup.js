Ext.define('BRAVO.form.field.combo.statics.AcctIDGroup', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_acctidgroup',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"A", "desc":"Asset"},
                    {"abbr":"L", "desc":"Liabilities "},
                    {"abbr":"C", "desc":"Capital(Equity)"},
                    {"abbr":"R", "desc":"Revenue"},
                    {"abbr":"E", "desc":"Expense"},
                ]
            })
        });
        me.callParent(arguments);
    }

});