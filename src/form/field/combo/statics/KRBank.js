Ext.define('BRAVO.form.field.combo.KRBank', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_krbank',
    valueField: 'BANK_CODE',
    displayField: 'BANK_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.KRBanks', {
                autoLoad: true,
                proxy: {
                    extraParams:{
                        INACTIVE: 'N'
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});