Ext.define('BRAVO.form.field.combo.statics.ReceiptYear', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_receiptyear',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: '========',

    initComponent : function() {
        var me = this,
            y = Ext.Date.clearTime(new Date()).getFullYear(),
            r = 5, list = [], i = 0;

        for (; i < r; i++) {
            list.push({
                abbr: y - i,
                desc: y - i
            });
        }

        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : list
            })
        });
        me.callParent(arguments);
    }

});