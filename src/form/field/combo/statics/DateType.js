Ext.define('BRAVO.form.field.combo.statics.DateType', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_datetype',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"Y", "desc":"Year"},
                    {"abbr":"M", "desc":"Month"},
                    {"abbr":"D", "desc":"Day"}
                ]
            })
        });
        me.callParent(arguments);
    }

});

