Ext.define('BRAVO.form.field.combo.statics.PeriodOnh', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_periodonh',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"0", "desc":"당월(Current)"},
                    {"abbr":"-1", "desc":"전월(-1 Month)"},
                    {"abbr":"-2", "desc":"2개월전(-2 Month)"}
                ]
            })
        });
        me.callParent(arguments);
    }

});