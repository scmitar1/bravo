Ext.define('BRAVO.form.field.combo.statics.RoundType', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_roundtype',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"DOWN", "desc":"절사"},
                    {"abbr":"OFF", "desc":"반올림"},
                    {"abbr":"UP", "desc":"올림"}
                ]
            })
        });
        me.callParent(arguments);
    }

});