Ext.define('BRAVO.form.field.combo.statics.PayGaap', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_paygaap',
    fieldLabel: '원장',
    name: 'PAY_GAAP',
    valueField : 'abbr',
    displayField: 'desc',
    allowBlank: false,
    value: '1',

    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"1", "desc":"Primary"},
                    {"abbr":"2", "desc":"Secondary"}
                ]
            })
        });
        me.callParent(arguments);
    }

});