Ext.define('BRAVO.form.field.combo.statics.Currency', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_currency',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"KRW", "desc":"KRW"},
                    {"abbr":"USD", "desc":"USD"},
                    {"abbr":"EUR", "desc":"EUR"},
                    {"abbr":"CNY", "desc":"CNY"},
                    {"abbr":"JPY", "desc":"JPY"},
                    {"abbr":"GBP", "desc":"GBP"},
                    {"abbr":"AUD", "desc":"AUD"},
                    {"abbr":"CAD", "desc":"CAD"},
                    {"abbr":"SGD", "desc":"SGD"}
                ]
            })
        });
        me.callParent(arguments);
    }

});