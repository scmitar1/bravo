Ext.define('BRAVO.form.field.combo.statics.TimeUnit', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_timeunit',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"DAY", "desc":"Day"},
                    {"abbr":"HOUR", "desc":"Hour"},
                    {"abbr":"MINUTE", "desc":"Minute"}
                ]
            })
        });
        me.callParent(arguments);
    }

});