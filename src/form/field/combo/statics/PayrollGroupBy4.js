Ext.define('BRAVO.form.field.combo.statics.PayrollGroupBy4', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_payrollgroupby4',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"PIN", "desc":"PIN"},
                    {"abbr":"CCENTER", "desc":"Cost Center"}
                ]
            })
        });
        me.callParent(arguments);
    }

});