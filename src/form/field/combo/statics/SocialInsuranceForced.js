Ext.define('BRAVO.form.field.combo.statics.SocialInsuranceForced', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_socialinsuranceforced',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: '',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"D", "desc":"D - 표준"},
                    {"abbr":"N", "desc":"N - 공제안함"},
                    {"abbr":"Y", "desc":"Y - 강제공제"}
                ]
            })
        });
        me.callParent(arguments);
    }

});