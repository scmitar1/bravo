Ext.define('BRAVO.form.field.combo.statics.Year', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_year',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: '========',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"2015", "desc":"2015"},
                    {"abbr":"2016", "desc":"2016"},
                    {"abbr":"2017", "desc":"2017"},
                    {"abbr":"2018", "desc":"2018"},
                    {"abbr":"2019", "desc":"2019"},
                    {"abbr":"2020", "desc":"2020"}
                ]
            })
        });
        me.callParent(arguments);
    }

});