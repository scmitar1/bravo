Ext.define('BRAVO.form.field.combo.statics.Gaap', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_gaap',
    title: '회계 원장',
    fieldLabel: '원장',
    name: 'PAY_GAAP',
    
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"1", "desc":"세무원장"},
                    {"abbr":"2", "desc":"관리원장"}
                ]
            })
        });
        me.callParent(arguments);
    }

});