Ext.define('BRAVO.form.field.combo.statics.OTType', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_ottype',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"OTTYPE1", "desc":"OT Type1"},
                    {"abbr":"OTTYPE2", "desc":"OT Type2"},
                    {"abbr":"OTTYPE3", "desc":"OT Type3"},
                    {"abbr":"OTTYPE4", "desc":"OT Type4"},
                    {"abbr":"OTTYPE5", "desc":"OT Type5"},
                    {"abbr":"OTTYPE6", "desc":"OT Type6"},
                    {"abbr":"OTTYPE7", "desc":"OT Type7"},
                    {"abbr":"OTTYPE8", "desc":"OT Type8"},
                    {"abbr":"OTTYPE5", "desc":"OT Type9"}
                ]
            })
        });
        me.callParent(arguments);
    }

});