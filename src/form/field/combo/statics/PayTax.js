Ext.define('BRAVO.form.field.combo.statics.PayTax', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_paxtax',
    // title: '지급/공제 구분',
    // fieldLabel: '지급/공제 구분',
    // name: 'PAYTAX_FLAG',
    
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"PAY", "desc":"Pay"},
                    {"abbr":"TAX", "desc":"Tax/Deduct"}
                ]
            })
        });
        me.callParent(arguments);
    }

});