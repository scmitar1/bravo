Ext.define('BRAVO.form.field.combo.statics.MasterTransaction', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_mastertransaction',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"MASTER", "desc":"책정임금"},
                    {"abbr":"TRANSACTION", "desc":"급여대장"}
                ]
            })
        });
        me.callParent(arguments);
    }

});