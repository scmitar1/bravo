Ext.define('BRAVO.form.field.combo.statics.Previous', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_previous',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: '====',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"1", "desc":"1"},
                    {"abbr":"2", "desc":"2"},
                    {"abbr":"3", "desc":"3"},
                    {"abbr":"4", "desc":"4"},
                    {"abbr":"5", "desc":"5"},
                    {"abbr":"6", "desc":"6"},
                    {"abbr":"7", "desc":"7"},
                    {"abbr":"8", "desc":"8"},
                    {"abbr":"9", "desc":"9"},
                    {"abbr":"10", "desc":"10"},
                    {"abbr":"11", "desc":"11"},
                    {"abbr":"12", "desc":"12"}
                ]
            })
        });
        me.callParent(arguments);
    }

});