Ext.define('BRAVO.form.field.combo.statics.PayrollGroupBy5', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_payrollgroupby5',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"EMP_NUMBER", "desc":"PIN"},
                    // {"abbr":"EMP_NAME", "desc":"사원명(한글)"},
                    // {"abbr":"EMP_ENAME", "desc":"Emp Name(English)"},
                    {"abbr":"HRDEPT_NAMK", "desc":"부서명 (한글)"},
                    {"abbr":"HRDEPT_NAME", "desc":"Dept.Name (English)"}
                ]
            })
        });
        me.callParent(arguments);
    }

});