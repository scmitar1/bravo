Ext.define('BRAVO.form.field.combo.statics.Calculate', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_calculate',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"", "desc":""},
                    {"abbr":"+", "desc":"+"},
                    {"abbr":"-", "desc":"-"},
                    {"abbr":"*", "desc":"*"},
                    {"abbr":"/", "desc":"/"}
                ]
            })
        });
        me.callParent(arguments);
    }

});