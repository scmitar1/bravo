Ext.define('BRAVO.form.field.combo.statics.ClientDBType', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_clientdbtype',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"SMALL", "desc":"Small"},
                    {"abbr":"MIDDLE", "desc":"Middle"},
                    {"abbr":"BIG", "desc":"Big"},
                    {"abbr":"EXTRA", "desc":"Extra Large"}
                ]
            })
        });
        me.callParent(arguments);
    }

});