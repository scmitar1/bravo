Ext.define('BRAVO.form.field.combo.statics.PayrollGroupBy1', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_payrollgroupby1',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"PIN", "desc":"PIN"},
                    {"abbr":"DEPT", "desc":"Department"}
                ]
            })
        });
        me.callParent(arguments);
    }

});