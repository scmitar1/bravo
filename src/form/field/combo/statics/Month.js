Ext.define('BRAVO.form.field.combo.statics.Month', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_month',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: '====',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"01", "desc":"01"},
                    {"abbr":"02", "desc":"02"},
                    {"abbr":"03", "desc":"03"},
                    {"abbr":"04", "desc":"04"},
                    {"abbr":"05", "desc":"05"},
                    {"abbr":"06", "desc":"06"},
                    {"abbr":"07", "desc":"07"},
                    {"abbr":"08", "desc":"08"},
                    {"abbr":"09", "desc":"09"},
                    {"abbr":"10", "desc":"10"},
                    {"abbr":"11", "desc":"11"},
                    {"abbr":"12", "desc":"12"}
                ]
            })
        });
        me.callParent(arguments);
    }

});