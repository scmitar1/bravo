Ext.define('BRAVO.form.field.combo.statics.YesNo', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_yesno',
    valueField : 'abbr',
    displayField: 'desc',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',
    emptyText: 'Please select one',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'desc'],
                data : [
                    {"abbr":"Y", "desc":"Yes"},
                    {"abbr":"N", "desc":"No"}
                ]
            })
        });
        me.callParent(arguments);
    }

});