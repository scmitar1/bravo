Ext.define('BRAVO.form.field.combo.Taxcode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_taxcode',
    valueField: 'TAXITM_CD',
    displayField: 'TAX_NAME',
    PAYTAX_FLAG: null,

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.payroll.TaxCodes', {
                autoLoad: true,
                proxy: {
                    extraParams:{
                        INACTIVE: 'N'
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});