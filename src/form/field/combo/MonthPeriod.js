Ext.define('BRAVO.form.field.combo.MonthPeriod', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.bravo_combo_month_period',
    border : false,
    padding : 0,
    layout : {
        type : 'hbox',
        align : 'bottom'
    },
    props : {
        START_YEAR : null,
        START_MONTH :null,
        END_YEAR :null,
        END_MONTH :null
    },
    initComponent: function () {
        var me = this;

        if(Ext.isEmpty(me.props.START_YEAR)) {
            alert('bravo_combo_month_period 의 props.START_YEAR 미입력');

        }else if(Ext.isEmpty(me.props.START_MONTH)){
            alert('bravo_combo_month_period 의 props.START_MONTH 미입력');

        }else if(Ext.isEmpty(me.props.END_YEAR)){
            alert('bravo_combo_month_period 의 props.END_YEAR 미입력');

        }else if(Ext.isEmpty(me.props.END_MONTH)){
            alert('bravo_combo_month_period 의 props.END_MONTH 미입력');
        }


        Ext.apply(me, {
            items : [
                {
                    xtype : 'bravo_combo_year', flex :1,
                    name : me.props.START_YEAR
                },
                {xtype : 'bravo_combo_month', flex :1,
                    name : me.props.START_MONTH
                },
                {
                    xtype : 'displayfield',
                    value : '&nbsp;~&nbsp;',
                    width: 15
                },
                {xtype : 'bravo_combo_year', flex :1,
                    name : me.props.END_YEAR
                },
                {xtype : 'bravo_combo_month', flex :1,
                    name : me.props.END_YEAR
                }
            ]
        });
        me.callParent(arguments);
    }
});