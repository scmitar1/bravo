Ext.define('BRAVO.form.field.combo.HrDeptcode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_hrdeptcode',
    valueField : 'HRDEPT_CODE',
    displayField : 'HRDEPT_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.hrm.HrDeptCodes', {
                autoLoad: true,
                proxy: {
                    extraParams:{
                        INACTIVE: 'N'
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});