Ext.define('BRAVO.form.field.combo.Code', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_code',
    valueField: 'REAL_CODE',
    displayField: 'CODE_NAME',
    queryMode: 'local',
    typeAhead: false,
    triggerAction: 'all',
    isShowCode : true,
    //value: '',
    allowAll : true,

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            store: Ext.create('Ext.data.Store', {
                fields: [
                    {name: 'SUPER_CODE', type: 'string'},
                    {name: 'REAL_CODE', type: 'string'},
                    {name: 'CODENAMK', type: 'string'},
                    {name: 'CODENAME', type: 'string'}
                ],
                autoLoad : true,
                remoteSort: true,
                single: true,
                proxy: {
                    type: 'ajax',
                    url: '/system/setting/mastcode/list',
                    actionMethods: {
                        create: 'POST', read: 'GET', update: 'POST', destroy: 'POST'
                    },
                    extraParams: {
//                        OPER_LANG: 'KOREAN',
                        SUPER_CODE: me.superCode,
                        INACTIVE: 'N'
                    },
                    reader: {
                        type: 'json',
                        rootProperty: 'list',
                        totalProperty: 'totalCount'
                    },
                    simpleSortMode: true
                }
            })
        });
        me.callParent(arguments);
    }
});