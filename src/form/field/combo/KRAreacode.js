Ext.define('BRAVO.form.field.combo.KRAreacode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_krareacode',
    valueField: 'AREA_CODE',
    displayField: 'AREA_NAM',
    AREA_LEVEL: null,

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.KRAreaCodes', {
                proxy: {
                    extraParams:{
                        AREA_LEVEL: this.AREA_LEVEL,
                        INACTIVE: "N"
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});