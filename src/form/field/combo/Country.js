Ext.define('BRAVO.form.field.combo.Country', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_country',
    valueField: 'CODE_2DIGIT',
    displayField: 'COUNTRY_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.Countries', {
                autoLoad: true,
                proxy: {
                    extraParams:{
                        SHOW_FLAG: 'Y'
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});