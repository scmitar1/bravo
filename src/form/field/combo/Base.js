Ext.define('BRAVO.form.field.combo.Base', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_base',
    isShowCode: true,
    matchFieldWidth: false,
    valueField: 'REAL_CODE',
    displayField: 'CODE_NAME',
    typeAhead: false,
    triggerAction: 'all',
    queryMode: 'local',
    allowAll: false,
    storeProps: {
        url: null,
        method: null,
        rootProperty: null,
        extraParams: null,
        params: null
    },
    onComboStoreLoad: Ext.emptyFn,
    onComboStoreException: Ext.emptyFn,
    superCode: null,


    initComponent: function () {
        var me = this;

        var store = Ext.create('Ext.data.Store', {

            //   autoLoad: me.storeProps.autoLoad,
            autoLoad: false,
            proxy: {
                type: me.storeProps.url == null ? 'memory' : 'ajax',
                url: me.storeProps.url,//'/payroll/code/pay-date/list',
                method: me.storeProps.method,
                extraParams: me.storeProps.extraParams,
                params: me.storeProps.params,
                reader: {
                    type: 'json',
                    rootProperty: Ext.isEmpty(me.storeProps.rootProperty) ? 'list' : me.storeProps.rootProperty
                }
            }
        });
        if (!me.store) {
            Ext.apply(me, {
                store: store
            });
        }

        if (me.isShowCode === true) {
            Ext.apply(me, {
                tpl: Ext.create('Ext.XTemplate',
                    '<tpl for="."><div class="x-boundlist-item">{' + me.valueField + '} - {' + me.displayField + '}</div></tpl>'
                ),
                displayTpl: Ext.create('Ext.XTemplate',
                    [
                        '<tpl for=".">{[this.getFormat(values)]}</tpl>',
                        {
                            getFormat: function (data) {
                                if (
                                    data
                                    && !Ext.isEmpty(Ext.String.trim(data[me.valueField]))
                                    && !Ext.isEmpty(Ext.String.trim(data[me.displayField]))
                                ) {
                                    return Ext.String.format('{0} - {1}', data[me.valueField], data[me.displayField]);
                                }
                                return '';
                            }
                        }
                    ]
                )
            });
        }

        Ext.apply(me, {
            listeners: {
                expand: function (field) {
                    var picker = field.getPicker();

                    if (field.getStore().getCount() == 0) {
                        return false;
                    }

                    if (!field.pickerWidth) {
                        field.pickerWidth = picker.getWidth();
                    }
                    var fieldWidth = field.bodyEl.getWidth();
                    var pickerWidth = field.pickerWidth;
                    picker.setWidth(pickerWidth < fieldWidth ? fieldWidth : pickerWidth);
                },
                afterrender: function (comp) {
                    comp.getStore().load();
                },
                select: function (field) {
                    field.getStore().clearFilter();

                    //기존 검색된결과값이 없을때 보이는 메시지를 제거함
                    var findIdx = field.getStore().find(field.valueField, 'NOT_FOUND_DATA');
                    if (findIdx != -1) {
                        field.getStore().removeAt(findIdx);
                    }

                    if (field.getValue() == 'NOT_FOUND_DATA') {
                        field.setValue('');
                    }
                }
            }
        });
        me.callParent(arguments);


        me.getStore().on('load', function (thisStore) {
            if (Ext.isString(me.superCode)) {
                var arr = [];
                for (var i = 0; i < thisStore.getCount(); i++) {
                    var rec = thisStore.getAt(i);
                    if (rec.get('SUPER_CODE') != me.superCode) {
                        arr.push(rec);
                    }
                }
                thisStore.remove(arr);
            }

            //전체 부분 추가하기
            if (me.allowAll === true) {
                var allowObj = {};
                allowObj[me.valueField] = '';
                allowObj[me.displayField] = 'Please select one';
                thisStore.insert(0, allowObj);
            }
        });

        me.getStore().on('exception', function (proxy, response, operation) {
            var responseData = Ext.decode(response.responseText);
            Ext.MessageBox.show({
                title: 'ERROR',
                msg: responseData.msg,
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });

        });
    },

    doLocalQuery: function () {
        var field = this;

        var fieldValue = field.getValue();

        //필터를 모두 제거함
        field.getStore().clearFilter();


        //기존 검색된결과값이 없을때 보이는 메시지를 제거함
        var findIdx = field.getStore().find(field.valueField, 'NOT_FOUND_DATA');
        if (findIdx != -1) {
            field.getStore().removeAt(findIdx);
        }

        if (Ext.isEmpty(field.getValue())) {
            return true;
        }
        //검색조건
        field.getStore().filterBy(function (record) {

            var dispValue = record.get(field.displayField);
            var codeValue = record.get(field.valueField);
            var compareValue = codeValue + ' - ' + dispValue;

            // 실제 Store내의 값과 입력한문자열이 포함되는지
            // 하단 search안에 파라미터의 정규식 작업을 한다면 초성검색도 가능함.
            // ex) ㅎㄱ 타이핑시 한글문서가 나오게끔도 할수 있음

            if (codeValue == 'NOT_FOUND_DATA' || compareValue.includes(fieldValue)) {
                return true;
            } else {
                return false;
            }
        });


        // 검색된결과값이 없을때 보여주는 메시지
        if (field.getStore().getCount() == 0) {

            var displayField = field.displayField;
            var valueField = field.valueField;
            var temp = {};

            temp[displayField] = '조회된 데이터가 존재하지 않습니다.';
            temp[valueField] = 'NOT_FOUND_DATA';

            field.getStore().add(temp);
        }
        field.expand();
    },

    validator: function (val) {
        var me = this;

        if (me.allowBlank) {
            return true;
        }

        return Ext.isEmpty(me.getValue()) ? me.blankText : true;
    },

    onTriggerClick: function (e) {
        var me = this;
        if (me.isExpanded) {
            me.collapse();
        } else {
            me.expand();
        }
    }

});