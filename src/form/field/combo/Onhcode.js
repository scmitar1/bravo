Ext.define('BRAVO.form.field.combo.Onhcode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_onhcode',
    name: 'ONH_CD',
    valueField : 'ONH_CD',
    displayField : 'ONH_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.hrm.OnhCodes', {
                autoLoad: true,
                proxy: {
                    extraParams: {
                        ACTIVE_ONLY: 'YES'
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});