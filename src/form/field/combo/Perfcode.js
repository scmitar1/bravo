Ext.define('BRAVO.form.field.combo.Perfode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_perfcode',
    name: 'PERF_CD',
    valueField : 'PERF_CD',
    displayField : 'PERF_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            // store: getPerfCode(),
            store: Ext.create('Magic.store.payroll.PerfCodes', {
                autoLoad: true,
                proxy: {
                    extraParams:{
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});