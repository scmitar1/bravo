Ext.define('BRAVO.form.field.combo.Paycode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_paycode',
    valueField: 'PAY_CODE',
    displayField: 'PAY_NAM',
    PAYTAX_FLAG: null,

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.payroll.PayCodes', {
                autoLoad: true,
                proxy: {
                    extraParams: {
                        PAYTAX_FLAG: me.PAYTAX_FLAG
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});