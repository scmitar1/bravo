Ext.define('BRAVO.form.field.combo.Leavecode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_leavecode',
    name: 'LEAVE_CD',
    valueField : 'LEAVE_CD',
    displayField : 'LEAVE_NAM',

    initComponent : function() {
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.hrm.LeaveCodes', {
                autoLoad: true,
                proxy: {
                    extraParams: {
                        ACTIVE_ONLY: 'YES'
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});