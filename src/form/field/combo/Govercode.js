Ext.define('BRAVO.form.field.combo.Govercode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_govercode',
    valueField: 'GOVERCODE',
    displayField: 'GOVER_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.GoverCodes', {
                proxy: {
                    extraParams:{
                        // PAYTAX_FLAG: me.PAYTAX_FLAG
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});