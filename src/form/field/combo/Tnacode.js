Ext.define('BRAVO.form.field.combo.Tnacode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_tnacode',
    name: 'TNA_CD',
    valueField : 'TNA_CD',
    displayField : 'TNA_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.hrm.Tnacodes', {
                autoLoad: true,
                proxy: {
                    extraParams: {
                        ACTIVE_ONLY: 'YES',
                        VACAT_TYPE: 'VACAT'
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});