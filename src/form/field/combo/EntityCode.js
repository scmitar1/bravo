Ext.define('BRAVO.form.field.combo.EntityCode', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_entity',
    valueField: 'ENTITY_CODE',
    displayField: 'CLI_NAME',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            store: Ext.create('Ext.data.Store', {
                pageSize: 100,
                remoteSort: true,
                autoLoad: false,
                single: true,
                proxy: {
                    type: 'ajax',
                    url: '/system/setting/entity-mgt/list',
                    actionMethods: {
                        create: 'POST', read: 'GET', update: 'POST', destroy: 'POST'
                    },
                    reader: {
                        type: 'json',
                        rootProperty: 'list',
                        totalProperty: 'totalCount'
                    },
                    simpleSortMode: true
                },
                listeners: {
                    exception: function (proxy, response, operation) {
                        var responseData = Ext.decode(response.responseText);
                        Ext.MessageBox.show({
                            title: 'ERROR',
                            msg: responseData.msg,
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});