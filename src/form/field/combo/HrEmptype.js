Ext.define('BRAVO.form.field.combo.HrEmptype', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_hremptype',
    valueField : 'EMPTYPE_CD',
    displayField : 'EMPTYPE_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.hrm.HrEmpTypes', {
                autoLoad: true,
                proxy: {
                    extraParams:{
                        INACTIVE: 'N'
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});