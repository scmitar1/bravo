Ext.define('BRAVO.form.field.combo.FormNum', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.bravo_combo_formnum',
    valueField : 'FORM_NUM',
    displayField : 'FORM_NAME',
    typeAhead: false,
    editable: false,
    triggerAction: 'all',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            // store: getPayCode(),
            store: Ext.create('Ext.data.Store', {
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: '/hr/contract/contractform/code',
                    extraParams:{
                    }
                }
            })/*,,
            tpl: Ext.create('Ext.XTemplate',
                '<tpl for="."><div class="x-boundlist-item">{' + me.valueField + '} - {' + me.displayField + '}</div></tpl>'
            )
            displayTpl: Ext.create('Ext.XTemplate',
                '<tpl for=".">{' + me.valueField + '} - {' + me.displayField + '}</tpl>'
            )*/
        });
        me.callParent(arguments);
    }
});
