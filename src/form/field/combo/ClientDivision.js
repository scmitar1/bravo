Ext.define('BRAVO.form.field.combo.ClientDivision', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_clidivision',
    valueField: 'DV_VATID',
    displayField: 'DV_NAME',
    store : 'CliDivisions'

});