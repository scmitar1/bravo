Ext.define('BRAVO.form.field.combo.Usergroup', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_usergroup',
    valueField : 'UGROUP_CODE',
    displayField : 'UGROUP_NAM',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            // store: getPayCode(),
            store: Ext.create('Magic.store.admin.UserGroups', {
                autoLoad: true,
                proxy: {
                    extraParams:{
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});