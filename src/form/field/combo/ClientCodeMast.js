Ext.define('BRAVO.form.field.combo.ClientCodeMast', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_client_code_mast',
    valueField: 'REAL_CODE',
    displayField: 'CODE_NAME',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            store: Ext.create('Ext.data.Store', {
                fields: [
                    {name: 'SUPER_CODE', type: 'string'},
                    {name: 'REAL_CODE', type: 'string'},
                    {name: 'CODENAMK', type: 'string'},
                    {name: 'CODENAME', type: 'string'}
                ],
                pageSize: 500,
                remoteSort: true,
                autoLoad: true,
                single: true,
                proxy: {
                    type: 'ajax',
                    url: '/hr/code/client_codemast/list',
                    actionMethods: {
                        create: 'POST', read: 'GET', update: 'POST', destroy: 'POST'
                    },
                    extraParams: {
                        SUPER_CODE: me.superCode,
                        INACTIVE: 'N'
                    },
                    reader: {
                        type: 'json',
                        rootProperty: 'list',
                        totalProperty: 'totalCount'
                    },
                    simpleSortMode: true
                },
                listeners: {
                    load: function (store, records, options) {
                        store.insert(0,{REAL_CODE: '', CODE_NAME: 'Please select one'});
                    },
                    exception: function (proxy, response, operation) {
                        var responseData = Ext.decode(response.responseText);
                        Ext.MessageBox.show({
                            title: 'ERROR',
                            msg: responseData.msg,
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});