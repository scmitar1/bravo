Ext.define('BRAVO.form.field.combo.ClientLTaxFile', {
    extend: 'BRAVO.form.field.combo.Base',
    alias: 'widget.bravo_combo_cliltaxfile',
    valueField: 'FILE_CODE',
    displayField: 'FILE_UNITNAME',

    initComponent : function(){
        var me = this;
        Ext.apply(me,{
            store: Ext.create('Magic.store.admin.CliLTaxFiles', {
                autoLoad: true,
                proxy: {
                    extraParams:{
                    }
                }
            })
        });
        me.callParent(arguments);
    }

});