Ext.define('BRAVO.form.field.radiogroup', {
    extend: 'Ext.form.RadioGroup',
    xtype: 'bravo_booleangroup',
    vertical: false,
    isFormField: false,
    initComponent: function () {
        Ext.apply(this, {
            items: [
                {
                    boxLabel: 'Yes',
                    name: this.name,
                    checked: this.value === 'Y',
                    inputValue: 'Y'
                },
                {
                    boxLabel: 'No',
                    name: this.name,
                    checked: this.value === 'N',
                    inputValue: 'N'
                }
            ]
        });
        this.callParent(arguments);
    } 
});