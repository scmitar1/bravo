Ext.define('BRAVO.form.field.MultiPopupField', {
    extend: 'Ext.form.field.Tag',
    xtype: 'multipopupfield',
    config: {
        enableSupple: false,    // 팝업에서 선택시 (true: 기존 선택된 항목에 추가, false: 이전값을 지우고 추가)
        enableClear: false,     // clear trigger 사용여부
        pop: {
            winTitle: '상세 조회',
            columns: [],
            width: 600,
            height: 400,
            store: null,
            storeProps: null
        }
    },
    triggerCls: 'x-form-search-trigger',
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            hidden: true,
            handler: function (fld) {
                fld.clearValue();
            }
        }
    },
    growMax: 100,
    store: {
        fields: [],
        data: []
    },
    onDownArrow: Ext.emptyFn,
    onEsc: Ext.emptyFn,

    //@formatter:off
    fieldSubTpl: [
        // listWrapper div is tabbable in Firefox, for some unfathomable reason
        '<div id="{cmpId}-listWrapper" data-ref="listWrapper"' + (Ext.isGecko ? ' tabindex="-1"' : ''),
            '<tpl foreach="ariaElAttributes"> {$}="{.}"</tpl>',
            ' class="' + Ext.baseCSSPrefix + 'tagfield {fieldCls} {typeCls} {typeCls}-{ui}"<tpl if="wrapperStyle"> style="{wrapperStyle}"</tpl>>',
            '<span id="{cmpId}-selectedText" data-ref="selectedText" aria-hidden="true" class="' + Ext.baseCSSPrefix + 'hidden-clip"></span>',
            '<ul id="{cmpId}-itemList" data-ref="itemList" role="presentation" class="' + Ext.baseCSSPrefix + 'tagfield-list{itemListCls}">',
                '<li id="{cmpId}-inputElCt" data-ref="inputElCt" role="presentation" class="' + Ext.baseCSSPrefix + 'tagfield-input" style="width: 0px !important;">',
                    '<input id="{cmpId}-inputEl" data-ref="inputEl" type="{type}" ',
                    'readOnly="true" ',
                    '<tpl if="name">name="{name}" </tpl>',
                    '<tpl if="value"> value="{[Ext.util.Format.htmlEncode(values.value)]}"</tpl>',
                    '<tpl if="size">size="{size}" </tpl>',
                    '<tpl if="tabIdx != null">tabindex="{tabIdx}" </tpl>',
                    '<tpl if="disabled"> disabled="disabled"</tpl>',
                    '<tpl foreach="inputElAriaAttributes"> {$}="{.}"</tpl>',
                    'class="' + Ext.baseCSSPrefix + 'tagfield-input-field {inputElCls} {emptyCls} {fixCls}" autocomplete="off">',
                '</li>',
            '</ul>',
            '<ul id="{cmpId}-ariaList" data-ref="ariaList" role="listbox"',
                '<tpl if="ariaSelectedListLabel"> aria-label="{ariaSelectedListLabel}"</tpl>',
                '<tpl if="multiSelect"> aria-multiselectable="true"</tpl>',
                ' class="' + Ext.baseCSSPrefix + 'tagfield-arialist">',
            '</ul>',
          '</div>',
        {
            disableFormats: true
        }
    ],
    //@formatter:on

    initComponent: function () {
        this.callParent();
        if (this.getEnableClear()) {
            this.getTrigger('clear').setHidden(false);
        }
    },

    onTriggerClick: function () {
        if (this.config.pop) {
            this.createWindow();
        }
    },
    // Ext.form.field.Tag.onItemListClick override
    onItemListClick: function (e) {
        var me = this,
            selectionModel = me.selectionModel,
            itemEl = e.getTarget(me.tagItemSelector),
            closeEl = itemEl ? e.getTarget(me.tagItemCloseSelector) : false;
        if (me.readOnly || me.disabled) {
            return;
        }
        e.stopPropagation();
        if (itemEl) {
            if (closeEl) {
                me.removeByListItemNode(itemEl);
                if (me.valueStore.getCount() > 0) {
                    me.fireEvent('select', me, me.valueStore.getRange());
                }
            } else {
                me.toggleSelectionByListItemNode(itemEl, e.shiftKey);
            }
            // If not using touch interactions, focus the input
            if (!Ext.supports.TouchEvents) {
                me.inputEl.focus();
            }
        } else {
            if (selectionModel.getCount() > 0) {
                selectionModel.deselectAll();
            }
            me.inputEl.focus();
            // if (me.triggerOnClick) {
            //     me.onTriggerClick();
            // }
        }
    },

    createWindow: function () {
        var me = this;
        var pop = this.config.pop;

        return Ext.widget({
            xtype: 'window',
            title: pop.winTitle,
            autoShow: true,
            caller: me,
            width: pop.width,
            height: pop.height,
            modal: true,
            layout: 'fit',
            closable: true,
            maximizable: true,
            items: [
                {
                    xtype: 'bravo_grid_base',
                    viewConfig: {
                        enableTextSelection: false
                    },
                    rowNumberer: false,
                    selModel: {
                        type: 'checkboxmodel',
                        mode: 'multi'
                    },
                    columns: pop.columns,
                    store: pop.store,
                    storeProps: pop.storeProps
                }
            ],
            dockedItems: [
                {
                    xtype: 'bravo_toolbar',
                    dock: 'bottom',
                    items: [
                        '->',
                        {
                            xtype: 'button',
                            text: '선택',
                            handler: function (btn) {
                                var win = btn.up('window');
                                var field = win.caller;
                                var fn = field.getEnableSupple() ? 'addBySupple' : 'addOnly';
                                btn[fn](btn);
                                win.close();
                            },
                            addBySupple: function () {
                                var btn = this;
                                var win = btn.up('window');
                                var grid = win.down('grid');
                                var field = win.caller;

                                Ext.each(grid.getSelection(), function (rec) {
                                    if (!field.findRecordByValue(rec.get(field.valueField))) {
                                        field.store.add(rec.clone());
                                    }
                                    if (!field.valueCollection.contains(rec)) {
                                        field.addValue(rec.get(field.valueField));
                                    }
                                });
                            },
                            addOnly: function () {
                                var btn = this;
                                var win = btn.up('window');
                                var grid = win.down('grid');
                                var field = win.caller;

                                field.clearValue();
                                Ext.each(grid.getSelection(), function (rec) {
                                    if (!field.findRecordByValue(rec.get(field.valueField))) {
                                        field.store.add(rec.clone());
                                    }
                                    field.addValue(rec.get(field.valueField));
                                });
                            }
                        },
                        {
                            xtype: 'button',
                            text: '닫기',
                            handler: function (btn) {
                                var win = btn.up('window');
                                win.close();
                            }
                        }
                    ]
                }
            ]
        });
    }
});