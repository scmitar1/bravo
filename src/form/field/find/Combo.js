
Ext.define('BRAVO.form.field.find.Combo', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.bravo_find_combo',
    layout: {
        type: 'hbox',
        align: 'bottom'
    },
    storeProps: {
        url: null,
        method: null,
        rootProperty: null,
        extraParams: null
    },
    displayField : 'CODE_NM',
    valueField  :'CODE',
    name  : '',
    setValue: function(value){
        this.down('field').setValue(value);
    },
    getValue : function(){
        this.down('field').getValue();
    },

    initComponent: function () {
        var me = this;

        Ext.apply(me,{
            fieldLabel : me.fieldLabel,
            labelAlign : me.labelAlign,
            items: [{
                xtype: 'bravo_combo_base',
                flex : 1,
                queryMode : 'local',
                emptyText : me.emptyText,
                store : Ext.create('Ext.data.Store'),
                name : me.name,
                value : me.value,
                valueField : me.valueField,
                displayField : me.displayField,
                allowBlank : me.allowBlank,
                listeners : me.listeners,
                getValue :function(){
                    var store = this.store;
                    var result = [];
                    for(var i=0;i<store.getCount();i++){
                        result.push(store.getAt(i).get(me.valueField));
                    }
                    return result;
                }
            }, {
                xtype: 'button',
                width: 30,
                iconCls : 'x-fa fa-search',
//                text: 'Find',
                handler: function(button){

                    BRAVO.Ajax.request({
                        url : me.storeProps.url,
                        method : me.method,
                        params: me.storeProps.extraParams,

                        success : function(resObj){
                            var rootProperty = me.storeProps.rootProperty;
                            var data = Ext.isEmpty(rootProperty) ? resObj : resObj[rootProperty];
                            var options = {
                                params : {
                                    valueField : me.valueField,
                                    checked : me.down('combo').getValue(),
                                    columns : [
                                        { xtype: 'bravo_checkcolumn', dataIndex: 'isChecked', width: 30},
                                        { text : '코드' , dataIndex : me.valueField,flex:1},
                                        { text : '코드명' , dataIndex : me.displayField,flex:2}
                                    ],
                                    data : data
                                },
                                callbackFunc : function(results){
                                    me.down('combo').setValue('');
                                    me.down('combo').getStore().removeAll();
                                    me.down('combo').getStore().loadData(results);
                                    if(results.length > 0){
                                        me.down('combo').setValue(results[0][me.valueField]);
                                    }
                                },
                                callbackScope : this,
                                autoSize : false
                            };
                            BRAVO.Utils.showPopup('combo_window',options);
                        }
                    });
                }
            }]

        });
        me.callParent(arguments);

    }


});