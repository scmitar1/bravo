Ext.define('BRAVO.form.field.find.Base', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.bravo_find_base',
    layout: {
        type: 'hbox',
        align: 'bottom'
    },
    gridItemId : null,
    PERSONAL_QRY: null,
    readOnly: null,
    maxLength: 12,
    
    setValue: function(value) {
        this.down('field').setValue(value);
    },
    getValue : function() {
        this.down('field').getValue();
    },

    initComponent: function () {
        var me = this;
        if(Ext.isEmpty(me.handler)){
            me.handler = function(button){

                var activeTab = this.up('global_center tabpanel').getActiveTab();
                var grid = activeTab.down('#'+me.gridItemId);
                console.log('---------------------------------', grid);
                if(grid !== null) {
                    var callUrl = grid.getStore().proxy.url;
                    if(me.PERSONAL_QRY === "YES") {
                        grid.getStore().proxy.url =  "/payroll/code/pay-date/list_personalqry";
                    } else {
                        grid.getStore().proxy.url =  callUrl;
                    }
                    grid.getStore().load();
                    grid.show();
                    grid.expand();
                }
            }
        }

        if(Ext.isEmpty(me.fieldXtype)) {
            var fieldXtype = 'textfield';
        } else {
            var fieldXtype = me.fieldXtype;
        }

        var fieldVType = null;
        if(Ext.isEmpty(me.fieldVType)) {
            fieldVType = 'codeOnly';
        } else {
            if(me.fieldXtype == 'datefield') {
                fieldVType = null;
            } else {
                fieldVType = me.fieldXtype;
            }
        }

        var inputfield = {
            xtype: fieldXtype,  //Ext.isEmpty(me.fieldXtype)? 'textfield' : me.fieldXtype,
            flex : 1,
            fieldLabel : me.fieldLabel,
            emptyText : me.emptyText,
            name : me.name,
            value : me.value,
            allowBlank : me.allowBlank,
            vtype : fieldVType,
            readOnly: me.readOnly,
            maxLength: me.maxLength,
            listeners: me.inputListeners
        };
        
        if( me.inputLabelAlign){
            inputfield.labelAlign = me.inputLabelAlign;
        }
        if( me.inputLabelWidth){
            inputfield.labelWidth = me.inputLabelWidth;
        }


        
        Ext.apply(me,{
            fieldLabel : null,
            items: [
                inputfield, 
                {
                    xtype: 'button',
                    width: 80,
                    iconCls : 'x-fa fa-search',
                    text: 'Find',
                    handler: me.handler
                }
            ]
        });
        me.callParent(arguments);

    }


});