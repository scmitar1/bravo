Ext.define('BRAVO.form.field.find.BaseSmall', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.bravo_find_base_small',
    layout: {
        type: 'hbox',
        align: 'bottom'
    },
    gridItemId : null,
    buttonItemId : null,
    PERSONAL_QRY: null,

    setValue: function(value){
        this.down('field').setValue(value);
    },
    getValue : function(){
        this.down('field').getValue();
    },

    initComponent: function () {
        var me = this;
        if(Ext.isEmpty(me.handler)){
            me.handler = function(button){

                var activeTab = this.up('global_center tabpanel').getActiveTab();
                var grid = activeTab.down('#'+me.gridItemId);
                var callUrl = grid.getStore().proxy.url;
                if(me.PERSONAL_QRY === "YES") {
                    grid.getStore().proxy.url =  "/payroll/code/pay-date/list_personalqry";
                } else {
                    grid.getStore().proxy.url =  callUrl;
                }
                grid.getStore().load();
                grid.show();
                grid.expand();
            }
        }

        Ext.apply(me,{
            fieldLabel : null,
            items: [{
                xtype: Ext.isEmpty(me.fieldXtype)? 'textfield' : me.fieldXtype,
                flex : 1,
                fieldLabel : me.fieldLabel,
                labelAlign : me.labelAlign,
                emptyText : me.emptyText,
                name : me.name,
                value : me.value,
                allowBlank : me.allowBlank
            }, {
                xtype: 'button',
                width: 40,
                iconCls : 'x-fa fa-search',
                text: 'F',
                handler: me.handler
            }]

        });
        me.callParent(arguments);

    }


});