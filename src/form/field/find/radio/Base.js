Ext.define('BRAVO.form.field.find.radio.Base', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.bravo_find_radio',
    layout: {
        type: 'hbox',
        align: 'bottom'
    },
    labelAlign : 'top',
    labelSeparator : '',
    gridItemId : null,
    columns : 2,
    title : null,
    setValue: function(value){
        this.down('field').setValue(value);
    },
    getValue : function(){
        this.down('field').getValue();
    },
    initComponent: function () {
        var me = this;

        me.title = me.fieldLabel;
        me.fieldLabel =null;
        if(Ext.isEmpty(me.handler)){
            me.handler = function(button){

                var activeTab = this.up('global_center tabpanel').getActiveTab();
                var grid = activeTab.down('#'+me.gridItemId);
                grid.getStore().load();
                grid.show();
            }
        }

        Ext.apply(me,{
            items: [{
                xtype: 'bravo_radio_base',
                flex : 1,
                name: 'SPECIALPAY_FLAG',
                title: me.title,
                radioItems: me.radioItems
            }, {
                xtype: 'button',
                width: 80,
                iconCls : 'x-fa fa-search',
                text: 'Find',
                handler: me.handler
            }]

        });
        me.callParent(arguments);

    }


});