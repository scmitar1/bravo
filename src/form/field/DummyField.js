Ext.define('BRAVO.form.field.DummyField', {
    extend: 'Ext.form.field.Text',
    xtype: 'bravo_dummyfield',
    fieldLabel: 'dummy',
    isFormField: false,
    listeners: {
        afterrender: function () {
            this.el.setVisibilityMode(Ext.Element.VISIBILITY);
            this.setVisible(false);
        }
    }
});