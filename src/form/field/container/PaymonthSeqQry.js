Ext.define('BRAVO.form.field.container.PaymonthSeqQry', {
    extend: 'Ext.container.Container',
    alias: 'widget.bravo_container_paymonthseq_qry',
    // height : 45,
    listeners : {},

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'monthfield',
                            flex: 0.3,
                            fieldLabel: '급여월',
                            name: 'PAY_MONTH',
                            allowBlank: false,
                            maxLength: 6,
                            minLength: 6
                        },
                        {
                            xtype: 'numberfield',
                            flex: 0.2,
                            fieldLabel: '차수',
                            name: 'SEQ',
                            allowBlank: false,
                            value: 1,
                            minValue: 0,
                            maxValue: 9,
                            plugins: [
                                {
                                    ptype: 'paydateautocomplete'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});