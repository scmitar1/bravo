Ext.define('BRAVO.form.field.container.DailyworkPaydateQry', {
    extend: 'Ext.container.Container',
    alias: 'widget.bravo_container_dailyworkpaydate_qry',
    // height : 45,
    listeners : {},

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'datefield',
                            flex: 0.4,
                            fieldLabel: '급여일',
                            name: 'PAY_DATE',
                            maxLength: 8,
                            minLength: 8
                        },
                        {
                            xtype: 'numberfield',
                            flex: 0.1,
                            fieldLabel: '찾기',
                            name: 'SEQ',
                            allowBlank: true,
                            value: 1,
                            minValue: 1,
                            maxValue: 1,
                            plugins: [
                                {
                                    ptype: 'dailyworkpaydateautocomplete'
                                }
                            ]

                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});