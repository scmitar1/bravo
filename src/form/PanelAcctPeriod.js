Ext.define('BRAVO.form.PanelAcctPeriod', {
    extend: 'BRAVO.form.Panel',
    alias: 'widget.bravo_form_panelacctpriod',
    cls: 'title-header',
    frame: false,
    bodyBorder: true,
    layout: 'column',
    autoScroll: true,
    submitEmptyText: false,

    items: [
        {
            xtype: 'fieldcontainer',
            labelAlign: 'top',
            fieldLabel: '회계기간',
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'bravo_combo_year',
                    width: 82,
                    name: 'CMM_FSC_YR_SEL',
                    bind: {
                        value: '{acct_ledger_common.CMM_FSC_YR_SEL}'
                    }
                }
            ]
        },
        {
            xtype: 'fieldcontainer',
            labelAlign: 'top',
            columnWidth: 1,
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'datefield',
                    name: 'CMM_WSDATE1',
                    readOnly: true,
                    width: 82,
                    bind: {
                        value: '{acct_ledger_common.CMM_WSDATE1}'
                    },
                    maxLength: 8
                }, {
                    xtype: 'label',
                    margin: '0 3 5 5',
                    width: 15,
                    text: ' ~ '
                }, {
                    xtype: 'datefield',
                    width: 82,
                    name: 'CMM_WSDATE2',
                    readOnly: true,
                    bind: {
                        value: '{acct_ledger_common.CMM_WSDATE2}'
                    },
                    maxLength: 8
                }
            ]
        },
        {
            xtype: 'fieldcontainer',
            labelAlign: 'top',
            fieldLabel: '조회기간',
            margin: '10 5 5 5',
            layout: {
                type: 'hbox',
                align: 'bottom'
            },
            items: [
                {
                    xtype: 'bravo_combo_month',
                    name: 'CMM_FSC_MO',
                    width: 82,
                    bind: {
                        value: '{acct_ledger_common.CMM_FSC_MO}'
                    }
                }
            ]
        },
        {
            xtype: 'fieldcontainer',
            labelAlign: 'top',
            margin: '5 5 5 5',
            columnWidth: 1,
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'datefield',
                    name: 'CMM_REQDAY1',
                    allowBlank: false,
                    width: 82,
                    bind: {
                        value: '{acct_ledger_common.CMM_REQDAY1}'
                    },
                    maxLength: 8
                }, {
                    xtype: 'label',
                    margin: '0 5 5 8',
                    width: 15,
                    text: ' ~ '
                }, {
                    xtype: 'datefield',
                    columnWidth: 0.5,
                    name: 'CMM_REQDAY2',
                    allowBlank: false,
                    width: 82,
                    bind: {
                        value: '{acct_ledger_common.CMM_REQDAY2}'
                    },
                    maxLength: 8
                }
            ]
        }
    ]
});