Ext.define('BRAVO.form.Panel', {
    extend: 'Ext.form.Panel',
    alias : 'widget.bravo_form',
    cls: 'title-header',
    frame: false,
    bodyBorder: true,
    bodyPadding: 5,
    layout: 'column',
    submitEmptyText: false,
    defaults: {
        columnWidth: 1,
        // height: 45,
        margin : '0 5 5 5'
    }
});