/**
 * 주로 Grid에서 표시될 날짜, 숫자, flag등에 대한 표시형태 renderer 모음
 */
Ext.define('BRAVO.Format', {
    extend: 'Ext.Base',

    statics: {
        borderExportStyleExcel: [
            {
                position: 'Top',
                lineStyle: 'None' //'Continuous'
            },
            {
                position: 'Right',
                lineStyle: 'None'
            },
            {
                position: 'Bottom',
                lineStyle: 'NOne'
            },
            {
                position: 'Left',
                lineStyle: 'None'
            }
        ],

        numberExportStyle: {
            format: 'Standard'
        },

        fixedNumberExportStyle: {
            format: 'Fixed'
        },

        onedecimalNumberExportStyle: {
            format: 'oneDecimalNumber'
        },
        
        fourdecimalNumberExportStyle: {
            format: 'FourDecimalNumber'
        },
        
        elevendecimalNumberExportStyle: {
            format: 'ElevenDecimalNumber'
        },

        /**
         * 시간, 날짜 표시 관련 renderer
         */
        TimestampRenderer: function (val) {
            return Ext.util.Format.date(new Date(val), 'Y-m-d H:i');
        },

        CalctimeRenderer: function (value) {
            if (value.length === 4) {
                return value.toString().substring(0, 2) + '.' + value.toString().substring(2, 4);
            } else {
            }
        },

        timeRenderer: function (value, metaData, record, rowIndex, colIndex, store) {
            if (value.length === 4) {
                return value.toString().substring(0, 2) + ':' + value.toString().substring(2, 4);
            } else {
            }

            /*             var combo = this.getColumns()[colIndex].getEditor();
             var comboStore = combo.getStore();
             var findIdx = combo.getStore().find(combo.valueField, value);
             if (findIdx == -1) {
             return value;
             }
             return comboStore.getAt(findIdx).data.disp;*/
        },

        dateRenderer: function (value) {

            if (Ext.isEmpty(value)) {
                return '';
            }
            if (Ext.isDate(value)) {
                return Ext.util.Format.date(value, 'Y-m-d');
            }
            if (value.trim(value).length < 6) {
                return '';
            }

            var year = value.substr(0, 4);
            var month = value.substr(4, 2);
            var day = value.substr(6, 2);
            var date = Ext.String.format('{0}-{1}-{2}', year, month, day);
            var dateYm = Ext.String.format('{0}-{1}', year, month);

            return value.length == '6' ? dateYm : date;
        },


        /**
         * Combobox 에 표시될 renderer
         */
        comboRenderer: function (value, metaData, record, rowIndex, colIndex, store) {
            var combo = this.getColumns()[colIndex].getEditor();
            var comboStore = combo.getStore();
            var findIdx = combo.getStore().find(combo.valueField, value);

            if (findIdx == -1) {
                return value;
            }
            return comboStore.getAt(findIdx).get(combo.displayField);
        },


        /**
         * 주로 Grid에 표시될 단순 숫자, edit가능형 숫자, summary숫자 등을 지정
         */
        numberRenderer: function (value) {
            if (!value) value = 0;
            if (!Ext.isNumber(value)) {
                return value;
            }
            return Ext.util.Format.number(value, '#,###');
        },

        numberRenderer2Digit: function (value) {
            if (!value) value = 0;
            if (!Ext.isNumber(value)) {
                return value;
            }
            return Ext.util.Format.number(value, '#,###.##');
        },

        numberEditRenderer: function (value) {
            if (!Ext.isNumber(value)) {
                return value;
            } else {
                return Ext.String.format(
                    '<span style="font-weight:bold;">{0}</span>',
                    BRAVO.Format.numberRenderer(value)
                );
            }
        },

        subTotalRenderer: function (value) {
            if (!Ext.isNumber(value)) {
                return value;
            }
            var color = value < 0 ? 'red' : 'black';
            return Ext.String.format('<span style="color:{0};font-weight:bold;">{1}</span>', color, Ext.util.Format.number(value, '#,###'));
        },

        summaryRenderer: function (value) {
            var rtnValue = 0;
            if (!isNaN(value)) rtnValue = value;

            if (!rtnValue) rtnValue = 0;
            if (!Ext.isNumber(rtnValue)) {
                return rtnValue;
            } else {
                var color = rtnValue < 0 ? 'red' : 'green';
                return Ext.String.format(
                    '<span style="color:{0};font-weight:bold;">{1}</span>',
                    color,
                    BRAVO.Format.numberRenderer(rtnValue)
                );
            }
        },
        
        summaryRenderer2Digit: function (value) {
            var rtnValue = 0;
            if (!isNaN(value)) rtnValue = value;

            if (!rtnValue) rtnValue = 0;
            if (!Ext.isNumber(rtnValue)) {
                return rtnValue;
            } else {
                var color = rtnValue < 0 ? 'red' : 'green';
                return Ext.String.format(
                    '<span style="color:{0};font-weight:bold;">{1}</span>',
                    color,
                    BRAVO.Format.numberRenderer2Digit(rtnValue)
                );
            }
        },
        
        summaryRenderer100Percent: function (value) {
            if (!value) value = 0;
            if (!Ext.isNumber(value)) {
                return value;
            } else {
                var color = value === 100 ? 'green' : 'red';
                return Ext.String.format(
                    '<span style="color:{0};font-weight:bold;">{1}</span>',
                    color,
                    BRAVO.Format.numberRenderer(value)
                );
            }
        },

        floatRenderer: function (value) {
            if (!Ext.isNumber(value)) {
                return value;
            }
            return Ext.util.Format.number(value, '#.00');
        },

        subTotalRenderer: function (value) {
            if (!Ext.isNumber(value)) {
                return value;
            }
            var color = value < 0 ? 'red' : 'black';
            return Ext.String.format('<span style="color:{0};font-weight:bold;">{1}</span>',
                color,
                BRAVO.Format.numberRenderer(value)
            );
        },

        /**
         * 사용중지 INACTIVE 필드나 Yes/No 표시 필드
         */
        inactiveRenderer: function (value) {
            return value == 'N' ? '' : value;
        },
        ynFlagRenderer: function (value) {
            return value == 'N' ? '' : value;
        },

        /**
         * 급여일 open인지 별도 표시하는 renderer
         * @param value
         */
        isOpenRenderer: function (value, col, record, rowIndex, colIndex) {
            if (value.toUpperCase() === "OPEN" || value === "오픈") {
                return Ext.String.format('<span style="color:{0};font-weight:bold;">{1}</span>',
                    'red', value
                );
            } else {
                return value;
            }
        },

        /**
         * 0 값을 blank로 치환
         * @param value
         * @returns {*}
         */
        stringCommaZeroBlank: function (value) {
            if (value === null || value === "" || value === "0") {
                return ""
            } else {
                var str = String(value);
                return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
            }
        },

        /**
         * 문자형으로 받은 변수를 숫자로 변환 후 더하기 연산
         * @param value1
         * @param value2
         * @param value3
         * @param value4
         * @returns {*}
         */
        stringAddAsNumber: function (value1, value2, value3, value4) {
            var value1Num, value2Num, value3Num, value4Num,
                value1Str, value2Str, value3Str, value4Str;

            if (value1 === null || value1 === "") {
                value1Num = 0;
            } else {
                value1Str = String(value1);
                value1Num = parseInt(value1Str.replace(/[^\d]+/g, ''));
                if (value1Str.substr(0, 1) === "-") {
                    value1Num = (-1) * value1Num
                }
            }
            if (value2 === null || value2 === "") {
                value2Num = 0;
            } else {
                value2Str = String(value2);
                value2Num = parseInt(value2Str.replace(/[^\d]+/g, ''));
                if (value2Str.substr(0, 1) === "-") {
                    value2Num = (-1) * value2Num
                }
            }

            if (value3 === null || value3 === "" || value3 === undefined) {
                value3Num = 0;
            } else {
                value3Str = String(value3);
                value3Num = parseInt(value3Str.replace(/[^\d]+/g, ''));
                if (value3Str.substr(0, 1) === "-") {
                    value3Num = (-1) * value3Num
                }
            }

            if (value4 === null || value4 === "" || value4 === undefined) {
                value4Num = 0;
            } else {
                value4Str = String(value4);
                value4Num = parseInt(value4Str.replace(/[^\d]+/g, ''));
                if (value4Str.substr(0, 1) === "-") {
                    value4Num = (-1) * value4Num
                }
            }
            return value1Num + value2Num + value3Num + value4Num;
        },

        /**
         * 문자형으로 받은 변수를 숫자로 변환 후 마이너스 연산
         * @param value1
         * @param value2
         * @returns {number}
         */
        stringMinusAsNumber: function (value1, value2) {
            var value1Num, value2Num, value1Str, value2Str;

            if (value1 === null || value1 === "") {
                value1Num = 0;
            } else {
                value1Str = String(value1);
                value1Num = parseInt(value1Str.replace(/[^\d]+/g, ''));
                if (value1Str.substr(0, 1) === "-") {
                    value1Num = (-1) * value1Num
                }
            }
            if (value2 === null || value2 === "") {
                value2Num = 0;
            } else {
                value2Str = String(value2);
                value2Num = parseInt(value2Str.replace(/[^\d]+/g, ''));
                if (value2Str.substr(0, 1) === "-") {
                    value2Num = (-1) * value2Num
                }
            }
            return value1Num - value2Num;
        }


    }

});
