Ext.define('BRAVO.tab.Base', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.bravo_tab_base',
    cls: 'title-header',
    tabPosition: 'bottom',
    storeProps: {
        url: null,
        method: null,
        fields: [],
        groupField : null,
        rootProperty: 'list',
        timeout : 9000000
    },

    height: '100%',
    emptyText: 'No data to display',
    plugins : [],
    viewConfig: {
        enableTextSelection: true
    },

    initComponent: function () {
    }

});
