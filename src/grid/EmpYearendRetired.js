Ext.define('BRAVO.grid.EmpYearendRetired', {
    extend: 'BRAVO.grid.BaseMulti',
    alias: 'widget.bravo_grid_emp_yearendretired',
    title: '사원목록',
    statics : {
        searchTxt : ''
    },
    initComponent: function () {

        var me = this;
        Ext.apply(me, {
            columns: [
                {text: '', dataIndex: 'EID', align: 'left', hidden:true},
                {text: '사번', dataIndex: 'EMP_NUMBER', width: 100,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                {text: '이름', dataIndex: 'EMP_NAM', width: 90},
                {text: '부서', dataIndex: 'HRDEPT_NAM', flex: 3},
                {text: '입사일', dataIndex: 'E_JOIN_DATE', width: 90, renderer: BRAVO.Format.dateRenderer},
                {text: '퇴사일', dataIndex: 'RETIRE_DATE', width: 90, renderer: BRAVO.Format.dateRenderer},
                {text: '마지막 급여일', dataIndex: 'LASTPAY_DATE', width: 110, renderer: BRAVO.Format.dateRenderer}
            ],
            store: {
                proxy: {
                    type: 'ajax',
                    url: '/yearend/retireynd/emp_retired/list',
                    actionMethods : {
                        create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'
                    },
                    extraParams: {
                        WORK_FLAG: "J"
                    },
                    reader: {
                        type: 'json',
                        rootProperty: 'list',
                        totalProperty: 'totalCount'
                    },
                    simpleSortMode: true
                }
            }, // 'EmpPayStore',

            dockedItems: [{
                xtype: 'toolbar',
                docked: 'top', padding: 5,
                items: [{
                    xtype: 'textfield',
                    itemId: 'payEmpSearchTxt',
                    labelAlign: 'left',
                    enableKeyEvents: true,
                    // fieldLabel: '검색(사번,성명)',
                    emptyText: '사번/이름 입력 후 Enter',
                    value : BRAVO.grid.EmpPayEntry.searchTxt,
                    listeners: {
                        keyup: function (field, event) {
                            BRAVO.grid.EmpPayEntry.searchTxt=field.getValue();
                            var gridStore = this.getStore();

                            gridStore.clearFilter();

                            var value = field.getValue();
                            gridStore.filterBy(function (rec) {
                                var EMP_NUMBER = rec.get('EMP_NUMBER');
                                var EMP_NAME = rec.get('EMP_NAME');
                                var isChecked = rec.get('isChecked')===true;
                                var isEqualEMP_NUMBER = Ext.isEmpty(value) ? true : EMP_NUMBER.search(value) != -1;
                                var isEqualEMP_NAME = Ext.isEmpty(value) ? true : EMP_NAME.search(value) != -1;

                                return isChecked || isEqualEMP_NUMBER || isEqualEMP_NAME;
                            });

                            var items = Ext.ComponentQuery.query('bravo_grid_emp_pay_entry');

                            for (var i = 0; i < items.length; i++) {
                                items[i].down('#payEmpSearchTxt').setValue(field.getValue());
                            }
                        },
                        scope: me
                    }
                }]
            }]
        });

        me.callParent(arguments);

    }

});