Ext.define('BRAVO.grid.Base', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.bravo_grid_base',
    cls: 'title-header',
    storeProps: {
        url: null,
        method: null,
        fields: [],
        groupField : null,
        rootProperty: 'list',
        timeout : 9000000
    },
    excelMode: false,
    frame: true,
    columnLines: false,
    rowLines: true,  // default 가 true 임
    floatable: false,
    isGlobal: false,
    onGridLoad: Ext.emptyFn,
    onGridUpdate : Ext.emptyFn,
    onGridSelect: Ext.emptyFn,
    onGridAfterRender: Ext.emptyFn,
    onGridRowDoubleClick : Ext.emptyFn,
    usePagingToolbar: true,
    dragTarget: null,
    dropTarget: null,
    height: '100%',
    ddGroup: null,
    enableDrag: true,
    enableDrop: true,
    dropZone: null,
    rowNumberer: 38,
    emptyText: 'No data to display',
    plugins : [],
    viewConfig: {
        enableTextSelection: true
    },

    initComponent: function () {
        var me = this;
        if (!me.isGlobal && me.onGridSelect == Ext.emptyFn) {
            me.onGridSelect = function (grid, record, index) {
                if(!this.up('global_center tabpanel')){
                    return false;
                }
                var activeTab = this.up('global_center tabpanel').getActiveTab();
                var form = activeTab.down('form');
                if( form){
                    form.getForm().setValues(record.data);
                }
            }
        }

        var columns = Ext.clone(me.columns);

        if (Ext.isNumber(me.rowNumberer)) {
            if (Ext.isArray(me.columns)) {
                columns.unshift({xtype: 'rownumberer', text: '#', width: me.rowNumberer,locked : false});
            }
        }

        var plugins = Ext.isArray(me.plugins) ?Ext.clone(me.plugins) : [];
        plugins.push({ptype: 'gridexporter'});
        plugins.push({ptype: 'gridfilters'});

        if (me.excelMode == true) {
            plugins.push({
                ptype: 'cellediting',
                clicksToEdit: 1
            });
        }

        Ext.apply(me, {
            columns: columns,
            plugins : plugins,
            store: me.configStore(),
            dockedItems: me.configDockedItems(),
            frame: false,
            listeners: {
                select: me.onGridSelect === null ? function(){} :me.onGridSelect ,
                storeLoad: me.onGridLoad,
                storeUpdate: me.onGridUpdate,
                afterrender: me.onGridAfterRender,
                rowdblclick : me.onGridRowDoubleClick
            }
        });
        me.callParent(arguments);
    },
    
    configStore: function () {
        var me = this;
        if (!Ext.isEmpty(me.store)) {
            return me.store;
        }
        var readMethod = "GET";
        if(me.storeProps.method == "POST") {
            readMethod = "POST";
        }

        return Ext.create('Ext.data.Store', {
            fields: Ext.isArray(me.storeProps.fields) ? Ext.clone(me.storeProps.fields) : [],
            groupField : me.storeProps.groupField,
            autoLoad: me.storeProps.autoLoad,
            proxy: {
                type: Ext.isEmpty(me.storeProps.url) ? 'memory': 'ajax',
                url: me.storeProps.url,
                actionMethods : {
                    create: 'POST', read: readMethod, update: 'POST', destroy: 'POST'
                },
                reader: {
                    type: 'json',
                    rootProperty:  Ext.isEmpty(me.storeProps.rootProperty) ? 'list' :  me.storeProps.rootProperty
                },
                extraParams: me.storeProps.extraParams || {},
                afterRequest: function( request, success) {
                    if( success==false){
                        try
                        {
                            var response = request.getOperation().error.response;
                            var objError = Ext.decode( response.responseText);
                            if(response.status == "400") {
                                Ext.MessageBox.show({ title: objError.title, msg: objError.msg, icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });
                            } else if(response.status == "500") {
                                Ext.MessageBox.show({ title: 'Error', msg: '관리자에게 문의하세요.[500]', icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });        // rtnString2[1]
                            } else if (response.status == "404") {
                                Ext.MessageBox.show({ title: 'Error', msg: "관리자에게 문의하세요.[404]", icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });            // invalid url
                            }else{
                                BRAVO.Msg.alert('확인','오류입니다.');
                            }
                        }catch( e){
                            console.log( e);
                        }
                    }
                },
                timeout :me.storeProps.timeout || BRAVO.Ajax.timeout
            },
            listeners: {
                load: function (store) {
                    me.show();
                    this.fireEvent('storeload', store)
                },
                update : function(thisStore , record , operation , modifiedFieldNames , details){
                    this.fireEvent('storeupdate', thisStore , record , operation , modifiedFieldNames , details)
                },
                exception : function(thisStore,request){

                },
                scope: me

            }
        });
    }, 
    
    configDockedItems: function () {
        var me = this;
        if (!me.dockedItems) {
            me.dockedItems = [];
        }

        var dockedItems = Ext.isArray(me.dockedItems) ? Ext.clone(me.dockedItems) : [];
        if (me.usePagingToolbar) {
            dockedItems.push({
                xtype: 'bravo_pagingtoolbar',
                dock: 'bottom'
            });
        }
        return dockedItems;
    },
    
    isValidData : function(){
        var grid = this;
        var gridColumn = grid.getColumns();
        var gridStore = grid.getStore();
        var errorMessage ='';
        for(var i=0;i<gridStore.getCount();i++){
            var rec = gridStore.getAt(i);
            for(var j=0;j<gridColumn.length;j++){
                var column = gridColumn[j];
                var editor = column.getEditor();
                if(Ext.isEmpty(editor)){
                    continue;
                }

                var error = editor.getErrors(rec.get(column.dataIndex));
                if(error.length>0){
                    errorMessage+= i+1+'행의 ['+column.text+']  ' + error[0]+'<br>';
                }
            }
        }
        if(!Ext.isEmpty(errorMessage)){
            BRAVO.Msg.alert('오류',errorMessage);
        }
        return Ext.isEmpty(errorMessage) ? true : false;
    },
    
    getDataIndexes : function(){
        var gridColumns = this.getColumns();
        var listDataIndex = [];
        for(var i=0;i<gridColumns.length;i++){
            var dataIndex = gridColumns[i].dataIndex;
            if(!Ext.isEmpty(dataIndex)){
                listDataIndex.push(dataIndex);
            }
        }
        return listDataIndex;
    },
    
    getSubmitDataList : function(){
        var listDataIndex = this.getDataIndexes();
        var gridStore = this.getStore();
        var gridDatas = [];
        for(var i=0;i<gridStore.getCount();i++){
            var rec = gridStore.getAt(i);
            var tempObj = {};
            for(var j=0;j<listDataIndex.length;j++){
                var index =listDataIndex[j];
                tempObj[index] = rec.get(index);
            }
            gridDatas.push(tempObj);
        }
        return gridDatas;
    }
});
