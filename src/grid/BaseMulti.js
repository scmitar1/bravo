Ext.define('BRAVO.grid.BaseMulti', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_base_multi',
    autoCheckChange : false,
    rowNumberer: 0,

    initComponent: function () {
        var me = this;
        var columns =Ext.clone(me.columns);
        columns.unshift({xtype: 'bravo_checkcolumn', dataIndex: 'isChecked', width: 30});

        me.columns = columns;
        me.callParent(arguments);

        if (me.autoCheckChange) {
            me.dirtyRecordMap = new Ext.util.HashMap();
            
            me.getStore().on('update', function (thisStore, record, operation, modifiedFieldNames, details) {
                
                if( me.dirtyRecordMap.contains( record.id)){
                    return;
                }else{
                    me.dirtyRecordMap.add( record.id, record.id);
                    record.set('isChecked',true);
                    
//                    if (modifiedFieldNames[0] != 'isChecked') {
//                        delete record.modified.isChecked;
//                        if('{}'!=Ext.encode(record.modified)){
//                            record.set('isChecked',true);
//                        }else{
//                            record.set('isChecked',false);
//                        }
//                    }
                }
            });
        }
    },

    getCheckedData : function(){
        var store = this.getStore();
        var arr = [];
        for(var i=0;i<store.getCount();i++){
            var rec = store.getAt(i);
            if(rec.get('isChecked')===true){
                arr.push(rec);
            }
        }
        return arr;
    },
    
    getSubmitDataList : function(){
        var store = this.getStore();
        var arr = [];
        for(var i=0;i<store.getCount();i++){
            var rec = store.getAt(i);
            if(rec.get('isChecked')===true){
                arr.push(rec.data);
            }
        }
        return arr;
    }
});