/*
나중에 parameter 전달 잘 되도록 해서 hrdeptcode와 합침
 */
Ext.define('BRAVO.grid.HrDeptCodeInactive', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_hrdeptcode_inactive',
    cls : 'title-header',
    columnHidden: true,
    rowNumberer: 0,
    INACTIVE : 'N',
    columns: [
        {text: '부서코드', dataIndex: 'HRDEPT_CODE', width:80},
        {text: '부서명', dataIndex: 'HRDEPT_NAMK', flex:1},
        {text: '상위코드', dataIndex: 'PCC_PARENT', width:70}
    ],
    storeProps : {
        url : '/hr/code/hr-dept/list',
        extraParams: {INACTIVE: 'N'}
    }

});