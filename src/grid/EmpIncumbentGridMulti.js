/**
 * 다른 사원정보와 다르게 DB/DC 타입 정보를 추가로 더 가져온다.
 * 퇴직 모듈에서 사용됨.
 */
Ext.define('BRAVO.grid.EmpIncumbentGridMulti', {
    extend: 'BRAVO.grid.BaseMulti',
    alias: 'widget.bravo_grid_incumbent_multi',
    title: '재직 사원목록',
    statics : {
        searchTxt : ''
    },
    initComponent: function () {

        var me = this;
        Ext.apply(me, {
            columns: [
                {
                    text: '사번', dataIndex: 'EMP_NUMBER', minWidth: 105, flex: 1, filter: {
                    type: 'string',
                    itemDefaults: {
                        emptyText: 'Search for...'
                    }
                }
                },
                {text: '이름', dataIndex: 'EMP_NAME', width: 120},
                {text: '부서', dataIndex: 'HRDEPT_NAM', flex: 1},
                {text: '타입', dataIndex: 'SEV_PENSION_TYPE', width: 60}
                // {text: '', dataIndex: 'DUMMY', width: 1}
            ],
            
            store: Ext.create( 'Ext.data.Store', {
             autoLoad: true,
             proxy: {
                    type: 'ajax',
                    url: '/payroll/retirement/incumbent-emp/list',
                    method: 'POST',
                    reader: {
                        type: 'json',
                        rootProperty:  'list'
                    }
                }
            }),

            dockedItems: [{
                xtype: 'toolbar',
                docked: 'top', padding: 5,
                items: [{
                    xtype: 'textfield',
                    itemId: 'payEmpSearchTxt',
                    labelAlign: 'left',
                    enableKeyEvents: true,
                    //fieldLabel: '검색(사번,성명)',
                    emptyText: '사번/이름 입력 후 Enter',
                    value : BRAVO.grid.EmpPayEntry.searchTxt,
                    listeners: {
                        keyup: function (field, event) {
                            BRAVO.grid.EmpPayEntry.searchTxt=field.getValue();
                            var gridStore = this.getStore();

                            gridStore.clearFilter();

                            var value = field.getValue();
                            gridStore.filterBy(function (rec) {


                                var EMP_NUMBER = rec.get('EMP_NUMBER');
                                var EMP_NAME = rec.get('EMP_NAME');
                                var isChecked = rec.get('isChecked')===true;
                                var isEqualEMP_NUMBER = Ext.isEmpty(value) ? true : EMP_NUMBER.search(value) != -1;
                                var isEqualEMP_NAME = Ext.isEmpty(value) ? true : EMP_NAME.search(value) != -1;

                                return isChecked || isEqualEMP_NUMBER || isEqualEMP_NAME;
                            });


                            var items = Ext.ComponentQuery.query('bravo_grid_emp_pay_entry');

                            for (var i = 0; i < items.length; i++) {
                                items[i].down('#payEmpSearchTxt').setValue(field.getValue());

                            }
                        },
                        scope: me
                    }
                }]
            }]
        });

        me.callParent(arguments);

    }

});