Ext.define('BRAVO.grid.EmpPayQry', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.bravo_grid_emp_pay_qry',
    title: '사원목록',
    selModel: {
        type: 'checkboxmodel',
        mode: 'multi',
        /**
         * Synchronize header checker value as selection changes.
         * @private
         */
        onSelectChange: function (record, isSelected, suppressEvent, commitFn) {
            var me = this,
                parentFn = Ext.selection.RowModel.prototype.onSelectChange;

            parentFn.call(me, record, isSelected, suppressEvent, commitFn);
            record.set('isChecked', isSelected, {
                dirty: false
            });
            if (me.column) {
                me.column.updateCellAriaDescription(record, isSelected);
            }

            if (!me.suspendChange) {
                me.updateHeaderState();
            }
        }
    },

    columns: [
        {
            text: '사번',
            dataIndex: 'EMP_NUMBER',
            width: 90
        },
        {
            text: '이름',
            dataIndex: 'EMP_NAM',
            width: 90
        },
        {
            text: '부서',
            dataIndex: 'HRDEPT_NAM',
            flex: 3
        },
        {
            text: '입사일',
            dataIndex: 'E_JOIN_DATE',
            width: 75,
            renderer: BRAVO.Format.dateRenderer
        },
        {
            text: '퇴사일',
            dataIndex: 'RETIRE_DATE',
            width: 75,
            renderer: BRAVO.Format.dateRenderer
        },
        {
            text: '최종급여일',
            dataIndex: 'RETIRE_DATE',
            width: 85,
            renderer: BRAVO.Format.dateRenderer
        },
        {
            text: '이메일',
            dataIndex: 'EMAIL',
            width: 90,
            hidden: true
        }
    ],
    store: 'EmpPayQryStore',

    updateBindSelection: function (selModel, selection) {
        var me = this,
            clazz = Ext.panel.Table,
            plugin = me.findPlugin('globalsm-emppayqry'),
            plugin2 = me.findPlugin('globalsm_emppayqry_one_type');

        if (plugin) {
            plugin.updateBindSelection.call(me, selModel, selection);
        }else if (plugin2) {
            plugin2.updateBindSelection.call(me, selModel, selection);
        } else {
            clazz.prototype.updateBindSelection.call(me, selModel, selection);
        }
    }

});