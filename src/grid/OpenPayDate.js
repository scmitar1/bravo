Ext.define('BRAVO.grid.OpenPayDate', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_open_paydate',
    cls : 'title-header',
    columnHidden: true,
    rowNumberer: 0,
    columns: [
        {text: '급여월', dataIndex: 'PAY_MONTH', width:75},
        {text: '차수', dataIndex: 'SEQ', width:50},
        {text: '급여일', dataIndex: 'PAY_DATE', flex:1, renderer: BRAVO.Format.dateRenderer},
        {text: '시작일', dataIndex: 'START_DATE', width:90, renderer: BRAVO.Format.dateRenderer},
        {text: '종료일', dataIndex: 'END_DATE', width:90, renderer: BRAVO.Format.dateRenderer}
    ],
    storeProps : {
        url : '/payroll/code/open-pay-date/grid-list'
    }

});