Ext.define('BRAVO.grid.EmpSelection', {
    extend: 'BRAVO.grid.Emp',
    alias: 'widget.bravo_grid_emp_selection',
    selType: 'checkboxmodel',
    onGridSelect: null
});