Ext.define('BRAVO.grid.PayCode', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_paycode',
    columns: [
        {text: '급여코드', dataIndex: 'PAY_CODE', align:'left', width:80},
        {text: '코드명(한글)', dataIndex: 'PAY_NAME', align:'left', flex:1},
        {text: '코드명(영어)', dataIndex: 'PAY_ENAME', align:'left', flex:1},
        {text: '사용중지', dataIndex: 'INACTIVE', align:'center', width:75, renderer: BRAVO.Format.inactiveRenderer},
        {text: '급여/공제', dataIndex: 'PAYTAX_FLAG', align:'center', width:80}
    ],
    storeProps : {
        url : '/payroll/code/pay-pay/list'
    }
});