Ext.define('BRAVO.grid.EmpSelectionRetireOnly', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.bravo_grid_emp_selection_retire_only',
    selType: 'checkboxmodel',
    frame: true,
    initComponent : function(){
        var me = this;

        Ext.apply(me,{
            store : Ext.create('Ext.data.Store', {
                fields: [],
                autoLoad: true,
                sorters: [{ property: 'RETIRE_DATE', direction: 'DESC' }],
                proxy: {
                    type: 'ajax',
                    url: '/employee/list',
                    extraParams: {
                        WORK_FLAG: "T",
                        ONLY_RETIRE_YND: "Y"
                    }
                }
            }),
            columns: [
                { text: '사번', dataIndex: 'EMP_NUMBER', width: 105,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                { text: '이름', dataIndex: 'EMP_NAME', width: 90,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                { text: '부서', dataIndex: 'HRDEPT_NAM', width: 200,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                { text: '직위', dataIndex: 'POSITION_NAME', width: 80,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                { text: '입사일', dataIndex: 'JOIN_DATE' , width: 100,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                { text: '퇴사일', dataIndex: 'RETIRE_DATE' , width: 100,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                }
            ]
        });

        me.callParent(arguments);
    }
});