Ext.define('BRAVO.grid.EmpYearend', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.bravo_grid_emp_yearend',
    title: '사원목록 yy',
    selModel: {
        type: 'checkboxmodel',
        mode: 'multi',
        /**
         * Synchronize header checker value as selection changes.
         * @private
         */
        onSelectChange: function (record, isSelected, suppressEvent, commitFn) {
            var me = this,
                parentFn = Ext.selection.RowModel.prototype.onSelectChange;

            parentFn.call(me, record, isSelected, suppressEvent, commitFn);
            record.set('isChecked', isSelected, {
                dirty: false
            });
            if (me.column) {
                me.column.updateCellAriaDescription(record, isSelected);
            }

            if (!me.suspendChange) {
                me.updateHeaderState();
            }
        }
    },
    columns: [
        {
            text: '사번', dataIndex: 'EMP_NUMBER', width: 105
        },
        {text: '이름xx', dataIndex: 'EMP_NAME', width: 90},
        {text: '부서', dataIndex: 'HRDEPT_NAM', flex: 4},
        {text: '입사일', dataIndex: 'E_JOIN_DATE', width: 90, renderer: BRAVO.Format.dateRenderer},
        {text: '퇴사일', dataIndex: 'RETIRE_DATE', width: 90, renderer: BRAVO.Format.dateRenderer}
    ],
    store: 'EmpPayStore', //'EmpYearendStore',

    updateBindSelection: function(selModel, selection) {
        var me = this,
            clazz = Ext.panel.Table,
            plugin = me.findPlugin('globalsm-emppay');

        if (plugin) {
            plugin.updateBindSelection.call(me, selModel, selection);
        } else {
            clazz.prototype.updateBindSelection.call(me, selModel, selection);
        }
    }
});