/**
 * 사원리스트 single select 용
 */
Ext.define('BRAVO.grid.EmpSingleSelectWithEMail', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_emp_single_email',
    // onGridLoad: Ext.emptyFn,
    // onGridSelect: Ext.emptyFn,
    cls: 'title-header',
    columnHidden: true,
    rowNumberer: 40,

    columns: [
        {
            text: '사번', dataIndex: 'EMP_NUMBER', width: 95,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        },
        {
            text: '이름', dataIndex: 'EMP_NAM', width: 90,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        },
        {
            text: '부서', dataIndex: 'HRDEPT_NAM', width: 120,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        },
        {
            text: '입사일', dataIndex: 'E_JOIN_DATE', width: 80,
            /*            filter: {
                            type: 'string',
                            itemDefaults: {
                                emptyText: 'Search for...'
                            }
                        },*/
              renderer: BRAVO.Format.dateRenderer
        },
        {
            text: '퇴사일', dataIndex: 'RETIRE_DATE', width: 80,
            renderer: BRAVO.Format.dateRenderer
        },
        {
            text: '이메일', dataIndex: 'EMAIL', width: 95
        }
    ],
    storeProps: {
        url: '/payroll/salarymanual/emp_qry/list',
        autoLoad: true
    },
    dockedItems: [
        {
            xtype: 'toolbar',
            docked: 'top',
            padding: 5,
            items: [{
                xtype: 'textfield',
                itemId: 'payEmpSearchTxt',
                labelAlign: 'left',
                enableKeyEvents: true,
                emptyText: '사번/이름',
                listeners: {
                    keyup: {
                        buffer: 200,
                        fn: function () {
                            var v = this.inputEl.dom.value,
                                store = this.up("grid").getStore(),
                                filters = store.getFilters(),
                                empNo, empNm;

                            if (v) {
                                filters.beginUpdate();
                                filters.clear();
                                filters.add(function (r) {
                                    empNm = r.get('EMP_NAM');
                                    empNo = r.get('EMP_NUMBER');
                                    return empNm.indexOf(v) > -1 || empNo.indexOf(v) > -1;
                                });
                                filters.endUpdate();
                            } else {
                                store.clearFilter();
                            }
                        }
                    }
                }
            }]
        }
    ]

});