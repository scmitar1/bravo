Ext.define('BRAVO.grid.EmpSeverance', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.bravo_grid_emp_severance',
    title: '사원목록',
    selModel: {
        type: 'checkboxmodel',
        mode: 'multi' // ,
        /**
         * Synchronize header checker value as selection changes.
         * @private
         */
    },
    columns: [
        {text: '사번', dataIndex: 'EMP_NUMBER', width: 95,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        },
        {text: '이름', dataIndex: 'EMP_NAM', width: 90},
        {text: '입사일', dataIndex: 'E_JOIN_DATE', width: 85, renderer: BRAVO.Format.dateRenderer},
        {text: '퇴사일', dataIndex: 'RETIRE_DATE', width: 85, renderer: BRAVO.Format.dateRenderer},
        {text: '중간정산일', dataIndex: 'RETIREBASE_DT', hidden : true, width: 85, renderer: BRAVO.Format.dateRenderer},
        {text: '근속년수', dataIndex: 'SERVICE_YEAR', width: 90},
        {text: '퇴직사유', dataIndex: 'RETIRE_REASON', flex: 1}
    ],
    store: {
        autoLoad: false,
        fields: [],
        proxy: {
            autoLoad: false,
            type: 'ajax',
            url: '/payroll/multiretirecfg/target-emp/list'
        }
    }
});