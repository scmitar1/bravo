/**
 * 연말정산시 사원 선택하는 grid.
 */
Ext.define('BRAVO.grid.EmpIncumbentSingleSelect', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_incumbent_single',
    cls : 'title-header',
    columnHidden: true,
    rowNumberer: 0,
    listeners : {
        select : 'onEmpGridSelect'
    },
    columns: [
        { 
            text: '사번', dataIndex: 'EMP_NUMBER', width: 120,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        }, {
            text: '이름', dataIndex: 'EMP_NAME', flex: 1,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        }
    ],
    storeProps : {
        url: '/employee/list?WORK_FLAG=J',
        autoLoad: true
    }
});