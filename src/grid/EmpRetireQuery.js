Ext.define('BRAVO.grid.EmpRetireQuery', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_retire_query',
    title: '퇴직 사원목록',
    statics : {
        searchTxt : ''
    },
    
    initComponent: function () {
        var me = this;
        me.dateFrom = Ext.Date.format( Ext.Date.add( new Date(), Ext.Date.DAY, -365), 'Ymd');
        me.dateTo = Ext.Date.format(  new Date(), 'Ymd');
        Ext.apply( me, {
            columns: [
                {
                    text: '사번', dataIndex: 'EMP_NUMBER', width: 105, filter: {
                    type: 'string',
                    itemDefaults: {
                        emptyText: 'Search for...'
                    }
                }
                },
                {text: '이름', dataIndex: 'EMP_NAME', width: 90},
                {text: '부서', dataIndex: 'HRDEPT_NAMK', flex: 4},
                // {text: '입사일', dataIndex: 'E_JOIN_DATE', width: 90, renderer: BRAVO.Format.dateRenderer},
                {text: '퇴사일', dataIndex: 'RETIRE_DATE', width: 90, renderer: BRAVO.Format.dateRenderer}
            ],
            store: Ext.create( 'Ext.data.Store', {
             autoLoad: true,
             proxy: {
                    type: 'ajax',
                    url: '/payroll/retirement/pay-list',
                    method: 'POST',
                    extraParams: {
                        REPORT_TYPE: 'SEVPAY_RETIRE',
                        DATE_FROM: me.dateFrom,
                        DATE_TO: me.dateTo,
                        SORTBY: 'EMP_NUMBER'
                    },
                    reader: {
                        type: 'json',
                        rootProperty:  'list'
                    }
                }
            }),

            dockedItems: [{
                xtype: 'toolbar',
                docked: 'top', padding: 5,
                layout: { layout: 'hbox', align: 'center'},
                items: [{
                    xtype: 'datefield',
                    fieldLabel: '퇴직일 From', labelAlign: 'left', labelWidth: 70, width: 200,
                    name: 'DATE_FROM',
                    itemId: 'DATE_FROM',
                    value: Ext.Date.add( new Date(), Ext.Date.DAY, -365)
                }, {
                    xtype: 'datefield',
                    fieldLabel: '퇴직일 To', labelAlign: 'right', labelWidth: 70, width: 200,
                    name: 'DATE_TO',
                    itemId: 'DATE_TO',
                    value: new Date()
                }, '->', {
                    xtype: 'bravo_button_reset',
                    text: '다시 읽기',
                    handler: function(){
                        me.reloadGrid();
                    }
                }]
            },{
                xtype: 'toolbar',
                border: true,
                docked: 'top', padding: 5,
                items: [{
                    xtype: 'textfield',
                    itemId: 'payEmpSearchTxt',
                    labelAlign: 'left',
                    labelWidth: 180,
                    enableKeyEvents: true,
                    // fieldLabel: '결과내 검색(사번,성명)',
                    emptyText: '사번/이름 입력 후 Enter',
                    value : BRAVO.grid.EmpPayEntry.searchTxt,
                    listeners: {
                        keyup: function (field, event) {
                            BRAVO.grid.EmpPayEntry.searchTxt=field.getValue();
                            var gridStore = this.getStore();

                            gridStore.clearFilter();

                            var value = field.getValue();
                            gridStore.filterBy(function (rec) {


                                var EMP_NUMBER = rec.get('EMP_NUMBER');
                                var EMP_NAME = rec.get('EMP_NAME');
                                var isChecked = rec.get('isChecked')===true;
                                var isEqualEMP_NUMBER = Ext.isEmpty(value) ? true : EMP_NUMBER.search(value) != -1;
                                var isEqualEMP_NAME = Ext.isEmpty(value) ? true : EMP_NAME.search(value) != -1;

                                return isChecked || isEqualEMP_NUMBER || isEqualEMP_NAME;
                            });


                            var items = Ext.ComponentQuery.query('bravo_grid_emp_pay_entry');

                            for (var i = 0; i < items.length; i++) {
                                items[i].down('#payEmpSearchTxt').setValue(field.getValue());

                            }
                        },
                        scope: me
                    }
                }]
            }]
        });

        me.callParent(arguments);
    },
    
    reloadGrid: function(){
        var me = this;
        var dateFrom = me.down( '#DATE_FROM').getSubmitValue();
        var dateTo = me.down( '#DATE_TO').getSubmitValue();
        var store = me.getStore();
        store.reload({
            params: {
                DATE_FROM : dateFrom,
                DATE_TO: dateTo
          }
       });
    }
});