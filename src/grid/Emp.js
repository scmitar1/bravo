Ext.define('BRAVO.grid.Emp', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_emp',
    usePagingToolbar : false,
    title: '전체사원목록',
    onGridSelect: null,
    initComponent : function(){
        var me = this;

        Ext.apply(me,{
            store : Ext.StoreManager.lookup('EmpStore').copy(),
            columns: [
                { hidden: true, text: "시스템 사번", dataIndex: "EID", width: 100 },
                { text: '이름', dataIndex: 'EMP_NAME', width: 90,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                }, 
                { text: '사번', dataIndex: 'EMP_NUMBER', width: 105,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                { text: '부서', dataIndex: 'HRDEPT_NAM', flex: 1.5,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                { text: '직위', dataIndex: 'POSITION_NAME', flex: 1,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                { text: '입사일', dataIndex: 'JOIN_DATE' , width: 85,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                { text: '퇴사일', dataIndex: 'RETIRE_DATE' , width: 85,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                }
            ],
            dockedItems: [{
                xtype: 'toolbar',
                layout: 'hbox', padding: 5,
                dock: 'top',
                items: [ {
                    xtype: 'textfield',
                    width: 130,
                    emptyText: '사번/이름 Enter',
                    listeners: { 
                        specialKey: function(field, e) {
                            if (e.getKey() === e.ENTER) {
                                var value = field.getValue();
                                var gridStore = this.up('bravo_grid_emp').getStore();
                                gridStore.clearFilter();
                                
                                gridStore.filterBy(function (rec) {
                                    var EMP_NUMBER = rec.get('EMP_NUMBER');
                                    var EMP_NAME = rec.get('EMP_NAME');
                                    var isEqualEMP_NUMBER = Ext.isEmpty(value) ? true : EMP_NUMBER.search(value) != -1;
                                    var isEqualEMP_NAME = Ext.isEmpty(value) ? true : EMP_NAME.search(value) != -1;
                                    return isEqualEMP_NUMBER || isEqualEMP_NAME;
                                });
                            }
                        }
                    }
                }, '->', {
                    xtype: 'radiogroup',
                    // fieldLabel: '근무상태',
                    itemId: 'workStatusFlagRadio',
                    // labelAlign: 'right', labelWidth: 90, 
                    width: 150,
                    items: [{ boxLabel: '재직', name: 'WORK_FLAG', inputValue: 'J', checked: true },
                        { boxLabel: '퇴직', name: 'WORK_FLAG', inputValue: 'T' },
                        { boxLabel: '전체', name: 'WORK_FLAG', inputValue: 'K' }],
                    listeners: {
                        change: function(radio, newV) {
                            me.loadEmpGrid( newV["WORK_FLAG"]);
                        },
                        afterrender: function(radio, newV) {
                            me.loadEmpGrid( 'J');
                        }
                    }
                }]
            }]
        });

        me.callParent(arguments);
    },
    
    loadEmpGrid: function( flag){
        var me = this;
        me.loadFlag = flag;
        
        BRAVO.Ajax.request({
            url: '/employee/list',
            mode: 'POST',
            params: { "WORK_FLAG": flag},
            success: function(res){
                Ext.StoreManager.lookup('EmpStore').loadData(res, false);
                me.getStore().loadData(res, false);
            },
            scope: this
        });
    },
    
    /**
     * 다시읽어들이기.
     */
    reloadEmpGrid: function(){
        var me = this;
        me.loadEmpGrid( me.loadFlag || 'J');
    }
    
});