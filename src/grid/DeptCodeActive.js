Ext.define('BRAVO.grid.DeptCodeActive', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_deptcode_active',
    cls : 'title-header',
    columnHidden: true,
    rowNumberer: 25,
    INACTIVE : 'N',
    columns: [
        {text: '부서코드', dataIndex: 'DEPT_CODE', width:80},
        {text: '부서명', dataIndex: 'DEPT_NAM', flex:1},
        {text: '상위코드', dataIndex: 'PCC_PARENT', width:70}
    ],
    storeProps : {
        url : '/acct/code/dept/list',
        extraParams: {INACTIVE: 'N'}
    }

});