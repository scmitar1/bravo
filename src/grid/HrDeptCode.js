Ext.define('BRAVO.grid.HrDeptCode', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_hrdeptcode',
    cls : 'title-header',
    columnHidden: true,
    rowNumberer: 0,
    INACTIVE : null,
    columns: [
        {text: '부서코드', dataIndex: 'HRDEPT_CODE', width:80},
        {text: '부서명', dataIndex: 'HRDEPT_NAMK', flex:1},
        {text: '상위코드', dataIndex: 'PCC_PARENT', width:70},
        {text: '중지', dataIndex: 'INACTIVE', width: 55, renderer: BRAVO.Format.inactiveRenderer}
    ],
    storeProps : {
        url : '/hr/code/hr-dept/list'
    }

});