/**
 * 일용직 사원목록 리스트
 */
Ext.define('BRAVO.grid.EmpDailyWorkersPayEntry', {
    extend: 'BRAVO.grid.BaseMulti',
    alias: 'widget.bravo_grid_emp_daily_workers_pay_entry',
    title: '일용직 사원목록',
    statics : {
        searchTxt : ''
    },

    initComponent: function () {

        var me = this;
        Ext.apply(me, {
            columns: [
                {
                    text: '사번', dataIndex: 'EMP_NUMBER', width: 105,
                    filter: {
                        type: 'string',
                        itemDefaults: {
                            emptyText: 'Search for...'
                        }
                    }
                },
                {text: '이름', dataIndex: 'EMP_NAME', width: 90},
                {text: '부서', dataIndex: 'HRDEPT_NAM', flex: 4},
                {text: '입사일', dataIndex: 'E_JOIN_DATE', width: 90, renderer: BRAVO.Format.dateRenderer},
                {text: '퇴사일', dataIndex: 'RETIRE_DATE', width: 90, renderer: BRAVO.Format.dateRenderer},
                {text: '', dataIndex: 'DUMMY', width: 1}
            ],

            storeProps : {
                url : '/payroll/deduction/pay-empdailyworkers/list',
                autoLoad: true
            },

            dockedItems: [{
                xtype: 'toolbar',
                docked: 'top', padding: 5,
                items: [{
                    xtype: 'textfield',
                    itemId: 'payEmpSearchTxt',
                    labelAlign: 'left',
                    enableKeyEvents: true,
                    emptyText: '사번/이름 입력 후 Enter',
                    value : BRAVO.grid.EmpPayEntry.searchTxt,
                    listeners: {
                        keyup: function (field, event) {
                            BRAVO.grid.EmpPayEntry.searchTxt=field.getValue();
                            var gridStore = this.getStore();

                            gridStore.clearFilter();

                            var value = field.getValue();
                            gridStore.filterBy(function (rec) {
                                var EMP_NUMBER = rec.get('EMP_NUMBER');
                                var EMP_NAME = rec.get('EMP_NAME');
                                var isChecked = rec.get('isChecked')===true;
                                var isEqualEMP_NUMBER = Ext.isEmpty(value) ? true : EMP_NUMBER.search(value) != -1;
                                var isEqualEMP_NAME = Ext.isEmpty(value) ? true : EMP_NAME.search(value) != -1;

                                return isChecked || isEqualEMP_NUMBER || isEqualEMP_NAME;
                            });

                            var items = Ext.ComponentQuery.query('bravo_grid_emp_daily_workers_pay_entry');
                            for (var i = 0; i < items.length; i++) {
                                items[i].down('#payEmpSearchTxt').setValue(field.getValue());
                            }
                        },
                        scope: me
                    }
                }]
            }]
        });

        me.callParent(arguments);
    }

});