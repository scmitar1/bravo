/**
 * 사원리스트 single select 용
 */
Ext.define('BRAVO.grid.EmpSingleSelect', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_emp_single',
    // onGridLoad: Ext.emptyFn,
    // onGridSelect: Ext.emptyFn,
    cls: 'title-header',
    columnHidden: true,
    rowNumberer: 40,

    columns: [
        {
            text: '사번', dataIndex: 'EMP_NUMBER', width: 95,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        },
        {
            text: '이름', dataIndex: 'EMP_NAM', width: 90,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        },
        {
            text: '부서', dataIndex: 'HRDEPT_NAM', flex: 1.5,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        },
        {
            text: '고용형태', dataIndex: 'EMPTYPE_NAM', flex: 1,
            filter: {
                type: 'string',
                itemDefaults: {
                    emptyText: 'Search for...'
                }
            }
        },
        {
            text: '입사일', dataIndex: 'E_JOIN_DATE', width: 85,
            renderer: BRAVO.Format.dateRenderer

            /*            filter: {
                            type: 'string',
                            itemDefaults: {
                                emptyText: 'Search for...'
                            }
                        },*/
            //  renderer: BRAVO.Format.dateRenderer
        },
        {
            text: '퇴사일', dataIndex: 'RETIRE_DATE', width: 85,
            renderer: BRAVO.Format.dateRenderer
        }
    ],
    storeProps: {
        url: '/payroll/salarymanual/emp_qry/list',
        autoLoad: true
    }

    /*dockedItems: [
        {
            xtype: 'toolbar',
            docked: 'top',
            padding: 5,
            items: [{
                xtype: 'textfield',
                itemId: 'payEmpSearchTxt',
                labelAlign: 'left',
                enableKeyEvents: true,
                emptyText: '사번/이름...',
                listeners: {
                    keyup: {
                        buffer: 200,
                        fn: function () {
                            var v = this.inputEl.dom.value,
                                store = this.up("grid").getStore(),
                                filters = store.getFilters(),
                                empNo, empNm, hrdeptNm, emptypeNm;

                            if (v) {
                                debugger;
                                filters.beginUpdate();
                                filters.clear();
                                filters.add(function (r) {
                                    empNm = r.get('EMP_NAM');
                                    empNo = r.get('EMP_NUMBER');
                                    hrdeptNm = r.get('HRDEPT_NAM');
                                    emptypeNm = r.get('EMPTYPE_NAM');
                                    return empNm.indexOf(v) > -1 || empNo.indexOf(v) > -1 || hrdeptNm.indexOf(v) > -1 || emptypeNm.indexOf(v) > -1
                                        ;
                                });
                                filters.endUpdate();
                            } else {
                                store.clearFilter();
                            }
                        }
                    }
                }
            }]
        }
    ]*/

});