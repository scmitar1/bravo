Ext.define('BRAVO.grid.PayDate', {
    extend: 'BRAVO.grid.Base',
    alias: 'widget.bravo_grid_paydate',
    // onGridLoad: Ext.emptyFn,
    // onGridSelect: Ext.emptyFn,
    cls : 'title-header',
    columnHidden: true,
    rowNumberer: 0,

    columns: [
        {text: '급여월', dataIndex: 'PAY_MONTH', width:70, renderer: BRAVO.Format.dateRenderer},
        {text: '차수', dataIndex: 'SEQ', width:50,},
        {text: '급여일', dataIndex: 'PAY_DATE', width:85, renderer: BRAVO.Format.dateRenderer},
        {text: '시작일', dataIndex: 'START_DATE', width:80, renderer: BRAVO.Format.dateRenderer},
        {text: '종료일', dataIndex: 'END_DATE', width:80, renderer: BRAVO.Format.dateRenderer},
        {text: '상태', dataIndex: 'CLOSE_FLAG', hidden:true},
        {
            text: '상태', dataIndex: 'CLOSE_FLAG_DESC', width: 75,
            renderer: BRAVO.Format.isOpenRenderer
        },
        // {text: '개인조회숨김', dataIndex: 'HIDE_INDIVIDUALQRY', flex:1, renderer: BRAVO.Format.inactiveRenderer
        //     , hidden: this.columnHidden},
        {text: '비정기 급여', dataIndex: 'SPECIALPAY_FLAG', flex:1, renderer: BRAVO.Format.inactiveRenderer
            , hidden: this.columnHidden}
    ],
    storeProps : {
        url : '/payroll/code/pay-date/list'
    }


});