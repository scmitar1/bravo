Ext.define('BRAVO.plugin.GridValidatorPlugin', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.gridvalidator',
    config: {
        editType: 'widgetcolumn'    // 'cellediting'
    },

    init: function (tablepanel) {
        tablepanel.validate = this.validate;
    },

    validate: function () {
        var me = this.findPlugin('gridvalidator'),
            grid = me.getCmp(),
            store = grid.getStore(),
            invalidRec = (function (store) {
                var idx = store.findBy(function (r) {
                    return !r.getValidation(true).isValid();
                });
                if (idx > -1) {
                    return store.getAt(idx);
                }
            })(store), validation, errorField, errorCol;

        if (invalidRec) {
            validation = invalidRec.getValidation();
            Ext.Object.each(validation.getData(), function (field, msg) {
                if (Ext.isString(msg)) {
                    errorField = field;
                    errorCol = grid.columnManager.getHeaderByDataIndex(field);
                    if (me[me.getEditType() + 'Callback']) {
                        me[me.getEditType() + 'Callback'](grid, msg, errorCol, invalidRec);
                    }
                    return false;
                }
            });
            return false;
        }
        return true;
    },

    widgetcolumnCallback: function (grid, msg, errorCol, invalidRec) {
        var errorMsg = Ext.String.format('{0}: {1}', errorCol.text, msg);
        Ext.Msg.alert('알림', errorMsg, function () {
            grid.down('tableview').scrollRowIntoView(invalidRec);
            errorCol.getWidget(invalidRec) && errorCol.getWidget(invalidRec).focus();
        });
    },

    celleditingCallback: function (grid, msg, errorCol, invalidRec) {
        //TODO: cellediting을 이용한 수정 모드의 경우
    }
});