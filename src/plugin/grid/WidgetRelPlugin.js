/**
 * Created by Hynkuk on 2018-01-30.
 */
Ext.define('BRAVO.plugin.grid.WidgetRelPlugin', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.widgetrel',
    config: {
        target: null,
        afterNull: true,
        callback: Ext.returnTrue
    },

    init: function (cmp) {
        var me = this,
            grid, store;

        fn = function () {
            grid = cmp.ownerGrid;
            store = grid && grid.store;

            if (store) {
                me.bindStoreEvent(me.findStore(grid));
            } else {
                Ext.Function.defer(fn, 200, me);
            }
        };
        fn();
    },

    findStore: function (grid) {
        var store = grid.store,
            storeBinding = grid.getBind() && grid.getBind()['store'];

        if (storeBinding) {
            store = storeBinding.getRawValue();
        }
        return store;
    },

    bindStoreEvent: function (store) {
        var me = this,
            col = me.getCmp(),
            grid = col.ownerGrid,
            dataIndex = col.dataIndex,
            targetList, widget, vc,
            targetCol, targetWidget, callback;

        store.on('update', function (store, rec, op, fields) {
            if (op === 'commit') {
                return;
            }
            if (Ext.Array.contains(fields, dataIndex)) {
                targetList = Ext.Array.from(me.getTarget() || []);
                callback = me.getCallback();
                widget = me.getCmp().getWidget(rec);

                Ext.each(targetList, function (target) {
                    targetCol = grid.columnManager.getHeaderByDataIndex(target);
                    targetWidget = targetCol.getWidget(rec);
                    targetCol.onWidgetAttach(targetCol, targetWidget, rec);

                    if (me.getAfterNull()) {
                        rec.set(target, null);
                    }
                });
                if (Ext.isFunction(callback)) {
                    callback.apply(widget, [col, widget, rec]);
                } else if (Ext.isString(callback)) {
                    vc = col.lookupController();
                    if (Ext.isFunction(vc[callback])) {
                        vc[callback].apply(vc, [col, widget, rec]);
                    } else {
                        Ext.log.warn(Ext.String.format('viewController에 {0} 메소드가 없습니다'));
                    }
                }

            }
        });
    }
});