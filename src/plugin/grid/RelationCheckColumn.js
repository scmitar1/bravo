Ext.define('BRAVO.plugin.grid.RelationCheckColumn', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.relationcheckcolumn',
    config: {
        parentDataIndex: null,
        childDataIndex: null
    },

    init: function (cmp) {
        var plugin = this;
        var colMn = cmp.columnManager;
        var parentCol = colMn.getHeaderByDataIndex(this.getParentDataIndex());
        var childCol = colMn.getHeaderByDataIndex(this.getChildDataIndex());

        parentCol.setRecordCheck = plugin._setRecordCheck;
        Ext.apply(childCol, {
            areAllChecked: plugin._areAllChecked,
            renderer: plugin._renderer,
            toggleAll: plugin._toggleAll
        });
    },

    /* parent Column override*/
    _setRecordCheck: function (record, recordIndex, checked, cell) {
        var fn = Ext.grid.column.Check.prototype.setRecordCheck;
        var grid = this.getView() && this.getView().ownerGrid;
        var plugin = grid && grid.findPlugin('relationcheckcolumn');
        var me = this;
        var view = me.getView();
        fn.call(me, record, recordIndex, checked, cell);
        if (plugin && !checked) {
            record.set(plugin.getChildDataIndex(), false);
        }
        view.refreshNode(record);
    },

    /* child Column override */
    _areAllChecked: function () {
        var me = this;
        var grid = this.getView() && this.getView().ownerGrid;
        var plugin = grid && grid.findPlugin('relationcheckcolumn');
        var store = me.getView().getStore();
        var records = store.queryRecords(plugin.getParentDataIndex(), true);
        var i;
        if (records.length) {
            for (i=0; i < records.length ;i++) {
                if(!me.isRecordChecked(records[i])) {
                    return false;
                }
            }
            return true;
        }
    },

    _renderer: function (value, cellValues) {
        var fn = Ext.grid.column.Check.prototype.defaultRenderer;
        var grid = this.getView() && this.getView().ownerGrid;
        var plugin = grid && grid.findPlugin('relationcheckcolumn');
        var me = this;
        var rec = cellValues && cellValues.record;

        if (!rec.get(plugin.getParentDataIndex())) {
            cellValues.tdCls += ' ' + me.disabledCls;
        }
        return fn.call(me, value, cellValues);
    },

    _toggleAll: function(e) {
        var me = this,
            view = me.getView(),
            store = view.getStore(),
            checked = !me.allChecked,
            position, text, anncEl;

        if (me.fireEvent('beforeheadercheckchange', me, checked, e) !== false) {

            // Only create and maintain a CellContext if there are consumers
            // in the form of event listeners. The event is a click on a
            // column header and will have no position property.
            if (me.hasListeners.checkchange || me.hasListeners.beforecheckchange) {
                position = e.position = new Ext.grid.CellContext(view);
            }
            store.each(function(record, recordIndex) {
                if (record.get('SHOW_FLAG')) {
                    me.setRecordCheck(record, recordIndex, checked, view.getCell(record, me));
                }
            });

            me.setHeaderStatus(checked, e);
            me.fireEvent('headercheckchange', me, checked, e);
        }
    }
});