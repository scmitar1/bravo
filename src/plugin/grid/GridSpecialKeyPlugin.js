/*
* 그리드 내부에서 WidgetColumn들만 enterKey시 column 이동 및 신규 행 추가
* */
Ext.define('BRAVO.plugin.GridSpecialKeyPlugin', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.gridspecialkey',
    config: {
        // 행을 추가할때 기초데이터를 추가하는 부분: string, function(store)
        addRowFn: null
    },

    init: function (panel) {
        var me = this;

        var settings = function (panel) {
            panel.keyNav = new Ext.util.KeyNav({
                target: panel.el,
                enter: {
                    scope: me,
                    fn: function (e) {
                        var panel = this.getCmp(),
                            target = e.target && Ext.get(e.target),
                            cmp, field, rec, col, cols, nextCol, grid, nextFocus, wXtype,
                            focusFn = function (rec, col) {
                                nextFocus = col.getWidget(rec);

                                if (nextFocus) {
                                    nextFocus.focus();
                                    if (nextFocus.isFormField) {
                                        Ext.defer(function() {
                                            nextFocus.focusEl.dom.select()
                                        }, 50);
                                    }
                                }
                            };

                        if (target && target.component && target.component.isXType('field')) {
                            cmp = target.component;
                            cols = panel.query('widgetcolumn');
                            rec = cmp.getWidgetRecord();
                            col = cmp.getWidgetColumn();
                            nextCol = cols[Ext.Array.indexOf(cols, col) +1];

                            if (nextCol) {
                                grid = nextCol.up("grid");
                            } else {
                                nextCol = cols[0];
                                grid = nextCol.up('grid');
                                rec = rec.store.getAt(rec.store.indexOf(rec) + 1);
                            }
                            if (!rec) {
                                rec = Ext.Array.from(this.addRow(grid.store));
                                rec = rec[0] || grid.store.last();
                                focusFn(rec, nextCol);
                            }
                            focusFn(rec, nextCol);
                        }
                    }
                }
            });
        };
        if (panel.rendered) {
            settings(panel);
        } else {
            panel.addListener('afterrender', function () {
                settings(this);
            });
        }
    },

    destroy: function () {
        var cmp = this.getCmp(),
            keyNav = cmp.keyNav;

        if (keyNav) {
            keyNav.destroy();
        }
        this.callParent();
    },

    addRow: function (store) {
        var fn = this.getAddRowFn(),
            vc = this.getCmp().lookupController();

        if (Ext.isString(fn)) {
            return vc[fn].call(vc, store);
        } else if (Ext.isFunction(fn)) {
            return fn.call(this.getCmp(), store);
        }
        return store.add({});
    }
});