Ext.define('BRAVO.plugin.grid.VmStoreRendererPlugin', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.vmstorerenderer',
    config: {
        rendererStoreMap: {
            /*          renderer: store
            /*          ynRenderer: 'yes_comm_yesno',*/
        },
        valueField: 'val',
        displayField: 'desc'
    },
    init: function (cmp) {
        var me = this;
        var vm = cmp.lookupViewModel();
        var vc = cmp.lookupController();
        var map = this.getRendererStoreMap();
        if (vc && vm) {
            Ext.Object.each(map, function (fn, v) {
                var valueField = me.getValueField();
                var displayField = me.getDisplayField();
                var store;

                if (Ext.isString(v)) {
                    store = vm.getStore(v);
                } else if (Ext.isObject(v)) {
                    store = Ext.isString(v.store) ? vm.getStore(v.store) : v.store;
                    valueField = v.valueField || valueField;
                    displayField = v.displayField || displayField;
                }

                if (!store.isStore) {
                    return;
                }

                if (store.proxy.type == 'ajax' && !store.isLoaded() && !store.isLoading()) {
                    store.flushLoad();
                }

                vc[fn] = function (val) {
                    var rec;
                    if (Ext.isDefined(val)) {
                        rec = store.findRecord(valueField, val);
                        if (rec) {
                            return rec.get(displayField)
                        }
                    }
                    return val;
                }
            });
        }
    }
});