/**
 * Created by Hynkuk on 2017-11-24.
 */
Ext.define('BRAVO.plugin.grid.column.Check', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.ext-checkcolumn',
    config: {
        disableCondition: Ext.emptyFn
    },

    init: function (col) {
        var me = this;

        if (col.isXType('checkcolumn')) {
            col.renderer = me.renderer;
        }
    },

    renderer: function (value, cellValues, record) {
        var me = this,
            cls = me.checkboxCls,
            plugin = me.findPlugin('ext-checkcolumn'),
            fn = plugin.getDisableCondition(),
            tip = me.tooltip;

        if (me.invert) {
            value = !value;
        }
        if (me.disabled) {
            cellValues.tdCls += ' ' + me.disabledCls;
        }
        if (fn(record)) {
            cellValues.tdCls += ' ' + me.disabledCls;
        }
        if (value) {
            cls += ' ' + me.checkboxCheckedCls;
            tip = me.checkedTooltip || tip;
        }

        if (me.useAriaElements) {
            cellValues.tdAttr += ' aria-describedby="' + me.id + '-cell-description' +
                (!value ? '-not' : '') + '-selected"';
        }

        // This will update the header state on the next animation frame
        // after all rows have been rendered.
        me.updateHeaderState();

        return '<span ' + (tip || '') + ' class="' + cls + '" role="' + me.checkboxAriaRole + '"' +
            (!me.ariaStaticRoles[me.checkboxAriaRole] ? ' tabIndex="0"' : '') +
            '></span>';
    }
});