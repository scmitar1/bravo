Ext.define('BRAVO.plugin.SpecialKeyPlugin', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.specialkey',
    config: {
        selector: 'field:not([hidden]):not([readOnly]),button',
        keyNav: null,
        // ctrl + s handler (function, string)
        ctrlS: null
    },

    init: function (cmp) {
        var me = this;

        var setting = function (cmp) {
            me.keyNav = new Ext.util.KeyNav({
                target: cmp.el,

                // enter key
                enter: function (e) {
                    var cmp = this.cmp;
                    var target = Ext.get(e.target);
                    var fieldEl = target.up('[class*=x-field]:not([data-ref])');
                    var field = fieldEl && Ext.getCmp(fieldEl.id);
                    var next;
                    if (field) {
                        next = me.findNext(cmp, field);
                        next && next.focus();
                        if (next && next.isFormField) {
                            Ext.defer(function () {
                                next.focusEl.dom.select();
                            }, 50);
                        }
                    }
                },

                // ctrl + s key
                S: {
                    ctrl: true,
                    //defaultEventAction: Ext.emptyFn,
                    fn: function (e) {
                        var me = this;
                        var cmp = this.cmp;
                        var vc = cmp.lookupController();
                        if (Ext.isString(me.ctrlS)) {
                            vc = cmp.lookupController();
                            if (Ext.isFunction(vc[me.ctrlS])) {
                                vc[me.ctrlS](e, cmp);
                            } else {
                                Ext.log.warn(Ext.String.format(
                                    'viewController({0})에 상응하는 메소드가 없습니다',
                                    vc.$className
                                ));
                            }
                        } else if (Ext.isFunction(me.ctrlS)) {
                            me.ctrlS(e, cmp);
                        }
                        e.stopEvent();
                    }
                },
                scope: me
            });
        };
        if (cmp.rendered) {
            setting(cmp);
        } else {
            cmp.addListener('afterrender', function () {
                setting(this);
            });
        }
    },

    findNext: function (cmp, field) {
        var me = this;
        var fields = cmp.query(this.getSelector());
        var nextIdx = Ext.Array.indexOf(fields, field) > -1 ? Ext.Array.indexOf(fields, field) + 1 : null;
        var next = nextIdx ? fields[nextIdx] : null;
        if (!next) {
            return null;
        }
        if (next.up('[hidden], [collapsed]')) {
            return me.findNext(cmp, next);
        }
        return next;
    }
});