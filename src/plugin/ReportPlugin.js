
Ext.define('BRAVO.plugin.ReportPlugin', {
    extend: 'Ext.AbstractPlugin',
    alias: 'plugin.pmh-report',
    dataPrefixIndex : null,
    dataPrefix :'',

    init: function (form) {
        var me = this;
        me.form = form;
        me.form.convertMultiRowData= Ext.Function.bind(me.convertMultiRowData,me);
        me.form.getForm().isDirty = Ext.Function.bind(me.isDirty,me);

        if(me.dataPrefixIndex){
            me.form.getForm().getValues = Ext.Function.bind(me.getValues,me);
        }


        me.form.addListener('afterrender',me.onAfterRender,me);
    },
    isDirty: function() {

        var basicForm = this.form.getForm();

        return !!basicForm.getFields().findBy(function(f) {
            return f.autoInput==true ? false :  f.isDirty();
        });
    },
    onAfterRender : function(comp){
        var me = this;
        var component = comp.getController() || comp;
        var tableProps = Ext.isFunction(component.getTableProps) ? component.getTableProps() :null;
        var dataProps = Ext.isFunction(component.getDataProps) ? component.getDataProps() :{};
        if(tableProps!=null){
            this.applyReportItems(me.form.tdAttrs, component, tableProps, dataProps);
        }
    },

    convertMultiRowData: function (datas,dataPrefixIndex) {
        var me = this;
        var dataObject = {};
        var dataPrefixIndex = dataPrefixIndex ||me.dataPrefixIndex;
        if(Ext.isEmpty(datas)){
            return [];
        }

        datas = Ext.isArray(datas) ? datas : [datas];

        if(Ext.isEmpty(datas[0][dataPrefixIndex])){
            return datas[0];
        }



        for (var i = 0; i < datas.length; i++) {
            var data = datas[i];

            Ext.iterate(data, function (key, value) {
                dataObject[data[dataPrefixIndex] + '_' + key] = value;
            });
        }

        return dataObject;
    },
    getValues : function(){
        var me = this;
        var fields = me.form.getForm().getFields().items;
        var result = [];
        var dataPrefixIndex = me.dataPrefixIndex;

        if(dataPrefixIndex){

            var tempObj = {};
            for(var i=0;i<fields.length;i++){

                var currentField = fields[i];
                var nextField = fields[i+1];
                tempObj[currentField.dataIndex] = currentField.getSubmitValue();
                if(Ext.isEmpty(nextField) || (currentField.dataPrefix!= nextField.dataPrefix)){

                    tempObj[dataPrefixIndex]=currentField.dataPrefix;
                    result.push(tempObj);
                    tempObj ={};
                }
            }
            return result.length==1 ? result[0] : result;
        }
    },

    createFieldItem : function (tdAttrs,key, prop, inputProp) {


        var result = [];

        var headers = prop.headers ||[];

        for (var i = 0; i < headers.length; i++) {

            var header = headers[i];

            var attr = Ext.clone(tdAttrs);

            Ext.merge(attr.style,header);

            delete attr.style.rowspan;
            delete attr.style.colspan;
            delete attr.style.value;


            if(Ext.isNumber(attr.style.height)){
                attr.style.height+='px';
            }
            if(Ext.isNumber(attr.style.width)){
                attr.style.width+='px';
            }




            var item={};
            if(header['xtype']){


                item = header;
                var cellCls = '';
                if(item.readOnly===true){
                    cellCls =item.readOnlyInputCls || 'field-readonly';
                }
                if(item.autoInput===true){
                    item.readOnly= true;
                    item.readOnlyInputCls =item.readOnlyInputCls|| 'field-auto-data';
                    cellCls =item.readOnlyInputCls;

                }

                item.tdAttrs=attr;

                item.width= item.width || '100%';
                item.height = item.height || '';
                item.cellCls = cellCls;
                item.cls    = 'hide-input';

            }else{

                item = {
                    xtype  : 'component',
                    margin : 0,
                    data : {
                        PER_MONTH : 'Y',
                        PER_CORRECT : 'Y'
                    },
                    name : header['name'],
                    rowspan: header['rowspan'],
                    colspan: header['colspan'],
                    width : header['width'],
                    tdAttrs: attr,
                    style : header.style,
                    tpl : header['value'] || '&nbsp;',
                    listeners : header['listeners']
                };
            }
            result.push(item);

        }

        var datas = prop.datas;

        if (Ext.isEmpty(datas)) {
            return result;
        }


        var fields = Ext.clone(inputProp);
        Ext.iterate(fields, function (name, fieldProp) {


            var prefix = (key == 'HEADER') ? '' : key + '_';

            fieldProp.name   = prefix + name;
            fieldProp.height = '100%';
            fieldProp.cls    = 'hide-input';
            fieldProp.style  = 'margin: 0px 0px 0px 0px;';
            fieldProp.dataIndex = name;
            fieldProp.dataPrefix = (key == 'HEADER') ? '' : key;
            //    fieldProp.emptyText = prefix + name;
            Ext.apply(fieldProp, datas[name]);
            var attr = Ext.clone(tdAttrs);

            var tdAttr = {
                borderWidth : fieldProp.borderWidth
            };

            var width = fieldProp.width;
            if(Ext.isNumber(fieldProp.width)){

                tdAttr.width=width+'px';
            }

            Ext.merge(attr.style,tdAttr);

            var cellCls = '';

            if(fieldProp.readOnly===true){
                cellCls =fieldProp.readOnlyInputCls || 'field-readonly';
            }
            if(fieldProp.autoInput===true){
                fieldProp.readOnly= true;
                fieldProp.readOnlyInputCls='field-auto-data';

                cellCls =fieldProp.readOnlyInputCls;
            }




            delete attr.style.rowspan;
            delete attr.style.colspan;
            delete attr.style.value;

            if(Ext.isNumber(attr.style.height)){
                attr.style.height+='px';
            }

            var field = {
                xtype   : 'container',
                layout  : {
                    type : 'vbox',
                    align: 'stretch'
                },
                width   : '100%',
                tdAttrs : attr,
                cellCls : cellCls,
                defaults: {
                    flex: 1
                }, items: [fieldProp]

            };
            result.push(field);
        });

        return result;

    },
    applyReportItems: function (tdAttrs,component, fieldProps, inputProp) {

        var me    = this;
        var items = [];
        Ext.iterate(fieldProps, function (key, prop) {
            items = items.concat(me.createFieldItem(tdAttrs,key, prop, inputProp));
        });
        component.add(items);

    }


});