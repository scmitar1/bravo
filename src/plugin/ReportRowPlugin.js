/**
 * Report Row Plug-in
 */
Ext.define('BRAVO.plugin.ReportRowPlugin', {
    extend: 'BRAVO.plugin.ReportPlugin',
    alias: 'plugin.ycpark-report-row',
    defaultRow: 9,
    startIndex: 1,
    dataPrefixIndex: null,
    dataPrefix: '',
    availableRow: 1,
    setRowStyle: Ext.emptyFn,
    setRowFieldProp: Ext.emptyFn,

    init: function (form) {
        var me = this;
        me.form = form;
        me.form.currentRow = 1;
        me.form.addRow = Ext.Function.bind(me.addRow, me);
        me.form.convertMultiRowData = Ext.Function.bind(me.convertMultiRowData, me);
        me.form.getForm().setValues = Ext.Function.bind(me.setValues, me);
        me.form.getForm().getValues = Ext.Function.bind(me.getValues, me);
        me.form.addListener('afterrender', me.createDefaultRow, me);
    },

    addRow: function (rowIndex) {
        var me = this;
        var item = me.createRow(rowIndex);
        me.form.add(item);
    },

    setDefaultFieldProps: function (fieldProp) {

        var me = this;
        var scope = me.form.up('global_default_view');
        fieldProp.cls = 'hide-input';

        switch (fieldProp.xtype) {
            case 'numericfield' :
                fieldProp.fieldStyle = 'text-align:right;';
                fieldProp.hideTrigger = true;
                break;
        }

        if (Ext.isEmpty(fieldProp.listeners)) {
            fieldProp.listeners = {}
        }

        Ext.apply(fieldProp.listeners, {
            blur: function (field) {
                if (!Ext.isEmpty(field.getValue())) {
                    if (me.form.currentRow == field.rowIndex) {
                        me.addRow(field.rowIndex + 1);
                        me.form.currentRow = field.rowIndex + 1;
                    }
                }
            }
        });

        return fieldProp;
    },

    createDefaultRow: function (comp) {
        var me = this;
        me.form.removeAll();
        var applyData = [];

        for (var i = me.startIndex; i <= me.defaultRow; i++) {
            applyData = applyData.concat(this.createRow(i));
        }
        me.form.add(applyData);

    },
    reset: function () {
        var fn = Ext.Form.Panel.prototype.reset,
            me = this;
        fn.call(me.form);
        me.createDefaultRow();
    },
    setValues: function (data) {


        var me = this;
        me.form.removeAll();


        var datas = Ext.isArray(data) ? data : Ext.isEmpty(data) ? [] : [data];


        var applyData = [];

        me.form.currentRow = datas.length;
        for (var i = 1; i <= datas.length || i <= me.defaultRow; i++) {


            applyData = applyData.concat(this.createRow(i, datas[i - 1], datas.length == i));
        }
        applyData = applyData.concat(this.createRow(i));

        me.form.add(applyData);


    },
    getValues: function () {
        var me = this;

        var result = [];

        for (var i = 1; i <= me.form.currentRow - 1; i++) {

            var fields = me.form.query('field[rowIndex=' + i + '][value!=][autoInput!=true]');

            if (fields.length == 0) continue;
            fields = me.form.query('field[rowIndex=' + i + ']');


            var tempObj = {};
            for (var j = 0; j < fields.length; j++) {
                var field = fields[j];
                tempObj[field.dataIndex] = field.getValue();
            }
            tempObj[me.dataPrefixIndex] = fields[0].dataPrefix;
            result.push(tempObj);
        }

        return result;
    }
});