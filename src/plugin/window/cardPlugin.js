/**
 * Created by Hynkuk on 2018-02-09.
 */
Ext.define('BRAVO.plugin.window.cardPlugin', {
    extend: 'Ext.AbstractPlugin',
    alias: 'plugin.window-card',
    init: function (cmp) {
        var w, main, body, width, height, position,
            dBody = document.body,
            mainId = dBody.getAttribute('data-componentid');

        if (cmp.isXType('window')) {
            w = cmp;
            w.modal = true;
            w.autoShow = true;
            main = Ext.getCmp(mainId);
            body = main.down('tabpanel').body;
            position = main.down('tabpanel').getActiveTab().getPosition();
            width = body.getWidth();
            height = body.getHeight();
            w.setPosition(position);
            w.setSize(width, height);
        }
    }
});