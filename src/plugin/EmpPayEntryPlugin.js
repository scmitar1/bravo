Ext.define('BRAVO.plugin.EmpPayEntryPlugin', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.emp-pay-entry',
    config: {
        selectHandler: 'onGlobalPayEmpGridSelect'
    },
    init: function (cmp) {
        var plugin = this;
        if (cmp.isXType('box')) {
            if (Ext.getClassName(cmp.getLayout()) == 'Ext.layout.container.Border') {
                cmp.add({
                    xtype: 'bravo_grid_emp_pay_entry',
                    region: cmp.down('> [region=center]') ? 'west' : 'center',
                    isGlobal: true,
                    title: '사원 리스트',
                    split: {width: 5},
                    itemId: 'globalPayEmpGrid',
                    collapsible: cmp.down('> [region=center]') ? true : false,
                    collapsed: cmp.down('> [region=center]') ? true : false,
                    width: 550,
                    listeners: {
                        select: function (sm, rec, idx) {
                            var me = this,
                                vc = me.lookupController(),
                                fn = plugin.getSelectHandler();
                            if (vc && Ext.isFunction(vc[fn])) {
                                vc[fn].call(vc, sm, rec, idx);
                            }
                        }
                    }
                });
            } else {
                Ext.log.error('border레이아웃에 적용하는 plugin입니다');
            }
        } else {
            Ext.log.error('panel내지는 container에 적용하는 plugin입니다');
        }
    }
});