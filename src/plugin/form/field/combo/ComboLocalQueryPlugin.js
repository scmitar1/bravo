/**
 * Created by anjin05 on 2017-08-30.
 */
Ext.define('BRAVO.plugin.form.field.combo.ComboLocalQueryPlugin', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.combolocalquery',
    config: {
        showDisplayValueTpl: true,
        showFieldTpl: false,
        invert: false,
        matchFieldWidth: null
    },

    init: function (combo) {
        var me = this,
            picker = combo.getPicker();

        combo.doLocalQuery = Ext.Function.clone(me.doLocalQuery);
        if (me.getShowDisplayValueTpl()) {
            picker.tpl = me.getBoundListTpl();

            if (Ext.isBoolean(me.getMatchFieldWidth())) {
                combo.matchFieldWidth = me.getMatchFieldWidth();
            } else {
                combo.matchFieldWidth = false;
            }

        }
        if (me.getShowFieldTpl()) {
            me.setDisplayTpl();
        }
    },

    doLocalQuery: function (queryPlan) {
        var me = this,
            queryString = queryPlan.query,
            store = me.getStore(),
            value = queryString,
            filter;
        me.clearLocalFilter();
        // Querying by a string...
        if (queryString) {
            // User can be typing a regex in here, if it's invalid
            // just swallow the exception and move on
            if (me.enableRegEx) {
                try {
                    value = new RegExp(value);
                } catch (e) {
                    value = null;
                }
            }
            if (value !== null) {
                // Must set changingFilters flag for this.checkValueOnChange.
                // the suppressEvents flag does not affect the filterchange event
                me.changingFilters = true;
                filter = me.queryFilter = new Ext.util.Filter({
                    id: me.id + '-filter',
                    filterFn: function (item) {
                        var v = item.get(me.valueField) || '',
                            d = item.get(me.displayField) || '',
                            reg,
                            fn = function (v) {
                                reg = Ext.String.createRegex(value, false, false, me.caseSensitive);
                                if (me.anyMatch) {
                                    return v.search(reg) > -1;
                                }
                                return v.search(reg) === 0;
                            };
                        return fn(v) || fn(d);
                    }
                });
                store.addFilter(filter, true);
                me.changingFilters = false;
            }
        }
        // Expand after adjusting the filter if there are records or if emptyText is configured.
        if (me.store.getCount() || me.getPicker().emptyText) {
            // The filter changing was done with events suppressed, so
            // refresh the picker DOM while hidden and it will layout on show.
            me.getPicker().refresh();
            me.expand();
        } else {
            me.collapse();
        }
        me.afterQuery(queryPlan);
    },

    getBoundListTpl: function () {
        var combo = this.getCmp(),
            me = this;

        return new Ext.XTemplate([
            //@formatter:off
            '<ul class="x-list-plain">',
                '<tpl for=".">',
                    '<li role="option" unselectable="on" class="x-boundlist-item">',
                        '{[this.getDisplay(values)]}',
                    '</li>',
                '</tpl>',
            '</ul>',
            //@formatter:on
            {
                getDisplay: function (values) {
                    var d = values[combo.displayField],
                        v = values[combo.valueField],
                        invert = me.getInvert(),
                        format = invert ? '{1} - {0}' : '{0} - {1}';

                    if (d && v) {
                        return Ext.String.format(format, d , v);
                    }
                    return d;
                }
            }
        ])
    },

    setDisplayTpl: function () {
        var me = this,
            combo = this.getCmp();

        combo.setDisplayTpl([
            '<tpl for=".">',
            '{[typeof values === "string" ? values : this.getDisplay(values)]}',
            '<tpl if="xindex < xcount">' + combo.delimiter + '</tpl>',
            '</tpl>',
            {
                getDisplay: function (values) {
                    var label = values[combo.displayField],
                        code = values[combo.valueField],
                        format = me.getInvert() ? '{1} - {0}' : '{0} - {1}';

                    if (label && code) {
                        return Ext.String.format(format, label , code);
                    }
                    return code;
                }
            }
        ]);
    }
});