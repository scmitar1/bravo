Ext.define('BRAVO.plugin.AutoCompletePlugin', {
    extend: 'Ext.plugin.Abstract',
    alias: 'plugin.autocomplete',
    config: {
        winTitle: null,
        pop: {
            winTitle: null,
            width: 600,
            height: 400,
            columns: null,
            storeProps: null,
            fields: []
        }
    },

    init: function (cmp) {
        var me = this,
            picker = Ext.clone(Ext.form.field.Picker.prototype.config.triggers.picker),
            spinner = Ext.clone(Ext.form.field.Spinner.prototype.config.triggers.spinner),
            search = this.getSearchTrigger(this),
            triggerCfg = {
                search: search
            };

        if (!cmp.isXType('pickerfield')) {
            if (cmp.isXType('spinnerfield')) {
                Ext.apply(triggerCfg, {
                    spinner: spinner
                });
            }
        } else if (cmp.isXType('datefield')) {
            Ext.apply(triggerCfg, {
                picker: picker
            });
        } else {
            Ext.apply(triggerCfg, {
                picker: picker
            });
            Ext.apply(cmp, {
                queryParam: cmp.name,
                typeAhead: cmp.queryMode === 'remote',
                minChars: 1
            });
            if (cmp.store.isEmptyStore && this.pop.storeProps) {
                cmp.setStore({
                    autoLoad: cmp.queryMode === 'local' && this.pop.storeProps.autoLoad,
                    fields: [],
                    proxy: {
                        type: 'ajax',
                        url: this.pop.storeProps.url,
                        reader: {
                            type: 'json'
                        }
                    }
                });
            }

            cmp.on('select', function (field, rec) {
                var plugin = field.findPlugin(me.ptype),
                    pop = plugin.pop,
                    data = {},
                    gridView,
                    gridRowRec;

                Ext.each(pop.fields, function (itm) {
                    data[itm] = rec.get(itm);
                });
                if (field.up('editor')) {
                    gridView = field.up('editor').grid.getView();
                    gridRowRec = gridView.getRecord(field.el);
                    gridRowRec.set(data);
                } else {
                    field.up('form').getForm().setValues(data);
                }
            });
        }
        cmp.setTriggers(triggerCfg);
    },

    getSearchTrigger: function (plugin) {
        return {
            cls: 'x-form-search-trigger',
            weight: 2,
            plugin: plugin,
            handler: function (field) {
                var trigger = field.getTrigger('search');
                var fn = field.up('editor') ? trigger.gridEditorCallback : trigger.formFieldCallback;
                fn.call(trigger, field);
            },

            formFieldCallback: function (field) {
                var form = field.up('form'),
                    plugin = this.plugin || field.findPlugin('autocomplete'),
                    pop = plugin.pop;

                this.createPopup({
                    itemdblclick: function (grid, popRec) {
                        var data = plugin.overwriteData(pop.fields, popRec);

                        form.getForm().setValues(data);
                        field.fireEvent('select', field, popRec.copy());
                        grid.up('window').close();
                    }
                });
            },

            gridEditorCallback: function (field) {
                var view = field.up().grid.getView(),
                    rec = view.getRecord(field.el),
                    plugin = this.plugin || field.findPlugin('autocomplete'),
                    pop = plugin.pop;

                this.createPopup({
                    itemdblclick: function (grid, popRec) {
                        var data = plugin.overwriteData(pop.fields, popRec);

                        rec.set(data);
                        field.fireEvent('select', field, popRec.copy());
                        grid.up('window').close();
                    }
                });
            },

            createPopup: function (listeners) {
                var field = this.field,
                    plugin = this.plugin || field.findPlugin('autocomplete'),
                    pop = plugin.pop,
                    columns = Ext.clone(pop.columns),
                    renderer = function (v) {
                        var _v = (v || '').toString();
                        var val = this.down('textfield').getValue();
                        if (!val) {
                            return v;
                        }
                        return _v.replace(val, Ext.String.format('<span style="color:red;">{0}</span>', val));
                    },
                    fn = function (col) {
                        if (col.columns) {
                            Ext.each(col.columns, function (_col) {
                                fn(_col);
                            });
                        } else {
                            if (col.renderer) {
                                Ext.log.warn(
                                    Ext.String.format('column에 renderer 쓰지 마세요 {0}', col.dataIndex)
                                );
                            }
                            col.renderer = renderer;
                        }
                    };

                Ext.each(columns, fn);
                Ext.defer(function () {
                    var win = Ext.create('Ext.window.Window', {
                        title: pop.winTitle,
                        width: pop.width || 600,
                        height: pop.height || 400,
                        layout: 'fit',
                        autoShow: true,
                        modal: true,
                        items: [
                            {
                                xtype: 'bravo_grid_base',
                                storeProps: pop.storeProps,
                                columns: columns,
                                dockedItems: [
                                    {
                                        xtype: 'toolbar',
                                        dock: 'top',
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                listeners: {
                                                    keyup: {
                                                        buffer: 200,
                                                        fn: function () {
                                                            var val = this.getValue(),
                                                                grid = this.up('grid'),
                                                                store = grid.getStore(),
                                                                dataIndexes = Ext.pluck(grid.config.columns, 'dataIndex'),
                                                                fn = function (v1, v2) {
                                                                    var _v1 = v1.toString();
                                                                    return _v1.indexOf(v2) > -1;
                                                                }, ret;

                                                            store.clearFilter();
                                                            if (!val) {
                                                                return;
                                                            }
                                                            store.filterBy(function (rec) {
                                                                ret = false;
                                                                Ext.each(dataIndexes, function (dataIndex) {
                                                                    if (dataIndex && fn(rec.get(dataIndex), val)) {
                                                                        ret = true;
                                                                        return false;
                                                                    }
                                                                });
                                                                return ret;
                                                            });
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ],
                                listeners: listeners
                            }
                        ]
                    });
                    Ext.WindowMgr.bringToFront(win);
                }, 200);
            }
        };
    },

    overwriteData: function (fields, popRec) {
        var data = {},
            columns = this.getPop()['columns'];

        if (!fields.length) {
            Ext.each(columns, function (col) {
                if (col['dataIndex']) {
                    fields.push(col['dataIndex']);
                }
            });
        }

        Ext.Array.each(fields, function (itm) {
            if (Ext.isString(itm)) {
                data[itm] = popRec.get(itm);
            } else if (Ext.isObject(itm)) {
                Ext.Object.each(itm, function (k, v) {
                    data[v] = popRec.get(k);
                });
            }
        });
        return data;
    }
});