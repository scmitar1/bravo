/**
 * 일부 페이지들 끼리 공통으로 사용하는 인쇄 함수들만 모음
 */
Ext.define('BRAVO.Prints', {
    extend : 'Ext.Base',
    statics: {

        printPayslipHTML: function (html) {
            if(Magic.printPopup) {
                Magic.printPopup.close();
            }
            Ext.callback(function () {
                //var myWindow = window.open('', '1', 'width=920,height=1140');
                var myWindow = window.open('', '1', 'width=1200,height=1000');
                //myWindow.location.reload();
                var task     = new Ext.util.DelayedTask(function () {
                    myWindow.location.reload();
                    myWindow.document.write('<html><head>');
                    myWindow.document.write('<title>' + '급여명세서' + '</title>');

                    myWindow.document.write('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
                    //myWindow.document.write('<style>');
                    //myWindow.document.write('.x-form-field.x-form-text.x-form-text-default {font-size:14px;color:black;border-width:0;border-color:black;width:169px;}');
                    //myWindow.document.write('.x-form-text-field-body-default {font-size:14px;color:black;border-width:1;border-color:black;width:400;height:37px;text-align:left;}');
                    //myWindow.document.write('.x-component.x-component-default {height:28px}');
                    //myWindow.document.write('.x-form-item-no-label {color:black;height:30px}');
                    //myWindow.document.write('</style>');
                    
                    myWindow.document.write('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
                    
                    // local에서 테스트 할때는 ../build/development/Magic/resources/Magic-all.css 이따위 식으로 써서 테스트
                    // myWindow.document.write('<link rel="stylesheet" href="../build/development/Magic/resources/Magic-all.css">');
                    // myWindow.document.write('<link rel="stylesheet" href="../build/development/Magic/resources/Magic-addon.css">');
                    myWindow.document.write('<link rel="stylesheet" href="../Magic/resources/Magic-all.css">');
                    myWindow.document.write('<link rel="stylesheet" href="../Magic/resources/Magic-addon.css">');
                    
                    myWindow.document.write('</head>');
                    myWindow.document.write('<body>');
                    myWindow.document.write(html);
                    myWindow.document.write('</body></html>');
                    //myWindow.print();
                    
                    setTimeout(function() {
                        myWindow.print();
                    }, 200)

                    myWindow.focus();
                    Magic.printPopup = myWindow;
                });
                task.delay(1000);
            }, Magic.browserArea);

        }


    }
});
