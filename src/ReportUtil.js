/**
 * Created by anjin05 on 2017-09-27.
 */
Ext.define('BRAVO.ReportUtil', {
    alternateClassName: 'ReportUtil',
    requires: [
        'BRAVO.Utils'
    ],
    singleton: true,

    // param.type: report template 종류 (widget.framepopup 예하 항목)
    popupReport: function (type, option) {
        var cls = Ext.ClassManager.getByAlias('report.' + type);
        if (cls) {
            Utils.popupReport(Ext.apply({}, option, cls.getConfig()));
        }
    },

    popupMultiReport: function (cfg) {
        var cfgList = cfg.list, itmList = [], cls, config;

        Ext.each(cfgList, function (itm) {
            cls = Ext.ClassManager.getByAlias('report.' + itm.type);
            config = itm.config;

            if (cls) {
                itmList.push({
                    title: config.title || cls.getConfig().title,
                    templateUrl: config.templateUrl || cls.getConfig().templateUrl,
                    figureOut: config.figureOut || cls.getConfig().figureOut,
                    data: config && config.data
                });
            }
        });

        if (Ext.isArray(cfgList)) {
            Utils.popupMultiReport({
                title: cfg.title,
                width: cfg['width'],
                height: cfg['height'],
                list: itmList
            });
        }
    },

    $appendInput: function (node, btn, cfg) {
        var input,
            width = node.offsetWidth - 6,
            height = node.offsetHeight - 8,
            fontSize = Math.floor(height * .8),
            value = '' + node.innerHTML,
            numberSel = cfg && cfg.numberSelector;

        if (numberSel && node.matches(numberSel)) {
            value = value.replace(/\,/g, '');

            if (Ext.isNumeric(value)) {
                value = Number(value);
            } else {
                value = null;
            }
        }
        Ext.DomHelper.applyStyles(node, {
            position: 'relative'
        });
        //@formatter:off
        input = Ext.String.format(
            [
                '<input value="{0}" ',
                    'type={1} ',
                    'style="width: {2};',
                    'height: {3};',
                    'background-color: #fdfdc0;',
                    'font-size: {4};',
                    'text-align: {5};',
                    'position: absolute;',
                    'left: 0px;',
                    'top: 0px;" >',
                '</input>'
            ].join(''),
            value,
            node.matches(numberSel) ? 'number' : 'text',
            width + 'px',
            height + 'px',
            Ext.Array.min([fontSize, 11]) + 'px',
            node.matches(numberSel) ? 'right' : 'left'
        );
        //@formatter:on
        Ext.DomHelper.append(node, input);
        input = node.querySelector('input');
        input.onfocus = function () {
            Ext.DomHelper.applyStyles(this, {
                'z-index': 1
            });
        };
        input.onblur = function () {
            Ext.DomHelper.applyStyles(this, {
                'z-index': 0
            });
        };
        // ctrl + enter => complete
        input.addEventListener('keydown', function (e) {
            if (e.ctrlKey && e.keyCode === 13) {
                btn.toggle();
            }
        });
    },

    $editComplete: function (doc, win, cfg) {
        var inputValue, parent,
            numberSel = cfg && cfg.numberSelector;

        Ext.each(doc.querySelectorAll('input'), function (input) {
            inputValue = input.value;
            parent = Ext.get(input.parentNode);

            if (numberSel && parent.is(numberSel)) {
                inputValue = inputValue.replace(/\,/g, '');
                inputValue = Ext.util.Format.number(inputValue, '#,###');
            }
            parent.setHtml(inputValue);
        });
    },

    $parseDataFn: function (win, doc, selectorCfg) {
        var ret = {}, k, v,
            vm = this.getViewModel(),
            numberSel = selectorCfg && selectorCfg.numberSelector,
            list = doc.querySelectorAll('[data-ref]'),
            retFn = function (k, v, ret) {
                var kList = k.split('.'),
                    fn = function (subRet, idx) {
                        if (!subRet[kList[idx]]) {
                            if (idx !== (kList.length - 1)) {
                                subRet[kList[idx]] = {}
                            }
                        }
                        if (idx !== (kList.length - 1)) {
                            fn(subRet[kList[idx]], idx + 1);
                        } else {
                            return subRet[kList[idx]] = v;
                        }
                    };
                fn(ret, 0);
            };

        Ext.each(list, function (dom) {
            k = dom.getAttribute('data-ref');
            v = dom.textContent;

            if (dom.matches(numberSel)) {
                v = v && v.replace(/\,/g, '');
                v = Ext.isNumeric(v) ? Number(v) : null;
            }

            if (vm) {
                vm.set('data.' + k, v);
            } else {
                retFn(k, v, ret);
            }
        });
        if (vm && vm.get('data')) {
            return vm.get('data');
        } else {
            return ret;
        }
    },

    $buildIFrameCfg: function (templateUrl, figureOut, data, scale) {
        var me = this;
        return {
            xtype: 'uxiframe',
            viewModel: true,
            listeners: {
                afterrender: function () {
                    var me = this,
                        pop = me.up('window');

                    if (Ext.isFunction(templateUrl)) {
                        templateUrl = templateUrl.call(me, data);
                    }
                    Ext.Ajax.request({
                        url: templateUrl,
                        success: function (ret) {
                            var html = ret && ret.responseText,
                                w = me.getWin(),
                                css;

                            if (html) {
                                me.getWin()['callee'] = me;
                                me.getDoc().write(figureOut.call(me, html, data, w));
                                css = w.document.styleSheets && w.document.styleSheets[0];

                                if (css) {
                                    css.insertRule([
                                        'input[type=number]::-webkit-inner-spin-button,',
                                        'input[type=number]::-webkit-outer-spin-button {',
                                        '   -webkit-appearance: none;',
                                        '   margin: 0;',
                                        '}'
                                    ].join(''), 0);
                                }

                                if (Ext.isNumeric(scale)) {
                                    me.iframeEl.applyStyles({
                                        '-ms-transform': Ext.String.format('scale({0})', scale),
                                        '-moz-transform': Ext.String.format('scale({0})', scale),
                                        '-o-transform': Ext.String.format('scale({0})', scale),
                                        '-webkit-transform': Ext.String.format('scale({0})', scale),
                                        'transform': Ext.String.format('scale({0})', scale),
                                        '-ms-transform-origin': 'top',
                                        '-moz-transform-origin': 'top',
                                        '-o-transform-origin': 'top',
                                        '-webkit-transform-origin': 'top',
                                        'transform-origin': 'top'
                                    });
                                }
                            }
                        }
                    });
                }
            }
        }
    }
});