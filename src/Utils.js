/**
 * 자주 사용되는 기능의 Javascript 공통함수 모음
 */
Ext.define('BRAVO.Utils', {
    extend: 'Ext.Base',
    alternateClassName: 'Utils',
    statics: {

        /**
         * @private
         * Form Validation
         * @param {Ext.grid.Panel} 그리드
         * @param {String} DataIndex
         * @return {Integer} 데이터존재시 index 리턴 없으면 -1 리턴
         *
         */
        findGridColumnIndex: function (grid, dataIndex) {
            var idx = -1;
            for (var i = 0; i < grid.columns.length; i++) {
                if (grid.columns[i].dataIndex == dataIndex) {

                    if (grid.columns[i].hidden) {
                        return -1;
                    }
                    idx = i;
                    break;
                }
            }
            return i;
        },

        /**
         * @public
         * Grid상의 모든 editor를 가져온다.
         * @param {Ext.grid.Panel} 대상 그리드
         * @return{Array} Ext.form.field.Base를 상속받는 객체들
         */
        getGridEditor: function (grid) {
            var columnArray = grid.columns;
            var returnArray = new Array();
            var returnObj = new Object();
            var dataObj = new Object();

            for (var i = 0; i < columnArray.length; i++) {

                var fieldName = columnArray[i].dataIndex;
                if (Ext.isEmpty(fieldName)) continue;

                var editor = columnArray[i].hidden ? null : columnArray[i].getEditor();
                if (!Ext.isEmpty(editor)) {
                    returnArray.push(editor);
                }
            }
            return returnArray;
        },

        /**
         * @public
         * rowEdting인경우 rendering은 editor의 valueField가 아닌 displayField로 보여주고 싶을때 사용
         * @param {Ext.grid.Panel} 초기화시킬 grid추가
         * @param {String} 데이터인덱스
         * @param {String} value
         */
        getGridComboRenderer: function (grid, dataIndex, value) {
            /**
             grid와 dataIndex를 넘겨 몇번째 column에 존재하는지 확인
             */
            var i = this.findGridColumnIndex(grid, dataIndex);

            /**
             찾지못하면 value 리턴
             */
            if (i == -1) {
                return value;
            }

            var editor = null;
            /**
             * Plugin이 존재하면 getEdtior를 통해 가져와야 정상적인 editor를 가져올수 있다.
             */

            var column = grid.getColumnManager().getColumns()[i];

            if (Ext.isFunction(column.getEditor)) {
                editor = column.getEditor();
            } else {
                editor = column.editor;
            }

            /**
             * 해당 컬럼의 editor를 찾지못하면 그냥 값 리턴
             */
            if (Ext.isEmpty(editor)) {
                return value;
            }

            /**
             * editor즉 Combobox의 store에서 editor의 valueField를 통해 값을 찾기
             */
            var idx = editor.store.find(editor.valueField, value);

            /**
             * 값을 찾지못하면 그냥 리턴
             */
            if (idx == -1) {
                return value;
            }

            /**
             * editor의 displayField를 리턴
             */
            return editor.store.getAt(idx).get(editor.displayField);
        },

        /**
         * @private
         * @param {Array} Ext.form.field.Base
         * @return {Object} Prototype으로 리턴합니다. obj.errorField, obj.errorMessage
         *
         *  사용법은 다음과 같습니다.
         *  아래코드 사용시 column내부의 editor들의 Validation을 가져오고 오류 발생시 가장 첫번째 editor로 focus됩니다.
         *
         *  rowEditPlugin.addListener('validateedit', function(editor, context, eOpts) {
         *
         *      if( ! isValidGridEditor(this)) {
         *          return false;
         *      }
         *  });
         */
        getFieldsErrorMessage: function (arrField) {
            var returnObj = {};
            returnObj.errorMessage = null;
            returnObj.errorField = null;

            var arrErrorMessage = [];

            for (var i = 0; i < arrField.length; i++) {
                var errObj = null;
                var field = arrField[i];
                if (Ext.isEmpty(field.getValue())) {
                    /**
                     * Ext.form.field.Base를 상속받는 모든객체는 getErrors()라는것이 존재합니다.
                     */
                    errObj = field.getErrors();
                } else {
                    errObj = field.getErrors(field.getValue());
                }
                //errObj의 목적은 에러메시지 띄운이후 해당 Field로 focus가 자동으로 이루어지게 하기위해서 만들었습니다.
                if (!Ext.isEmpty(errObj)) {
                    var errorPrefix = '';//Ext.isEmpty(field.oriFieldLabel) ?  '': field.oriFieldLabel +' : ';
                    arrErrorMessage.push(errorPrefix + errObj[0]);
                    if (arrErrorMessage.length == 1) {
                        returnObj.errorField = field
                    }
                }
            }

            // 에러필드가 존재하지 않으면 null 리턴을합니다.
            if (Ext.isEmpty(returnObj.errorField)) {
                return null;
            }

            returnObj.errorMessage = arrErrorMessage.join('<br>').toString();
            return returnObj;
        },

        /**
         * @public
         * RowEditingPlugin 사용시 그리드의 데이터 Validation
         * @param {Ext.grid.Panel}
         * @return {boolean} 오류 발생시 return false
         *
         *  사용법은 다음과 같습니다.
         *  아래코드 사용시 column내부의 editor들의 Validation을 가져오고 오류 발생시 가장 첫번째 editor로 focus됩니다.
         *
         *  rowEditPlugin.addListener('validateedit', function(editor, context, eOpts) {
         *
         *      if(!util.isValidGridEditor(this)) {
         *          return false;
         *      }
         *  });
         */
        isValidGrid: function (grid) {
            var arrEditor = Broavo.Utils.getGridEditor(grid);
            var returnObj = this.getFieldsErrorMessage(arrEditor);

            if (Ext.isEmpty(returnObj)) {
                return true;
            }
            Ext.Msg.alert('오류', returnObj.errorMessage, function () {
                returnObj.errorField.focus();
            });

            return false;
        },

        /**
         * @public
         * Form Validation
         * @param {Ext.form.Panel}
         * @return {boolean} 오류 발생시 false 없으면 true
         *
         */
        isValidForm: function (formPanel) {
            var basicForm = formPanel.getForm();
            if (basicForm.isValid()) {
                return true;
            }
            Ext.toast('입력값을 확인해 주세요.');
            return false;
            /*
             var arrField = basicForm.getFields().items;
             var returnObj = this.getFieldsErrorMessage(arrField);
             if(Ext.isEmpty(returnObj)) {
             return true;
             }

             Ext.Msg.alert('오류',returnObj.errorMessage,function() {
             returnObj.errorField.focus();
             });
             return false;
             */
        },

        isValidRecord: function (rec) {
            var errorRec = rec.getValidation(true);
            if (!errorRec.isValid()) {
                Ext.Object.each(errorRec.getData(), function (field, msg) {
                    if (Ext.isString(msg)) {
                        BRAVO.Msg.alert('Error', msg);
                        return false;
                    }
                });
                return false;
            }
            return true;
        },


        /**
         * 사업자등록번호 유효성 체크
         * @param vencod
         * @returns {boolean}
         */
        checkBizVATId: function (vencod) {
            var sum = 0;
            var getlist =new Array(10);
            var chkvalue =new Array("1","3","7","1","3","7","1","3","5");

            for(var i=0; i<10; i++) {
                getlist[i] = vencod.substring(i, i+1);
            }
            for(var j=0; j<9; j++) {
                sum += getlist[j]*chkvalue[j];
            }

            sum = sum + parseInt((getlist[8]*5)/10);
            var sidliy = sum % 10;
            var sidchk = 0;
            if(sidliy != 0) {
                sidchk = 10 - sidliy;
            } else {
                sidchk = 0;
            }

            if(sidchk != getlist[9]) { return false; }

            return true;
        },


        checkJuminNo : function (juminno) {
            juminno = juminno.replace("-", "");

            if(juminno=="" || juminno==null || juminno.length!=13) {
                alert("주민등록번호 13자리를 적어주세요.");
                return false;
            }

            var jumin1 = juminno.substr(0,6);
            var jumin2 = juminno.substr(6,7);
            var yy = jumin1.substr(0,2); // 년도
            var mm = jumin1.substr(2,2); // 월
            var dd = jumin1.substr(4,2); // 일
            var genda = jumin2.substr(0,1); // 성별
            var msg, ss, cc;

            // 숫자가 아닌 것을 입력한 경우
            if (!this.isNumeric(jumin1)) {
                alert("주민등록번호 앞자리를 숫자로 입력하세요.");
                return false;
            }

            // 길이가 6이 아닌 경우
            if (jumin1.length != 6) {
                alert("주민등록번호 앞자리를 다시 입력하세요.");
                return false;
            }

            // 첫번째 자료에서 연월일(YYMMDD) 형식 중 기본 구성 검사
            if (yy < "00" || yy > "99" || mm < "01" || mm > "12" || dd < "01" || dd > "31") {
                alert("주민등록번호 앞자리를 다시 입력하세요.");
                return false;
            }

            // 숫자가 아닌 것을 입력한 경우
            if (!this.isNumeric(jumin2)) {
                alert("주민등록번호 뒷자리를 숫자로 입력하세요.");
                return false;
            }

            // 길이가 7이 아닌 경우
            if (jumin2.length != 7) {
                alert("주민등록번호 뒷자리를 다시 입력하세요.");
                return false;
            }

            // 성별부분이 1 ~ 4 가 아닌 경우
            if (genda < "1" || genda > "4") {
                alert("주민등록번호 뒷자리를 다시 입력하세요.");
                return false;
            }

            // 연도 계산 - 1 또는 2: 1900년대, 3 또는 4: 2000년대
            cc = (genda == "1" || genda == "2") ? "19" : "20";

            // 첫번째 자료에서 연월일(YYMMDD) 형식 중 날짜 형식 검사
            if (this.isYYYYMMDD(parseInt(cc+yy), parseInt(mm), parseInt(dd)) == false) {
                alert("주민등록번호 앞자리를 다시 입력하세요.");
                return false;
            }

            // Check Digit 검사
            if (!this.isSSN(jumin1, jumin2)) {
                return false;
            }
            return true;
        },


        /**
         * 올바른 날짜형태의 문자열인지 체크
         * @param y
         * @param m
         * @param d
         * @returns {boolean}
         */
        isYYYYMMDD : function (y, m, d) {
            switch (m) {
                case 2: // 2월의 경우
                    if (d > 29) return false;

                    if (d == 29) {
                        // 2월 29의 경우 당해가 윤년인지를 확인
                        if ((y % 4 != 0) || (y % 100 == 0) && (y % 400 != 0))
                            return false;
                    }
                    break;
                case 4: // 작은 달의 경우
                    if (d == 31) return false;
                case 6:
                    if (d == 31) return false;
                case 9:
                    if (d == 31) return false;
                case 11:
                    if (d == 31) return false;
            }
            // 큰 달의 경우
            return true;
        },

        /**
         * 숫자형 문자열인지 체크
         * @param s
         * @returns {boolean} 넘어온 문자열 중에 하나라도 숫자가 아닌것이 있으면 false를  return
         */
        isNumeric : function (s) {
            for (i=0; i<s.length; i++) {
                c = s.substr(i, 1);
                if (c < "0" || c > "9") return false;
            }
            return true;
        },


        /**
         * 윤년인지 체크
         * @param y
         * @returns {boolean} 윤년일 경우 true return
         */
        isLeapYear : function (y) {
            if (y < 100)
                y = y + 1900;
            if ( (y % 4 == 0) && (y % 100 != 0) || (y % 400 == 0) ) {
                return true;
            } else {
                return false;
            }
        },

        /**
         * 년도의 월의 해당 일수 구하기
         * @param yy
         * @param mm
         * @returns {*|number}
         */
        getNumberOfDate : function (yy, mm) {
            month = new Array(29,31,28,31,30,31,30,31,31,30,31,30,31);
            if (mm == 2 && this.isLeapYear(yy)) mm = 0;
            return month[mm];
        },


        /**
         * 주민번호 check digit
         * @param s1
         * @param s2
         * @returns {boolean}
         */
        isSSN : function (s1, s2) {
            n = 2;
            sum = 0;

            for (i=0; i<s1.length; i++)
                sum += parseInt(s1.substr(i, 1)) * n++;

            for (i=0; i<s2.length-1; i++) {
                sum += parseInt(s2.substr(i, 1)) * n++;
                if (n == 10) n = 2;
            }

            c = 11 - sum % 11;
            if (c == 11) c = 1;
            if (c == 10) c = 0;

            if (c != parseInt(s2.substr(6, 1)))
                return false;
            else
                return true;
        },




        ///////////////////////////
        codeRenderer: function (store, valuefield, viewfield) {
            return function (v) {
                var sv = v + "";
                var its = store.data.items;
                for (var i = 0; i < its.length; i++) {
                    if (( its[i].data[valuefield] + "") === sv) {
                        return its[i].data[viewfield];
                    }
                }
                return sv;
            }
        },

        getToday: function (separator) {
            var sprtr = '';
            if (separator) {
                sprtr = separator;
            }
            var today = new Date(),
                NowDay = today.getDate(),
                NowYear,
                NowMonth = today.getMonth() + 1;
            if (NowMonth < 10) {
                NowMonth = "0" + String(NowMonth);
            }
            if (NowDay < 10) {
                NowDay = "0" + String(NowDay);
            }
            NowYear = today.getFullYear();
            return String(NowYear) + sprtr + String(NowMonth) + sprtr + String(NowDay);
        },

        downloadFile: function (fileId) {
            Ext.core.DomHelper.append(document.body, {
                tag: 'iframe', frameBorder: 0, width: 0, height: 0,
                css: 'display:none;visibility:hidden;height:0px;',
                src: "/file/download/" + fileId
            });
        },

        fileUploadWindow: function (callbackFc) {
            var formTaget = Ext.DomHelper.append(document.body, {
                tag: 'ifame',
                name: 'fileframe',
                style: 'width:0;height:0;'
            });
            document.body.appendChild(formTaget);

            var uploadFile = function (fd, path) {
                var form = fd.up('form').getForm();
                if (form.isValid()) {
                    form.submit({
                        url: "/file/upload",
                        waitMsg: 'Uploading...',
                        success: function (fp, o) {
                            fd.up('window').close();
                            callbackFc(o.result.id, o.result.name);
                        },
                        failure: BRAVO.Utils.showFormSubmitError
                    });
                }
            };

            var win = Ext.create('Ext.window.Window', {
                title: 'File Uploading', modal: true, layout: 'fit',
                buttons: {
                    xtype: 'bravo_button_close', handler: function () {
                        win.close();
                    }
                },
                listeners: {
                    show: function (thisWin, evt) {
                        thisWin.down('filefield').fileInputEl.dom.click();
                    }
                },
                items: [{
                    xtype: 'form', frame: false, layout: {type: 'vbox', align: 'stretch'},
                    defaults: {
                        anchor: '100%',
                        allowBlank: false,
                        msgTarget: 'side',
                        labelWidth: 20,
                        labelSeparator: ' '
                    },
                    items: [{
                        xtype: 'fieldcontainer',
                        padding: 10,
                        margin: 10,
                        layout: {type: 'vbox', align: 'stretch'},
                        width: 180,
                        items: {
                            xtype: 'filefield',
                            flex: 1,
                            align: 'right',
                            textAlign: 'right',
                            name: 'UP_FILE',
                            buttonText: 'Select',
                            listeners: {change: uploadFile}
                        }
                    }]
                }]
            }).show();
        },

        imageUploadWindow: function (callbackFc) {
            var formTaget = Ext.DomHelper.append(document.body, {
                tag: 'ifame',
                name: 'fileframe',
                style: 'width:0;height:0;'
            });
            document.body.appendChild(formTaget);

            var uploadFile = function (fd, path) {
                var form = fd.up('form').getForm();
                if (form.isValid()) {
                    form.submit({
                        url: "/file/upload",
                        waitMsg: 'Uploading...',
                        success: function (fp, o) {
                            fd.up('window').close();
                            callbackFc(o.result.id, o.result.name);
                        },
                        failure: BRAVO.Utils.showFormSubmitError
                    });
                }
            };

            var win = Ext.create('Ext.window.Window', {
                title: 'File Uploading', modal: true, layout: 'fit',
                buttons: {
                    xtype: 'bravo_button_close', handler: function () {
                        win.close();
                    }
                },
                listeners: {
                    show: function (thisWin, evt) {
                        thisWin.down('filefield').fileInputEl.dom.click();
                    }
                },
                items: [{
                    xtype: 'form', frame: false, layout: {type: 'vbox', align: 'stretch'},
                    defaults: {
                        anchor: '100%',
                        allowBlank: false,
                        msgTarget: 'side',
                        labelWidth: 20,
                        labelSeparator: ' '
                    },
                    items: [{
                        xtype: 'fieldcontainer',
                        padding: 10,
                        margin: 10,
                        layout: {type: 'vbox', align: 'stretch'},
                        width: 180,
                        items: {
                            xtype: 'filefield',
                            flex: 1,
                            align: 'right',
                            textAlign: 'right',
                            name: 'UP_FILE',
                            buttonText: 'Select',
                            accept: "image/*",
                            listeners: {change: uploadFile}
                        }
                    }]
                }]
            }).show();
        },

        showFormSubmitInfo: function (form, action) {
            var title = 'Info';
            var msg = '';
            try {
                var ro = Ext.decode(action.response.responseText);
                title = ro.title || title;
                msg = ro.msg;
            } catch (e) {
                console.log(e);
            }

            Ext.MessageBox.show({
                title: title,
                msg: msg,
                icon: Ext.Msg.INFO,
                buttons: Ext.Msg.OK
            });
        },

        showFormSubmitError: function (form, action) {
            var title = 'Error';
            var msg = '에러입니다.';
            try {
                var ro = Ext.decode(action.response.responseText);
                title = ro.title;
                msg = ro.msg || ro.message;
            } catch (e) {
                console.log(e);
            }

            Ext.MessageBox.show({
                title: title,
                msg: msg,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        },


        /**
         * 해당패널의 내용만 인쇄한다.
         * @param {Ext.Component} 인쇄하고자 하는 Component 또는 Panel
         *
         */
        printComponent: function (pnl) {
            var targetElement = Ext.getCmp(pnl.id);

            // window창을 띄움
            var myWindow = window.open('', '', 'width=1200,height=1000');

            if (Ext.isEmpty(myWindow)) {
                // 창을 못띄우거나 브라우저에서 강제로 막아둔경우 이걸 강제로 해제시킨다.
                document.write("<object id='DHTMLEdit' classid='clsid:2D360201-FFF5-11d1-8D03-00A0C959BC0A' width='1' height='1' align='middle'><PARAM NAME='ActivateApplets' VALUE='1'><PARAM NAME='ActivateActiveXControls' VALUE='1'></object>");

                setTimeout(function () {
                    myWindow.location.reload();
                    myWindow.document.write('<html><head>' +
                        '<link rel="stylesheet" href="../build/development/Magic/resources/Magic-all.css">' +
                        '<link rel="stylesheet" href="../build/development/Magic/resources/Magic-addon.css">' +
                        '</head><body>');

                    myWindow.document.write(targetElement.el.dom.innerHTML);
                    myWindow.document.write('</body></html>');

                    myWindow.focus();
                    //myWindow.print();
                    //myWindow.close();
                }, 500);
            } else {
                myWindow.location.reload();
                window.document.getElementById(pnl.id + '-body').style.height = '100%';
                //window.document.getElementById(pnl.id+'-body').style.zoom='60%';

                var html = '<html><head>' +
                    '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
                    '<link rel="stylesheet" href="' + location.origin + '/sencha/build/development/Magic/resources/Magic-all.css">' +
                    '<link rel="stylesheet" href="' + location.origin + '/sencha/build/development/Magic/resources/Magic-addon.css">' +
                    '</head><body>' +
                    targetElement.el.dom.innerHTML +
                    '</body></html>';

                myWindow.document.write(html);

                var baseUrl = '/report/createPDF';

                var submitForm = myWindow.document.createElement("form"); // form 엘리멘트 생성
                submitForm.setAttribute("method", "post");
                submitForm.setAttribute("action", baseUrl);
                submitForm.setAttribute("accept-charset", "UTF-8");
                myWindow.document.body.appendChild(submitForm);

                var inputField = myWindow.document.createElement("input");
                inputField.setAttribute("type", "hidden");
                inputField.setAttribute("name", "htmlContent");
                inputField.setAttribute("value", html);
                submitForm.appendChild(inputField);

                submitForm.submit();

                myWindow.focus();
                //myWindow.print();
                //myWindow.close();
            }
        },

        showPopup: function (widgetName, options) {
            var popup = Ext.ComponentQuery.query(widgetName)[0];

            if (!popup) {
                popup = Ext.widget(widgetName);
            }
            popup.show();
            popup.setupPopup(options);
        },

        showResult: function (widgetName, options) {
            var ct = Ext.ComponentQuery.query('global_center tabpanel')[0].getActiveTab();
            var layout = ct.getLayout();

            var popup = Ext.widget(widgetName);
            popup.setupPopup(options);
            ct.add(popup);

            var first = ct.getComponent(0);
            var second = ct.getComponent(1);
            second.getEl().slideIn('r');
            layout.setActiveItem(1);

//            popup.close = function() {
//                layout.setActiveItem( 0);
//            };

            popup.on('beforeclose', function (comp) {
                layout.setActiveItem(0);
                ct.remove(popup);
                popup.destroy();
            });
        },

        printHTML: function (html) {
            if (Magic.printPopup) {
                Magic.printPopup.close();
            }

            Ext.callback(function () {
                var myWindow = window.open('', '1', 'width=920,height=1140');
                myWindow.location.reload();

                var task = new Ext.util.DelayedTask(function () {
                    myWindow.location.reload();
                    myWindow.document.write('<html><head>');
                    myWindow.document.write('<title>' + 'Title' + '</title>');
                    myWindow.document.write('</head><body>');
                    myWindow.document.write(html);
                    myWindow.document.write('</body></html>');
                    myWindow.focus();
                    Magic.printPopup = myWindow;
                    /*if (Ext.isIE === true) {
                     var OLECMDID   = 7;
                     var PROMPT     = 1;
                     var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
                     document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
                     WebBrowser.ExecWB(OLECMDID, PROMPT);
                     } else {
                     //myWindow.print();
                     }*/
                });

                task.delay(1000);
            }, Magic.browserArea);
        },


        createFieldItem: function (tdAttrs, key, prop, inputProp) {
            var result = [];
            var headers = prop.headers || [];

            for (var i = 0; i < headers.length; i++) {
                var header = headers[i];
                var attr = Ext.clone(tdAttrs);

                Ext.merge(attr.style, header);

                delete attr.style.rowspan;
                delete attr.style.colspan;
                delete attr.style.value;

                if (Ext.isNumber(attr.style.height)) {
                    attr.style.height += 'px';
                }
                if (Ext.isNumber(attr.style.width)) {
                    attr.style.width += 'px';
                }

                var item = {};
                if (header['xtype']) {
                    item = header;
                    var cellCls = '';
                    if (item.readOnly === true) {
                        cellCls = item.readOnlyInputCls || 'field-readonly';
                    }
                    item.tdAttrs = attr;
                    item.width = item.width || '100%';
                    item.height = item.height || '';
                    item.cellCls = cellCls;
                    item.cls = 'hide-input';
                } else {
                    item = {
                        xtype: 'component',
                        margin: 0,
                        data: {
                            PER_MONTH: 'Y',
                            PER_CORRECT: 'Y'
                        },
                        name: header['name'],
                        rowspan: header['rowspan'],
                        colspan: header['colspan'],
                        width: header['width'],
                        tdAttrs: attr,
                        style: header.style,
                        tpl: header['value'] || '&nbsp;',
                        listeners: header['listeners']
                    };
                }
                result.push(item);
            }

            var datas = prop.datas;

            if (Ext.isEmpty(datas)) {
                return result;
            }
            var fields = Ext.clone(inputProp);
            Ext.iterate(fields, function (name, fieldProp) {
                var prefix = (key == 'HEADER') ? '' : key + '_';

                fieldProp.name = prefix + name;
                fieldProp.height = '100%';
                fieldProp.cls = 'hide-input';
                fieldProp.style = 'margin: 0px 0px 0px 0px;';
                fieldProp.dataIndex = name;
                fieldProp.dataPrefix = (key == 'HEADER') ? '' : key;
                //    fieldProp.emptyText = prefix + name;
                Ext.apply(fieldProp, datas[name]);
                var attr = Ext.clone(tdAttrs);
                var tdAttr = {
                    borderWidth: fieldProp.borderWidth
                };

                var width = fieldProp.width;
                if (Ext.isNumber(fieldProp.width)) {
                    tdAttr.width = width + 'px';
                }

                Ext.merge(attr.style, tdAttr);

                var cellCls = '';
                if (fieldProp.readOnly === true) {
                    cellCls = fieldProp.readOnlyCls || 'field-readonly';
                }

                delete attr.style.rowspan;
                delete attr.style.colspan;
                delete attr.style.value;

                if (Ext.isNumber(attr.style.height)) {
                    attr.style.height += 'px';
                }

                var field = {
                    xtype: 'container',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    width: '100%',
                    tdAttrs: attr,
                    cellCls: cellCls,
                    defaults: {
                        flex: 1
                    }, items: [fieldProp]
                };
                result.push(field);
            });

            return result;
        },

        applyReportItems: function (tdAttrs, component, fieldProps, inputProp) {
            var me = this;
            var items = [];
            Ext.iterate(fieldProps, function (key, prop) {
                items = items.concat(me.createFieldItem(tdAttrs, key, prop, inputProp));
            });
            component.add(items);
        },

        /**
         * 오픈된 급여일 가져와서 RETURN , 그 이후 세팅은 각 페이지에서 알아서
         */
        setOpenPayDate: function (objThis) {
            var settingFunc = function (resObj) {
                var form = objThis.view.down('form').getForm();
                if (resObj === null || resObj === undefined || resObj === "") {
                    BRAVO.Msg.alert('확인', '급여일이 오픈되지 않았습니다.급여일 관리 메뉴를 통해 급여일을 오픈해주세요');
                    return false;
                } else {
                    if (resObj.PAY_MONTH === null || resObj.PAY_MONTH === undefined || resObj.PAY_MONTH === "") {
                        BRAVO.Msg.alert('확인', '급여일이 오픈되지 않았습니다.급여일 관리 메뉴를 통해 급여일을 오픈해주세요');
                        return false;
                    }
                }
                form.findField('PAY_MONTH').setValue(resObj.PAY_MONTH);
                form.findField('SEQ').setValue(resObj.SEQ);
            };
            BRAVO.Ajax.request({
                url: '/payroll/code/open-pay-date/list',
                mode: 'POST',
                success: objThis.payDateSetting || settingFunc,
                scope: objThis
            });
        },

        /**
         * 현재 calendar의 월 (month)를 가져와서 pay_month에 세팅
         * @param objThis
         */
        setThisMonth: function (objThis) {
            var dateFrom = Ext.Date.add(new Date(), Ext.Date.MONTH, 0);
            dateFrom.setDate(1);

            var form = objThis.getView().down('form').getForm();
            // form.findField('PAY_MONTH').setValue(dateFrom);
            form.findField('PAY_MONTH').setRawValue(dateFrom);
        },

        setThisAccountMonth: function (objThis) {
            var dateFrom = Ext.Date.add(new Date(), Ext.Date.MONTH, 0);
            dateFrom.setDate(1);
        },

        setFormEmptypeList: function (formData) {
            var empTypeList = [];
            formData.EMPTYPE_CD = Ext.isArray(formData.EMPTYPE_CD) ? formData.EMPTYPE_CD : [];
            for (var i = 0; i < formData.EMPTYPE_CD.length; i++) {
                empTypeList.push({EMPTYPE_CD: formData.EMPTYPE_CD[i]});
            }
            delete formData.EMPTYPE_CD;
            return empTypeList;
        },

        calculateRealAge: function (birth, attYear) {
            var attDate = (function (v) {
                    if (v && Ext.isNumeric(v)) {
                        v = v.toString();
                        if (v.length === 4) {
                            return v + '0101';
                        } else if (v.length === 8) {
                            return v;
                        }
                    }
                    return;
                })(attYear),
                birthDate = Ext.Date.parse(birth, 'Ymd'),
                attYearDate = Ext.Date.parse(attDate, 'Ymd'),
                ret;
            if (Ext.isDate(attYearDate) && Ext.isDate(birthDate)) {
                ret = Ext.Date.diff(birthDate, attYearDate, Ext.Date.YEAR);
            }
            return ret;
        },

        treeStoreTransform: function (idProperty, parentIdProperty, rootProperty, leafProperty, cascadeFn) {
            var cfg;
            if (Ext.isObject(idProperty)) {
                cfg = idProperty;
                parentIdProperty = cfg.parentIdProperty;
                rootProperty = cfg.rootProperty;
                leafProperty = cfg.leafProperty;
                cascadeFn = cfg.cascadeFn;
                idProperty = cfg.idProperty;
            }

            return function (ret) {
                var dataSource = Ext.Array.clone(!!rootProperty ? ret[rootProperty] : ret),
                    root = Ext.Array.findBy(dataSource, function (itm) {
                        return (itm[idProperty] === 'ROOT' || Ext.isEmpty(itm[idProperty]));
                    }),
                    firstChildren = Ext.Array.filter(dataSource, function (itm) {
                        var ret = false;
                        if (root) {
                            if (root[idProperty] === itm[parentIdProperty]) {
                                ret = true;
                            }
                        } else if (itm[parentIdProperty] === 'ROOT') {
                            ret = true;
                        } else {
                            ret = !itm[parentIdProperty];
                        }
                        return ret;
                    }),
                    appendChild = function (p) {
                        var children = Ext.Array.filter(dataSource, function (itm) {
                            return itm[parentIdProperty] === p[idProperty];
                        });
                        if (children.length) {
                            Ext.apply(p, {
                                children: Ext.Array.clone(children),
                                expanded: true,
                                leaf: false
                            });
                            Ext.Array.remove(dataSource, children);
                            Ext.each(p['children'], function (itm) {
                                appendChild(itm);
                            });
                        } else {
                            p['leaf'] = leafProperty ? getLeaf(p) : true;
                            if (!p['leaf']) {
                                p['children'] = [];
                            }
                        }
                    }, getLeaf = function (itm) {
                        return itm[leafProperty] === 'Y';
                    },
                    cascade = function (root, fn) {
                        Ext.each(root.children, function (itm, idx, parent) {
                            fn(itm, parent);
                            if (Ext.isArray(itm.children) && itm.children.length) {
                                cascade(itm, fn);
                            }
                        });
                    };

                firstChildren = Ext.Array.clone(firstChildren);
                Ext.Array.remove(dataSource, root);
                Ext.Array.each(firstChildren, function (itm) {
                    appendChild(itm);
                });
                if (root) {
                    root = {
                        expanded: true,
                        children: [
                            Ext.apply(root, {
                                expanded: true,
                                children: firstChildren
                            })
                        ]
                    };
                } else {
                    root = {
                        expanded: true,
                        children: firstChildren
                    };
                }
                if (Ext.isFunction(cascadeFn)) {
                    cascade(root, cascadeFn);
                }
                return root;
            }
        },

        /*
         *  html 모달 팝업창 조회시 사용
         *  param.data: binding할 데이터,
         *  param.width: popup width
         *  param.height: popup height
         *  param.scale: iframe scale
         *  param.editable(boolean): 전체 editable
         *  param.editable.selector: css selector editable
         *  param.editable.callback: edit complete 이후 callback
         *  param.templateUrl: popup에 호출 할 templateUrl,
         *  param.title: popup 타이틀
         *  param.figureOut: popup template html 가공 함수
         * */
        popupReport: function (param) {
            var data = param.data,
                templateUrl = param.templateUrl,
                editable = Ext.isDefined(param.editable) ? param.editable : false,
                savable = Ext.isDefined(param.savable) ? param.savable : false,
                title = param.title,
                numberSel = param.numberSelector || '[data-number]',
                figureOut = param.figureOut || function (html) {
                    return html;
                };

            Ext.widget('window', {
                layout: 'fit',
                title: title,
                width: param['width'] || 1000,
                height: param['height'] || 600,
                autoShow: true,
                closeAction: 'destroy',
                maximizable: true,
                modal: true,
                items: [
                    ReportUtil.$buildIFrameCfg(templateUrl, figureOut, data, param.scale)
                ],

                buttons: [
                    '->',
                    {
                        text: 'edit',
                        iconCls: 'x-fa fa-edit',
                        hidden: !editable,
                        enableToggle: true,
                        toggleHandler: function (btn, pressed) {
                            var win = this.up('window'),
                                selector = (function () {
                                    if (Ext.isString(editable)) {
                                        return editable;
                                    } else if (Ext.isObject(editable)) {
                                        return editable['selector'];
                                    } else {
                                        return 'body td,span,li';
                                    }
                                })(),
                                callback = editable && editable['callback'],
                                doc = win.down('uxiframe').getDoc(),
                                firstChild, appendInput, editComplete;

                            appendInput = Ext.isFunction(param.appendInput) ?
                                param.appendInput : ReportUtil.$appendInput;
                            editComplete = Ext.isFunction(param.editComplete) ?
                                param.editComplete : ReportUtil.$editComplete;

                            this.setText(pressed ? 'complete' : 'edit');
                            if (pressed) {
                                Ext.each(doc.querySelectorAll(selector), function (node) {
                                    firstChild = node.firstChild;
                                    if (Ext.isBoolean(editable)) {
                                        if (firstChild && firstChild.nodeName === '#text') {
                                            if (node.querySelector('*:not(br)')) {
                                                return;
                                            }
                                            appendInput(node, btn, {
                                                numberSelector: numberSel
                                            });
                                        }
                                    } else {
                                        appendInput(node, btn, {
                                            numberSelector: numberSel
                                        });
                                    }
                                });
                            } else {
                                editComplete(doc, win.down('uxiframe').getWin(), {
                                    numberSelector: numberSel
                                });

                                callback = Ext.Array.from(callback);

                                Ext.each(callback, function (fn) {
                                    fn(win.down('uxiframe').getWin(), win);
                                });
                            }
                        }
                    },
                    {
                        text: 'save',
                        iconCls: 'x-fa fa-save',
                        hidden: !savable,
                        handler: savable && function (btn) {
                            var iframe = btn.up('window').down('uxiframe'),
                                w = iframe.getWin(), data,
                                doc = w.document,
                                toggleBtn = btn.previousSibling('[enableToggle=true]');

                            if (doc.querySelector('input')) {
                                toggleBtn.toggle();
                            }
                            if (Ext.isFunction(savable.parseData)) {
                                data = savable.parseData.call(iframe, w, doc);
                            } else {
                                data = ReportUtil.$parseDataFn.call(iframe, w, doc, {
                                    numberSelector: numberSel
                                });
                            }
                            if (Ext.isFunction(savable.callback)) {
                                savable.callback.call(iframe, data, w, doc);
                            }
                        }
                    },
                    {
                        text: 'print',
                        iconCls: 'x-fa fa-print ',
                        handler: function (btn) {
                            var win = btn.up("window"),
                                iframe = win.down("uxiframe"),
                                _win = iframe.getWin(),
                                doc = _win.document,
                                toggleBtn = btn.previousSibling('[enableToggle=true]');

                            if (doc.querySelector('input')) {
                                toggleBtn.toggle();
                            }
                            _win.print();
                        }
                    },
                    {
                        text: 'close',
                        iconCls: 'x-fa fa-window-close ',
                        handler: function (btn) {
                            var win = btn.up('window');
                            win.close();
                        }
                    },
                    '->'
                ]
            });
        },


        /*
        * html multi 모달 팝업창 조회시 사용
        *  param.width: popup width
        *  param.height: popup height
        *  param.title: popup 타이틀
        *  param.list: 화면 목록
        *  param.list.title: 탭 타이틀
        *  param.list.data: 탭에 바인딩할 데이터
        *  param.list.templateUrl: 탭에 호출 할 templateUrl,
        *  param.figureOut: popup template html 가공 함수
        * */
        popupMultiReport: function (param) {
            var list = param.list,
                itmList = [];

            if (!Ext.isArray(param.list)) {
                return;
            }
            Ext.each(list, function (itm) {
                itmList.push(Ext.apply({
                    title: itm.title
                }, ReportUtil.$buildIFrameCfg(
                    itm.templateUrl,
                    itm.figureOut,
                    itm.data,
                    itm.scale
                )));
            });

            Ext.widget('window', {
                layout: 'fit',
                title: param['title'] || '상세조회',
                width: param['width '] || 1000,
                height: param['height'] || 600,
                autoShow: true,
                closeAction: 'destroy',
                maximizable: true,
                modal: true,
                items: [
                    {
                        xtype: 'tabpanel',
                        deferredRender: false,
                        items: itmList
                    }
                ],
                buttons: [
                    '->',
                    {
                        text: '전체 출력',
                        iconCls: 'x-fa fa-print ',
                        handler: function (btn) {
                            var win = btn.up('window'),
                                list = win.query('tabpanel uxiframe'), _w;

                            Ext.each(list, function (panel) {
                                _w = panel.getWin();
                                _w.focus();
                                _w.print();
                            });
                        }
                    },
                    {
                        text: '현재탭 출력',
                        iconCls: 'x-fa fa-print ',
                        handler: function (btn) {
                            var win = btn.up('window'),
                                tabpanel = win.down('tabpanel'),
                                w = tabpanel && tabpanel.getActiveTab().getWin();

                            w && w.print();
                        }
                    },
                    {
                        text: 'close',
                        iconCls: 'x-fa fa-window-close ',
                        handler: function (btn) {
                            var win = btn.up('window');
                            win.close();
                        }
                    },
                    '->'
                ]
            });
        },

        /*
         *  급여명세서 html 모달 팝업창 조회시 사용
         *  param.url: 파일다운로드시 사용하는 url,
         *  param.param: submitAction에서 사용하는 param
         *  param.fileName: 다운로드되는 파일 이름
         * */
        fileDownload: function (cfg) {
            var fileName = cfg['fileName'],
                url = cfg['url'],
                param = cfg['params'],
                xhr = new XMLHttpRequest(),
                formData, msg;

            xhr.open('POST', encodeURI(url));
            xhr.responseType = 'arraybuffer';

            msg = Ext.Msg.wait('잠시만 기다리세요..', '파일 다운로드 중');

            xhr.onload = function () {
                msg.hide();
                if (this.status === 200) {
                    var type = xhr.getResponseHeader('Content-Type');

                    var blob = new Blob([this.response], {
                        type: type
                    });
                    if (typeof window.navigator.msSaveBlob !== 'undefined') {
                        window.navigator.msSaveBlob(blob, fileName);
                    } else {
                        var URL = window.URL || window.webkitURL;
                        var downloadUrl = URL.createObjectURL(blob),
                            header = this.getResponseHeader('Content-Disposition'),
                            regEx = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/, ret;

                        // client에서 fileName을 지정하지 않으면 responseHeader에서 추출
                        if (!fileName && header) {
                            ret = regEx.exec(header);
                            fileName = ret && ret[1];
                            fileName = fileName && fileName.replace(/\"/g,'');
                            fileName = fileName && fileName.replace(/\'/g, '');
                        }
                        if (fileName) {
                            // use HTML5 a[download] attribute to specify filename
                            var a = document.createElement("a");
                            // safari doesn't support this yet
                            if (typeof a.download === 'undefined') {
                                window.location = downloadUrl;
                            } else {
                                a.href = downloadUrl;
                                a.download = fileName;
                                document.body.appendChild(a);
                                a.click();
                            }
                        } else {
                            window.location = downloadUrl;
                        }

                        setTimeout(function () {
                            URL.revokeObjectURL(downloadUrl);
                        }, 100); // cleanup
                    }
                } else {
                    Ext.Msg.show({
                        title: 'WARNING',
                        message: '파일 다운로드를 실패하였습니다',
                        icon: Ext.Msg.WARNING,
                        buttons: Ext.Msg.OK
                    });
                }
            };

            if (Ext.isObject(param)) {
                formData = new FormData();
                Ext.Object.each(param, function (k, v) {
                    formData.append(k, v);
                });
                xhr.send(formData);
                return;
            }
            xhr.send();
        },


        /**
         * Grid의 각 column별로 filtering 기능 담당
         * @param event
         * @param target
         * @param options
         *  options.gridItem : Grid의 itemId
         *  options.filterReference : 해당컬럼 hbox.textfield 의 reference 값
         *  options.columnName : 해당컬럼의 name 값
         *  options.anyMatch : anyMatch의 true/false 값  false 일 경우 앞부분부터 순차비교
         */
        onGridFilterField: function (thisObj, options) {
            var grid = thisObj.getView().down('#' + options.gridItem),
                // Access the field using its "reference" property name.
                filterField = thisObj.lookupReference(options.filterReference), //'nameFilterField'),
                filters = grid.store.getFilters();

            if (filterField.value) {
                thisObj.nameFilter = filters.add({
                    id: 'nameFilter',
                    property: options.columnName,
                    value: filterField.value,
                    anyMatch: options.anyMatch,
                    caseSensitive: false
                });
            } else if (thisObj.nameFilter) {
                filters.remove(thisObj.nameFilter);
                this.nameFilter = null;
            }
        },

        onEmpGridFilterField: function (grid, thisObj, options) {
            var filterField = thisObj.lookupReference(options.filterReference), //'nameFilterField'),
                filters = grid.store.getFilters();

            if (filterField.value) {
                thisObj.nameFilter = filters.add({
                    id: 'nameFilter',
                    property: options.columnName,
                    value: filterField.value,
                    anyMatch: options.anyMatch,
                    caseSensitive: false
                });
            } else if (thisObj.nameFilter) {
                filters.remove(thisObj.nameFilter);
                this.nameFilter = null;
            }
        },


        uploadWithForm: function (cfg) {
            var url = cfg.url,
                formData = cfg.formData,
                xhr = new XMLHttpRequest(),
                success = cfg.success;

            xhr.open('POST', url);
            Utils.fileUploadCallback({
                xhr: xhr,
                success: success
            });

            xhr.send(formData);
        },


        /**
         *  업로드 콜백(fileUpload, fileGridUpload, fileFormUpload 해당)
         *  cfg
         *   - xhr: XMLHttpRequest
         *   - success: 성공시 콜백
         *   - error: 실패시 콜백
         */
        fileUploadCallback: function (cfg) {
            var xhr = cfg.xhr,
                success = cfg.success,
                error = cfg.error,
                _error = function (ret) {
                    if (error && Ext.isFunction(error)) {
                        error(ret);
                    } else {
                        Ext.Msg.show({
                            title: 'Error',
                            icon: Ext.Msg.ERROR,
                            buttons: Ext.Msg.OK,
                            message: ret.message || '관리자에게 문의 하십시오'
                        });
                    }
                };

            xhr.addEventListener('loadend', function (e) {
                var target = e.target,
                    ret = (function () {
                        try {
                            return Ext.decode(target.responseText);
                        } catch (e) {
                            return {};
                        }
                    })();

                if (target.status == 200) {
                    if (success && Ext.isFunction(success)) {
                        success(ret);
                    } else {
                        _error(ret);
                    }
                } else {
                    _error(ret);
                }
            });
        },


        /**
        * form 을 이용한 파일 다운로드
        * options = {
        * url: '호출 url',
        * fields: [ '필드이름1', '필드이름2'.....],
        * params: { '필드이름1': '값1', '필드이름2': '값2' }
        * }
        */
        downloadByForm: function (options) {
            var formFd = [];
            var fds = options.fields;
            var vals = options.params;
            for (var i = 0; i < fds.length; i++) {
                formFd.push(
                    {
                        tag: 'input',
                        type: 'hidden',
                        name: fds[i],
                        value: vals[fds[i]]
                    }
                );
            }

            var form = Ext.DomHelper.append(document.body, {
                tag: 'form',
                method: 'post',
                action: options.url,
                children: formFd
            });
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        getData: function (data) {
            var ret = [];
            if (Ext.isArray(data)) {
                Ext.each(data, function (row) {
                    ret.push(row.getData({serialize: true}));
                });
            }
            if (data.isStore) {
                data.each(function (rec) {
                    ret.push(rec.getData({serialize: true}));
                });
            }
            return ret;
        },


        validateRecord: function (rec) {
            if (rec.isModel) {
                var val = rec.getValidation(true),
                    data = val.getData(), errorMsg;

                Ext.Object.each(data, function (k, v) {
                    if (Ext.isString(v)) {
                        errorMsg = v;
                        return false;
                    }
                });
                if (errorMsg) {
                    BRAVO.Msg.alert('알림', errorMsg);
                    return false;
                }
            }
            return true;
        }
    }
});
