/**
 * 각 페이지에서 서버와의 Ajax통신에 필요한 메서드 모음
 */
Ext.define('BRAVO.Ajax', {
    extend: 'Ext.Base',
    
    statics: {
        confirmMsg: null,
        successMsg: null,
        timeout : 9000000,
        loadMessage : null,
        
        /**
         * Ajax Request
         */
        request: function (options) {

            if (options.params) {
                //  options.jsonData = Ext.encode(options.params);
            }

            options.timeout = options.timeout || BRAVO.Ajax.timeout;
            var me = this;
            var confirmMsg = options.confirmMsg;
            if (confirmMsg) {
                BRAVO.Msg.confirm(confirmMsg.title, confirmMsg.message, function (b) {
                    me._runAjax(options);
                });
            } else {
                me._runAjax(options);
            }
        },
        
        
        /**
         * Ajax 실행
         */
        _runAjax: function (options) {

            if(!options.msgBox){
                BRAVO.Ajax.loadMessage=Ext.MessageBox.wait('잠시만 기다리세요..','데이터조회중', {
                    interval: 500, //bar will move fast!
                    duration: 120000,
                    increment: 15
                });
            }

            var callBackFunc = Ext.clone(options.success);
            options.success = function (response) {
                BRAVO.Ajax.loadMessage.hide();

                var resObj = Ext.isEmpty(response.responseText) ?'' :Ext.decode(response.responseText, true);
                var me = this;
                var successMsg = options.successMsg;
                if (Ext.isObject(successMsg)) {
                    Ext.toast({
                        title: successMsg.title,
                        closable: true,
                        html: successMsg.message
                    });
                }
                Ext.callback(callBackFunc, me, [resObj]);
            };

            options.failure = function (response) {
                BRAVO.Ajax.loadMessage.hide();
                // 나중에 server-side와 형식 맞춰서 code 고도화
                var objError = Ext.decode( response.responseText, true);
                if(response.status == "400" && objError) {
                    Ext.MessageBox.show({ title: objError.title, msg: objError.msg, icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });
                } else if(response.status == "500") {
                    Ext.MessageBox.show({ title: 'Error', msg: '관리자에게 문의하세요.[500]', icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK });        // rtnString2[1]
                }

                if( response.status == 403) {
                    var msgBox = Ext.MessageBox.show({
                        title: 'Forbidden',
                        msg: "요청하신 페이지를 조회할 수 있는 권한이 없습니다.",
                        closable: true,
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK
                    });
                }
                if(response.status == 404) {
                    var result = Ext.decode( response.responseText, true);
                    Ext.MessageBox.show({
                        title: response.status + " ERROR ",
                        msg: 'Message: 요청하신 페이지를 찾을 수 없습니다.',
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK
                    });
                }    
            };
            Ext.Ajax.request(options);
        },

        
        /**
         * 해당 페이지의 form을 서버로 post 방식으로 submit
         */
        submitForm : function( form, url,callBackFunc){
            if(!BRAVO.Utils.isValidForm(form)){
                return false;
            }
            var paramObj = form.getForm().getValues();
            BRAVO.Ajax.request({
                url: url,
                mode: 'POST',
                params: paramObj,
                successMsg : {
                    title : 'Success',
                    message : '성공하였습니다.'
                },
                success: callBackFunc,
                scope: ( form.getController() || form )
            });
        },

        insertForm : function( scope,url,callBackFunc){
            var form = scope.getView();
            if(form.getXTypes().search('/form')==-1){
                form= form.down('form');
            }
            if(!BRAVO.Utils.isValidForm(form)){
                return false;
            }
            var paramObj = form.getForm().getValues();
            BRAVO.Ajax.request({
                url: url,
                mode: 'POST',
                //jsonData: Ext.encode( paramObj),
                params: paramObj,
                successMsg : {
                    title : '신규성공',
                    message : '저장하였습니다.'
                },
                success: callBackFunc,
                scope: scope
            });
        },

        updateForm : function(scope,url,callBackFunc){
            var form = scope.getView();
            if(form.getXTypes().search('/form')==-1){
                form= form.down('form');
            }
            if(!BRAVO.Utils.isValidForm(form)){
                return false;
            }
            var paramObj = form.getForm().getValues();
            BRAVO.Ajax.request({
                url: url,
                mode: 'POST',
                //jsonData: Ext.encode( paramObj),
                params: paramObj,
                successMsg : {
                    title : '수정성공',
                    message : '저장하였습니다.'
                },
                success: callBackFunc,
                scope: scope
            });
        },

        deleteForm : function(scope,url,callBackFunc){
            var form = scope.getView();
            if(form.getXTypes().search('/form')==-1){
                form= form.down('form');
            }

            if(!BRAVO.Utils.isValidForm(form)){
                return false;
            }
            var paramObj = form.getForm().getValues();
            BRAVO.Ajax.request({
                url: url,
                mode: 'POST',
                params: paramObj,
                confirmMsg : {
                    title : '삭제',
                    message : '삭제하시겠습니까?'
                },
                successMsg : {
                    title : '삭제성공',
                    message : '삭제하였습니다.'
                },
                success: callBackFunc,
                scope: scope
            });
        },

        previewResult : function(scope,url,callBackFunc){
            var form = scope.getView().down('form');
            if(!BRAVO.Utils.isValidForm(form)){
                return false;
            }
            var paramObj = form.getForm().getValues();
            BRAVO.Ajax.request({
                url: url,
                mode: 'POST',
                params: paramObj,
                successMsg : {
                    title : '수정성공',
                    message : '저장하였습니다.'
                },
                success: callBackFunc,
                scope: scope
            });
        },
        
        postForm : function( options){
            var form =  options.form; 
            
            if(form.getXTypes().search('/form')==-1){
                form= form.down('form');
            }
            if( options.skipValidationCheck ){}else{
                if(!BRAVO.Utils.isValidForm(form)){
                    return false;
                }
            }
            var paramObj = form.getForm().getValues();
            BRAVO.Ajax.request({
                url: options.targetUrl,
                mode: 'POST',
                params: paramObj,
                confirmMsg: options.confirmMsg,
                successMsg : options.successMsg,
                success: options.callBack
            });
        }
    }
});