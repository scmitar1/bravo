Ext.define('BRAVO.toolbar.BravoToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.bravo_toolbar',
    border: false,
    layout : { type : 'hbox' },
    defaults : { width: 80, height: 25 }
});