Ext.define('BRAVO.toolbar.BottomButtonToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.toolbar_bottom_button',
    layout : { type : 'hbox',align : 'stretch' },
    bottomButtons : [],
    defaults : { width : 80, height : 30 },
    buttonAlign : 'center',
    onBtnInsert: 'onBtnInsert',
    onBtnUpdate: 'onBtnUpdate',
    onBtnDelete: 'onBtnDelete',
    onBtnReset : 'onBtnReset',
    onBtnPreview :'onBtnPreview',

    initComponent: function () {
        var me = this;
        var items = [];

        if(!Ext.isEmpty(me.infoText)){
            items.push({
                xtype : 'displayfield',
                value : me.infoText,
                width: 250
            });
        }

        if(me.buttonAlign=='center' ||me.buttonAlign== 'right'){
            items.push({xtype:'tbfill'});
        }

        for(var i=0;i<me.bottomButtons.length;i++){
            var buttonType = me.bottomButtons[i];
            var tempObj={};
            if(Ext.isString(buttonType)) {

                if(buttonType=='reset'){
                    Ext.apply(tempObj,{
                        xtype: 'bravo_button_' + buttonType,
                        handler: function(button){
                            button.up('global_default_view').down('form').getForm().reset();
                        }
                    });
                }else{
                    Ext.apply(tempObj,{
                        xtype: 'bravo_button_' + buttonType,
                        handler: 'onBtn' + buttonType.charAt(0).toUpperCase() + buttonType.slice(1)
                    });
                }
            } else {
                Ext.apply(tempObj, buttonType);
                Ext.apply(tempObj,{
                    xtype : 'bravo_button_'+buttonType.btnType
                });

                if(!tempObj.handler){
                    tempObj.handler= 'onBtn' + buttonType.btnType.charAt(0).toUpperCase() + buttonType.btnType.slice(1);
                }
            }
            items.push(tempObj);
        }

        if(me.buttonAlign=='center') {
            items.push({xtype:'tbfill'});
        }

        Ext.apply(me, {
            items :items
        });

        me.callParent(arguments);
    }

});