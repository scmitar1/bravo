Ext.define('BRAVO.toolbar.Paging', {
    extend: 'Ext.toolbar.Paging',
    alias: 'widget.bravo_pagingtoolbar',

    displayInfo: true,
    displayMsg: 'Total Count :  {2}',
    getPagingItems: function () {
        var me = this,
            inputListeners = {
                scope: me,
                blur: me.onPagingBlur
            };

        inputListeners[Ext.supports.SpecialKeyDownRepeat ? 'keydown' : 'keypress'] = me.onPagingKeyDown;

        return [
            {
                itemId: 'refresh',
                tooltip: me.refreshText,
                overflowText: me.refreshText,
                iconCls: Ext.baseCSSPrefix + 'tbar-loading',
                disabled: me.store.isLoading(),
                handler: me.doRefresh,
                scope: me
            }];
    },

    initComponent: function () {
        var me = this,
            grid = me.up('grid');

        me.callParent(arguments);
        if (grid) {
            me.bindStore(me._getStore(grid));
        }
    },

    getStoreListeners: function () {
        return Ext.apply({}, this.callParent(), {
            'datachanged': function (store) {
                this.updateInfo();
            }
        });
    },

    doRefresh: function () {
        var me = this,
            store = this.store,
            current = store.currentPage;

        if (me.fireEvent('beforechange', me, current) !== false) {
            store.loadPage(current, store.lastOptions);
            return true;
        }
    },

    beforeLoad: function (store) {
        this.setChildDisabled('#refresh', true);
    },

    getPageData: function () {
        var store = this.store,
            totalCount = store.getTotalCount() || store.count(),
            pageCount = Math.ceil(totalCount / store.pageSize),
            toRecord = Math.min(store.currentPage * store.pageSize, totalCount);
        return {
            total: totalCount,
            currentPage: store.currentPage,
            pageCount: Ext.Number.isFinite(pageCount) ? pageCount : 1,
            fromRecord: ((store.currentPage - 1) * store.pageSize) + 1,
            toRecord: toRecord || totalCount
        };
    },

    _getStore: function (grid) {
        var bind = grid.getBind() && grid.getBind()['store'];
        if (bind) {
            return bind.stub.boundValue;
        } else {
            return grid.getStore();
        }
    }
});