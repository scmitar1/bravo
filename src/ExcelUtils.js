/**
 * Created by Hyunkuk on 2017-10-30.
 */
Ext.define('BRAVO.ExcelUtils', {
    singleton: true,
    alternateClassName: 'ExcelUtils',

    getWorkbook: function () {
        return {
            SheetNames: [],
            Sheets: {}
        };
    },

    datenum: function (v, date1904) {
        if (date1904) v += 1462;
        var epoch = Date.parse(v);
        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    },

    convertData: function (data, merge) {
        var me = this,
            ws = {},
            range = {s: {c: 10000000, r: 10000000}, e: {c: 0, r: 0}},
            row, mergeList;

        for (var R = 0; R !== data.length; ++R) {
            data[R] = (data[R] || []);
            for (var C = 0; C !== data[R].length; ++C) {
                if (range.s.r > R) range.s.r = R;
                if (range.s.c > C) range.s.c = C;
                if (range.e.r < R) range.e.r = R;
                if (range.e.c < C) range.e.c = C;
                var cell = {v: data[R][C]};
                if (cell.v === null) continue;
                var cell_ref = XLSX.utils.encode_cell({c: C, r: R});

                if (typeof cell.v === 'number') {
                    Ext.apply(cell, {
                        t: 'n',
                        z: '#,##0'
                    });
                }
                else if (typeof cell.v === 'boolean') cell.t = 'b';
                else if (cell.v instanceof Date) {
                    cell.t = 'n';
                    cell.z = XLSX.SSF._table[14];
                    cell.v = me.datenum(cell.v);
                }
                else cell.t = 's';

                ws[cell_ref] = cell;
            }
        }
        if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
        if (Ext.isArray(merge)) {
            mergeList = ws['!merges'] = [];
            Ext.each(merge, function (itm) {
                var c = {
                    s: {
                        r: itm[0][0],
                        c: itm[0][1]
                    },
                    e: {
                        r: itm[1][0],
                        c: itm[1][1]
                    }
                };
                mergeList.push(c);
            });
        }
        return ws;
    },

    createExcelFile: function (param) {
        var sheetNm = param.sheetNm || 'sheet',
            data = param.data,
            merge = param.merge,
            wb = ExcelUtils.getWorkbook(),
            sheets = param.sheets;

        if (sheets) {
            Ext.Object.each(sheets, function (k, excelDataModel) {
               wb.SheetNames.push(k);
               wb.Sheets[k] = ExcelUtils.convertData(excelDataModel.getData(), excelDataModel.getMergeList());
            });
        } else {
            wb.SheetNames.push(sheetNm);
            wb.Sheets[sheetNm] = ExcelUtils.convertData(data, merge);
        }

        return XLSX.write(wb, {
            bookType: 'xlsx',
            bookSTT: true,
            type: 'binary'
        });
    },

    saveFile: function (file, fileNm) {
        var fn = function (s) {
            var buf = new ArrayBuffer(s.length);
            var view = new Uint8Array(buf);
            for (var i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
        };

        // window.saveAs: FileSaver.js에서 정의된 전역함수
        if (window.saveAs) {
            saveAs(new Blob([fn(file)], {type: "application/octet-stream"}), fileNm);
        }
    },

    /*
     * param.sheetNm,
     * param.data
     * param.fileNm
     * */
    downloadFile: function (param) {
        var file = ExcelUtils.createExcelFile(param);
        ExcelUtils.saveFile(file, param.fileNm || '보고서.xlsx');
    }
});