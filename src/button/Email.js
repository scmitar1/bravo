Ext.define('BRAVO.button.Email',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_email',
    text: 'EMail',
    iconCls: 'x-fa fa-envelope'
});