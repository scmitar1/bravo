Ext.define('BRAVO.button.Excel',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_excel',
    text: 'Excel',
    iconCls: 'x-fa fa-file-excel-o'
});