Ext.define('BRAVO.button.Pdf',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_pdf',
    text: 'Excel',
    iconCls: 'x-fa fa-file-pdf-o'
});