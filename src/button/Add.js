Ext.define('BRAVO.button.Add',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_add',
    text: '추가',
    iconCls: 'x-fa fa-plus-circle'
});