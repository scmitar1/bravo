Ext.define('BRAVO.button.Reset',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_reset',
    text: '리셋',
    iconCls: 'x-fa fa-refresh'
});