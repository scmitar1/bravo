Ext.define('BRAVO.button.Update',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_update',
    text: '수정',
    formBind: true,
    iconCls: 'x-fa fa-floppy-o'
});