Ext.define('BRAVO.button.Insert', {
    extend: 'Ext.button.Button',
    alias: 'widget.bravo_button_insert',
    text: '입력',
    formBind: true,
    iconCls: 'x-fa fa-plus'

});