Ext.define('BRAVO.button.Modify',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_modify',
    text: '수정',
    iconCls: 'x-fa fa-pencil'
});