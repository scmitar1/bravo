Ext.define('BRAVO.button.Save',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_save',
    text: '저장',
    formBind: true,
    iconCls: 'x-fa fa-floppy-o'
});