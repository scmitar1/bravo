Ext.define('BRAVO.button.Preview',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_preview',
    text: '미리보기',
    minWidth: 85,
    iconCls: 'x-fa fa-search'
});