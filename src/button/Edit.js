Ext.define('BRAVO.button.Edit',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_edit',
    text: '편집',
    iconCls: 'x-fa fa-pencil'
});