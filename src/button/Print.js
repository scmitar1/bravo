Ext.define('BRAVO.button.Print',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_print',
    text: '출력',
    iconCls: 'x-fa fa-print'
});
