Ext.define('BRAVO.button.Search',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_search',
    text: '조회',
    iconCls: 'x-fa fa-search'
});