Ext.define('BRAVO.button.Copy',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_copy',
    text: '복사',
    iconCls: 'x-fa fa-copy'
});