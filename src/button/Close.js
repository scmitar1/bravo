Ext.define('BRAVO.button.Close',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_close',
    text: '닫기',
    iconCls: 'x-fa fa-times'
});