Ext.define('BRAVO.button.Next',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_next',
    text: 'NEXT',
    iconCls: 'x-fa fa-angle-double-right',
    padding: 8, margin: 1,
    width: 100
});