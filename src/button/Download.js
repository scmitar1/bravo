Ext.define('BRAVO.button.Download',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_download',
    text: '다운로드',
    minWidth: 100,
    iconCls: 'x-fa fa-download'
});