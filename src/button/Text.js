Ext.define('BRAVO.button.Text',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_text',
    text: 'Text',
    iconCls: 'x-fa fa-file-text-o'
});