Ext.define('BRAVO.button.Delete',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_delete',
    text: '삭제',
    iconCls: 'x-fa fa-trash'
});