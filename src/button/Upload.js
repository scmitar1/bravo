Ext.define('BRAVO.button.Upload',{
    extend : 'Ext.button.Button',
    alias : 'widget.bravo_button_upload',
    text: '업로드',
    iconCls: 'x-fa fa-upload'
});