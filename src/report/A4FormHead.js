Ext.define('BRAVO.report.A4FormHead',{
    alias : 'widget.a4_form_head',
    extend : 'Ext.form.Panel',
    columns: 13,
    tableLayout : 'auto',
    border : '0px solid black',
    borderWidth : '1px 1px 1px 1px',
    plugins : [{
        ptype : 'pmh-report'
    }],
    tdAttrs : {
        style: {
            border        : '0px dotted black',
            borderWidth   : '1px 1px 0px 0px',
            borderCollapse: 'collapse',
            verticalAlign : 'middle',
            textAlign     : 'center',
            height        : '100%'
        }
    },
    initComponent : function(){
        var me = this;

        Ext.apply(me,{
            border : false,
            layout: {
                type: 'table',
                columns: me.columns,
                tableAttrs : {
                    style : {
                        border : me.border,
                        borderWidth : me.borderWidth,
                        tableLayout : me.tableLayout
                    }
                }
            }
        });
        me.callParent(arguments);
    }
});