/**
 * Created by Hynkuk on 2018-01-19.
 */
Ext.define('BRAVO.ExcelDataModel', {
    alternateClassName: 'ExcelDataModel',
    config: {
        data: [],
        mergeList : []
    },

    constructor: function () {
        this.setData([]);
        this.callParent();
    },

    getRowData: function (cursor) {
        var r = this.getData()[cursor.rIdx];

        if (!this.getData()[cursor.rIdx]) {
            r = this.getData()[cursor.rIdx] = [];
        }
        return r;
    },

    setCellValue: function (cursor, v) {
        this.getRowData(cursor)[cursor['cIdx']] = v;
    },

    setColumns: function (cursor, columns) {
        var me = this,
            cIdx = cursor.cIdx;

        Ext.each(columns, function (colTxt) {
            me.setCellValue(cursor, colTxt);
            cursor.cIdx++;
        });
        cursor.rIdx++;
        cursor.cIdx = cIdx;
    }
});